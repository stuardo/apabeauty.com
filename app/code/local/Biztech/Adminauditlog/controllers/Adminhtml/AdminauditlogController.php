<?php
/**
 * Log grid controller
 */
class Biztech_Adminauditlog_Adminhtml_AdminauditlogController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Log page
     */
    public function indexAction()
    {
        $this->_title($this->__('System'))
             ->_title($this->__('Admin Actions Audit Log'))
             ->_title($this->__('Report'));

        $this->loadLayout();
        $this->_setActiveMenu('system/adminauditlog');
        $this->renderLayout();
    }

    /**
     * Log grid ajax action
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * View Admin Audit Log details
     */
    public function detailsAction()
    {
        $this->_title($this->__('System'))
             ->_title($this->__('Admin Actions Audit Log'))
             ->_title($this->__('Report'))
             ->_title($this->__('View Entry'));

        $eventId = $this->getRequest()->getParam('event_id');
        $model   = Mage::getModel('adminauditlog/event')
            ->load($eventId);
        if (!$model->getId()) {
            $this->_redirect('*/*/');
            return;
        }
        Mage::register('current_event', $model);

        $this->loadLayout();
        $this->_setActiveMenu('system/adminauditlog');
        $this->renderLayout();
    }

    /**
     * Export log to CSV
     */
    public function exportCsvAction()
    {
        $this->_prepareDownloadResponse('log.csv',
            $this->getLayout()->createBlock('adminauditlog/adminhtml_index_grid')->getCsvFile()
        );
    }

    /**
     * Export log to MSXML
     */
    public function exportXmlAction()
    {
        $this->_prepareDownloadResponse('log.xml',
            $this->getLayout()->createBlock('adminauditlog/adminhtml_index_grid')->getExcelFile()
        );
    }

    /**
     * permissions checker
     */
    protected function _isAllowed()
    {
        switch ($this->getRequest()->getActionName()) {
            case 'grid':
            case 'exportCsv':
            case 'exportXml':
            case 'details':
            case 'index':
                return Mage::getSingleton('admin/session')->isAllowed('admin/system/adminauditlog/events');
                break;
        }

    }
}
