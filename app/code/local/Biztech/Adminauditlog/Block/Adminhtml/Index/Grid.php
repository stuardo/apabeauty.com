<?php
    /**
    * Admin Audit Log Grid
    *
    * @package     Biztech_Adminauditlog
    */
    class Biztech_Adminauditlog_Block_Adminhtml_Index_Grid extends Mage_Adminhtml_Block_Widget_Grid
    {
        /**
        * Constructor
        */
        public function __construct()
        {
            $flag = Mage::helper('adminauditlog')->isEnable();
            if($flag){
                parent::__construct();

                $this->setId('adminauditLogGrid');
                $this->setDefaultSort('time');
                $this->setDefaultDir('DESC');
                $this->setSaveParametersInSession(true);
                $this->setUseAjax(true);
            }
        }

        /**
        * PrepareCollection method.
        *
        * @return Mage_Adminhtml_Block_Widget_Grid
        */
        protected function _prepareCollection()
        {
            $this->setCollection(Mage::getResourceModel('adminauditlog/event_collection'));
            return parent::_prepareCollection();
        }

        /**
        * Return grids url
        *
        * @return string
        */
        public function getGridUrl()
        {
            return $this->getUrl('*/*/grid', array('_current' => true));
        }

        /**
        * Grid URL
        *
        * @param Mage_Catalog_Model_Product|Varien_Object $row
        * @return string
        */
        public function getRowUrl($row)
        {
            return $this->getUrl('*/*/details', array('event_id' => $row->getId()));
        }

        /**
        * Configuration of grid
        *
        * @return Biztech_Adminauditlog_Block_Adminhtml_Index_Grid
        */
        protected function _prepareColumns()
        {
            $this->addColumn('time', array(
                    'header'    => $this->__('Time'),
                    'index'     => 'time',
                    'type'      => 'datetime',
                    'width'     => 160,
                ));

            $this->addColumn('event', array(
                    'header'    => $this->__('Action Group'),
                    'index'     => 'event_code',
                    'type'      => 'options',
                    'sortable'  => false,
                    'options'   => Mage::getSingleton('adminauditlog/config')->getLabels(),
                ));

            $actions = array();
            foreach (Mage::getResourceSingleton('adminauditlog/event')->getAllFieldValues('action') as $action) {
                $actions[$action] = Mage::helper('adminauditlog')->getAdminauditlogActionTranslatedLabel($action);
            }
            $this->addColumn('action', array(
                    'header'    => $this->__('Action'),
                    'index'     => 'action',
                    'type'      => 'options',
                    'options'   => $actions,
                    'sortable'  => false,
                    'width'     => 75,
                ));

            $this->addColumn('ip', array(
                    'header'    => $this->__('IP Address'),
                    'index'     => 'ip',
                    'type'      => 'text',
                    'filter'    => 'adminauditlog/adminhtml_grid_filter_ip',
                    'renderer'  => 'adminhtml/widget_grid_column_renderer_ip',
                    'sortable'  => false,
                    'width'     => 125,
                ));

            $this->addColumn('user', array(
                    'header'    => $this->__('Username'),
                    'index'     => 'user',
                    'type'      => 'text',
                    'escape'    => true,
                    'sortable'  => false,
                    'filter'    => 'adminauditlog/adminhtml_grid_filter_user',
                    'width'     => 150,
                ));

            $this->addColumn('status', array(
                    'header'    => $this->__('Result'),
                    'index'     => 'status',
                    'sortable'  => false,
                    'type'      => 'options',
                    'options'   => array(
                        Biztech_Adminauditlog_Model_Event::RESULT_SUCCESS => $this->__('Success'),
                        Biztech_Adminauditlog_Model_Event::RESULT_FAILURE => $this->__('Failure'),
                    ),
                    'width'     => 100,
                ));

            $this->addColumn('fullaction', array(
                    'header'   => $this->__('Full Action Name'),
                    'index'    => 'fullaction',
                    'sortable' => false,
                    'type'     => 'text'
                ));

            $this->addColumn('info', array(
                    'header'    => $this->__('Item ID'),
                    'index'     => 'info',
                    'type'      => 'text',
                    'sortable'  => false,
                    'filter'    => 'adminhtml/widget_grid_column_filter_text',
                    'renderer'  => 'adminauditlog/adminhtml_grid_renderer_details',
                    'width'     => 100,
                ));

            $this->addColumn('view', array(
                    'header'  => $this->__('Full Details'),
                    'width'   => 50,
                    'type'    => 'action',
                    'getter'  => 'getId',
                    'actions' => array(array(
                            'caption' => $this->__('View'),
                            'url'     => array(
                                'base'   => '*/*/details',
                            ),
                            'field'   => 'event_id'
                        )),
                    'filter'    => false,
                    'sortable'  => false,
                ));

            $this->addExportType('*/*/exportCsv', Mage::helper('customer')->__('CSV'));
            $this->addExportType('*/*/exportXml', Mage::helper('customer')->__('Excel XML'));
            return $this;
        }
    }
