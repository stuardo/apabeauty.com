<?php
/**
 * Log grid container
 */
class Biztech_Adminauditlog_Block_Adminhtml_Log extends Mage_Adminhtml_Block_Widget_Container
{
    /**
     * Header text getter
     *
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper('adminauditlog')->__('Admin Actions Audit Log');
    }

    /**
     * Grid contents getter
     *
     * @return string
     */
    public function getGridHtml()
    {
        return $this->getChildHtml();
    }
}
