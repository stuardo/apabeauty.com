<?php
/**
 * Admin Audit Log grid
 *
 */
class Biztech_Adminauditlog_Block_Adminhtml_Details_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Initialize default sorting and html ID
     */
    protected function _construct()
    {
        $this->setId('adminauditlogDetailsGrid');
        $this->setPagerVisibility(false);
        $this->setFilterVisibility(false);
    }

    /**
     * Prepare grid collection
     *
     */
    protected function _prepareCollection()
    {
        $event = Mage::registry('current_event');
        $collection = Mage::getResourceModel('adminauditlog/event_changes_collection')
            ->addFieldToFilter('event_id', $event->getId());
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepare grid columns
     *
     */
    protected function _prepareColumns()
    {
        $this->addColumn('source_name', array(
            'header'    => Mage::helper('adminauditlog')->__('Source Data'),
            'sortable'  => false,
            'renderer'  => 'adminauditlog/adminhtml_details_renderer_sourcename',
            'index'     => 'source_name',
            'width'     => 1
        ));

        $this->addColumn('original_data', array(
            'header'    => Mage::helper('adminauditlog')->__('Value Before Change'),
            'sortable'  => false,
            'renderer'  => 'adminauditlog/adminhtml_details_renderer_diff',
            'index'     => 'original_data'
        ));

        $this->addColumn('result_data', array(
            'header'    => Mage::helper('adminauditlog')->__('Value After Change'),
            'sortable'  => false,
            'renderer'  => 'adminauditlog/adminhtml_details_renderer_diff',
            'index'     => 'result_data'
        ));

        return parent::_prepareColumns();
    }
}
