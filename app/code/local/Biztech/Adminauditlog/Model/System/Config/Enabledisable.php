<?php
	
class Biztech_Adminauditlog_Model_System_Config_Enabledisable{

    public function toOptionArray()
    {
        $options = array(
            array('value' => 0, 'label'=>Mage::helper('adminauditlog')->__('No')),
        );
        $websites = Mage::helper('adminauditlog')->getAllWebsites();
        if(!empty($websites)){
            $options[] = array('value' => 1, 'label'=>Mage::helper('adminauditlog')->__('Yes'));
        }
        return $options;
    }
}