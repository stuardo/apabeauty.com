<?php
    class Biztech_Adminauditlog_Model_Observer
    {

        protected $_processor;
        
        const ADMIN_USER_LOCKED = 243;

        /**
        * Initialize Biztech_Adminauditlog_Model_Processor class
        *
        */
        public function __construct()
        {
            $this->_processor = Mage::getSingleton('adminauditlog/processor');
        }

        /**
        * Mark actions for admin audit log, if required
        *
        * @param Varien_Event_Observer $observer
        */
        public function controllerPredispatch($observer)
        {
            /* @var $action Mage_Core_Controller_Varien_Action */
            $action = $observer->getEvent()->getControllerAction();
            /* @var $request Mage_Core_Controller_Request_Http */
            $request = $observer->getEvent()->getControllerAction()->getRequest();

            $beforeForwardInfo = $request->getBeforeForwardInfo();

            // Always use current action name bc basing on
            // it we make decision about access granted or denied
            $actionName = $request->getRequestedActionName();

            if (empty($beforeForwardInfo)) {
                $fullActionName = $action->getFullActionName();
            } else {
                $fullActionName = array($request->getRequestedRouteName());

                if (isset($beforeForwardInfo['controller_name'])) {
                    $fullActionName[] = $beforeForwardInfo['controller_name'];
                } else {
                    $fullActionName[] = $request->getRequestedControllerName();
                }

                if (isset($beforeForwardInfo['action_name'])) {
                    $fullActionName[] = $beforeForwardInfo['action_name'];
                } else {
                    $fullActionName[] = $actionName;
                }

                $fullActionName = implode('_', $fullActionName);
            }

            $this->_processor->initAction($fullActionName, $actionName);
        }

        /**
        * Model after save observer.
        *
        * @param Varien_Event_Observer
        */
        public function modelSaveAfter($observer)
        {
            $this->_processor->modelActionAfter($observer->getEvent()->getObject(), 'save');
        }

        /**
        * Model after delete observer.
        *
        * @param Varien_Event_Observer
        */
        public function modelDeleteAfter($observer)
        {
            $this->_processor->modelActionAfter($observer->getEvent()->getObject(), 'delete');
        }

        /**
        * Model after load observer.
        *
        * @param Varien_Event_Observer
        */
        public function modelLoadAfter($observer)
        {
            $this->_processor->modelActionAfter($observer->getEvent()->getObject(), 'view');
        }

        /**
        * Log marked actions
        *
        * @param Varien_Event_Observer $observer
        */
        public function controllerPostdispatch($observer)
        {
            if ($observer->getEvent()->getControllerAction()->getRequest()->isDispatched()) {
                $this->_processor->logAction();
            }
        }

        /**
        * Log successful admin sign in
        *
        * @param Varien_Event_Observer $observer
        */
        public function adminSessionLoginSuccess($observer)
        {
            $this->_logAdminLogin($observer->getUser()->getUsername(), $observer->getUser()->getId());
        }

        /**
        * Log failure of sign in
        *
        * @param Varien_Event_Observer $observer
        */
        public function adminSessionLoginFailed($observer)
        {
            $eventModel = $this->_logAdminLogin($observer->getUserName());

            if ($eventModel) {
                $exception = $observer->getException();
                if ($exception->getCode() == self::ADMIN_USER_LOCKED) {
                    $eventModel->setInfo(Mage::helper('adminauditlog')->__('User is locked'))->save();
                }
            }
        }

        /**
        * Log sign in attempt
        *
        * @param string $username
        * @param int $userId
        * @return Biztech_Adminauditlog_Model_Event
        */
        protected function _logAdminLogin($username, $userId = null)
        {
            $eventCode = 'admin_login';
            if (!Mage::getSingleton('adminauditlog/config')->isActive($eventCode, true)) {
                return;
            }
            $success = (bool)$userId;
            if (!$userId) {
                $userId = Mage::getSingleton('admin/user')->loadByUsername($username)->getId();
            }
            $request = Mage::app()->getRequest();
            return Mage::getSingleton('adminauditlog/event')->setData(array(
                    'ip'         => Mage::helper('core/http')->getRemoteAddr(),
                    'user'       => $username,
                    'user_id'    => $userId,
                    'is_success' => $success,
                    'fullaction' => "{$request->getRouteName()}_{$request->getControllerName()}_{$request->getActionName()}",
                    'event_code' => $eventCode,
                    'action'     => 'login',
                ))->save();
        }

        public function checkKey($observer)
        {
            $key = Mage::getStoreConfig('adminauditlog/activation/key');
            Mage::helper('adminauditlog')->checkKey($key);
        }
    }
