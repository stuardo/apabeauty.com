<?php
/**
 * Admin audit log event changes model
 *
 * @package     Biztech_Adminauditlog
 */
class Biztech_Adminauditlog_Model_Resource_Event_Changes extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource
     *
     */
    protected function _construct()
    {
        $this->_init('adminauditlog/event_changes', 'id');
    }
}
