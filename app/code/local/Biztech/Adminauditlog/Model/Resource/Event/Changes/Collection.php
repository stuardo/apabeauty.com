<?php
/**
 * Log items collection
 *
 * @package     Biztech_Adminauditlog
 */
class Biztech_Adminauditlog_Model_Resource_Event_Changes_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Initialize resource
     *
     */
    protected function _construct()
    {
        $this->_init('adminauditlog/event_changes');
    }
}
