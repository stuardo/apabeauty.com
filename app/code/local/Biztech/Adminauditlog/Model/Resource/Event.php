<?php
/**
 * Admin audit log event resource model
 *
 * @package     Biztech_Adminauditlog
 */
class Biztech_Adminauditlog_Model_Resource_Event extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource
     *
     */
    protected function _construct()
    {
        $this->_init('adminauditlog/event', 'log_id');
    }

    /**
     * Convert data before save ip
     *
     * @param Mage_Core_Model_Abstract $event
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $event)
    {
        $event->setData('ip', ip2long($event->getIp()));
        $event->setTime($this->formatDate($event->getTime()));
    }

    /**
     * Rotate logs - get from database and pump to CSV-file
     *
     * @param int $lifetime
     */
    public function rotate($lifetime)
    {
//        $this->beginTransaction();
//        try {
            $readAdapter  = $this->_getReadAdapter();
            $writeAdapter = $this->_getWriteAdapter();
            $table = $this->getTable('adminauditlog/event');

            // get the latest log entry required to the moment
            $clearBefore = $this->formatDate(time() - $lifetime);

            $select = $readAdapter->select()
                ->from($this->getMainTable(), 'log_id')
                ->where('time < ?', $clearBefore)
                ->order('log_id DESC')
                ->limit(1);
            $latestLogEntry = $readAdapter->fetchOne($select);
            if ($latestLogEntry) {
                // make sure folder for dump file will exist
                $archive = Mage::getModel('adminauditlog/archive');
                $archive->createNew();

                $expr = Mage::getResourceHelper('adminauditlog')->getInetNtoaExpr('ip');
                $select = $readAdapter->select()
                    ->from($this->getMainTable())
                    ->where('log_id <= ?', $latestLogEntry)
                    ->columns($expr);

                $rows = $readAdapter->fetchAll($select);

                // dump all records before this log entry into a CSV-file
                $csv = fopen($archive->getFilename(), 'w');
                foreach ($rows as $row) {
                    fputcsv($csv, $row);
                }
                fclose($csv);

                $writeAdapter->delete($this->getMainTable(), array('log_id <= ?' => $latestLogEntry));
//                $this->commit();
            }
//        } catch (Exception $e) {
//            $this->rollBack();
//        }
    }

    /**
     * Select all values of specified field from main table
     *
     * @param string $field
     * @param bool $order
     * @return array
     */
    public function getAllFieldValues($field, $order = true)
    {
        $adapter = $this->_getReadAdapter();
        $select  = $adapter->select()
            ->distinct(true)
            ->from($this->getMainTable(), $field);
        if (!is_null($order)) {
            $select->order($field . ($order ? '' : ' DESC'));
        }
        return $adapter->fetchCol($select);
    }

    /**
     * Get all admin usernames that are currently in event log table
     * Possible SQL-performance issue
     *
     * @return array
     */
    public function getUserNames()
    {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()
            ->distinct()
            ->from(array('admins' => $this->getTable('admin/user')), 'username')
            ->joinInner(
                array('events' => $this->getTable('adminauditlog/event')),
                'admins.username = events.' . $adapter->quoteIdentifier('user'),
                array());
        return $adapter->fetchCol($select);
    }

    /**
     * Get event change ids of specified event
     *
     * @param int $eventId
     * @return array
     */
    public function getEventChangeIds($eventId)
    {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()
            ->from($this->getTable('adminauditlog/event_changes'), array('id'))
            ->where('event_id = ?', $eventId);
        return $adapter->fetchCol($select);
    }
}
