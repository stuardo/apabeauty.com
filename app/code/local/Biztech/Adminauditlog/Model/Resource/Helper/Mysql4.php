<?php
/**
 * Eav Mysql4 resource helper model
 *
 * @package     Biztech_Adminauditlog
 */
class Biztech_Adminauditlog_Model_Resource_Helper_Mysql4 extends Mage_Eav_Model_Resource_Helper_Mysql4
{
    /**
     * Returns expression for converting int field to IP string
     *
     * @param string | null $field if field is null then this field must be user for prepareSqlCondition
     * @return Zend_Db_Expr
     */
    public function getInetNtoaExpr($field = null)
    {
        $field = $field ? $this->_getReadAdapter()->quoteIdentifier($field) : '#?';
        return new Zend_Db_Expr('INET_NTOA(' . $field . ')');
    }
}
