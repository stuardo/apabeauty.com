<?php
 /**
  * Source model for admin audit log frequency
  */
class Biztech_Adminauditlog_Model_Source_Frequency
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => 1,
                'label' => Mage::helper('adminauditlog')->__('Daily')
            ),
            array(
                'value' => 7,
                'label' => Mage::helper('adminauditlog')->__('Weekly')
            ),
            array(
                'value' => 30,
                'label' => Mage::helper('adminauditlog')->__('Monthly')
            ),
        );
    }
}
