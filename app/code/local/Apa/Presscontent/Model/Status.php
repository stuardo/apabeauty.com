<?php
class Apa_Presscontent_Model_Status
{
	
    const STATUS_ACTIVE    = 1;
    const STATUS_INACTIVE   = 2;
    static public function getOptionArray()     
    {         
		 return array(self::STATUS_ACTIVE    =>Mage::helper('presscontent')->__('Active'),
		 self::STATUS_INACTIVE   => Mage::helper('presscontent')->__('Inactive')         
     );
   }
}
