<?php
class Apa_Presscontent_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
            $this->loadLayout();
            $this->renderLayout();
    }
    
    public function pageAction()
    {    	
    	$this->loadLayout();
      $this->renderLayout();
    }
        
    public function viewarticleAction()
    {  
    	$block = $this->getLayout()->createBlock('core/template');
      $block->setTemplate('presscontent/article.phtml');
      echo $block->toHtml();  
    }
    
    public function viewsocialAction()
    {
    	$this->loadLayout();
      $this->renderLayout();
    }
    
    
    
}
