<?php
  
class Apa_Presscontent_Adminhtml_PresscontentController extends Mage_Adminhtml_Controller_Action
{
  
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('presscontent/items')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Content Manager'), Mage::helper('adminhtml')->__('Content Manager'));
        return $this;
    }  
    
    public function indexAction() {
        $this->_initAction();      
        $this->_addContent($this->getLayout()->createBlock('presscontent/adminhtml_presscontent'));
        $this->renderLayout();
    }
  
    public function editAction()
    {
        $presscontentId     = $this->getRequest()->getParam('id');
        $presscontentModel  = Mage::getModel('presscontent/presscontent')->load($presscontentId);
  
        if ($presscontentModel->getId() || $presscontentId == 0) {
  
            Mage::register('presscontent_data', $presscontentModel);
  
            $this->loadLayout();
            $this->_setActiveMenu('presscontent/items');
            
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Content Manager'), Mage::helper('adminhtml')->__('Item Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Content'), Mage::helper('adminhtml')->__('Content'));
            
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            
            $this->_addContent($this->getLayout()->createBlock('presscontent/adminhtml_presscontent_edit'))
                 ->_addLeft($this->getLayout()->createBlock('presscontent/adminhtml_presscontent_edit_tabs'));
                
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('presscontent')->__('Content does not exist'));
            $this->_redirect('*/*/');
        }
    }
    
    public function newAction()
    {
        $this->_forward('edit');
    }   
    
    public function saveAction()
    {	   
         if ( $this->getRequest()->getPost() ) {
			  $postData = $this->getRequest()->getPost();
			  
			  if(isset($postData['cover']['delete']) && ($postData['cover']['delete']==1))
			  {
			      unlink(Mage::getBaseDir('media').DS.'presscontent'.DS. $postData['cover']['value']);
			  }
			  
			  if(isset($postData['image']['delete']) && ($postData['image']['delete']==1))
			  {
			      unlink(Mage::getBaseDir('media').DS.'presscontent'.DS. $postData['image']['value']);
			  }
            try 
            {	
            	$req_id=$this->getRequest()->getParam('id');
            	if(empty($req_id))
            	{
            		if(($_FILES['image']['name']['0'] == '') || ($_FILES['cover']['name'] == '') )
            		{
            			Mage::getSingleton('core/session')->addError('Please Fill All The Required Fields');
            			$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            			return;
            		}
            	}
            	else
            	{	
            		$model=Mage::getModel('presscontent/presscontent')->load($req_id);
						$image_name=$model->getData('image');
						$cover_image=$model->getData('cover');	
            	}
            	
            	if(isset($_FILES['cover']['name']) && ($_FILES['cover']['name']!= ''))
            	{
		         	/* Code for Cover Image Upload */
				      		 try {
										 $uploader = new Varien_File_Uploader('cover');
			 							 $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png')); 
	 									 $uploader->setAllowRenameFiles(false);
		 								 $uploader->setFilesDispersion(false);
	  									 $target = Mage::getBaseDir('media') . DS . 'presscontent' . DS ;
										 $uploader->save($target, $_FILES['cover']['name']);
	 									 $cover_image =$_FILES['cover']['name'];
	  									}
	  									catch(Exception $e) 
	  									{
	  										echo $e->getMessage();
	  									}
				      		/* Code for Cover Image Upload */
            	}
            	
		         if(isset($_FILES['image']['name']) &&  ($_FILES['image']['name']['0'] != '')) 
		         {
		         		/* Code for Multiple upload Press Article Image */
		          		$image_name="";
		          	    foreach($_FILES['image']['name'] as $key => $image)
		          		 {
				       		try {
										  $uploader = new Varien_File_Uploader(
				  						 	array(
												 'name' => $_FILES['image']['name'][$key],
												 'type' => $_FILES['image']['type'][$key],
												 'tmp_name' => $_FILES['image']['tmp_name'][$key],
												 'error' => $_FILES['image']['error'][$key],
												 'size' => $_FILES['image']['size'][$key])
											);

										  // Any extention would work
										  $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
										  $uploader->setAllowRenameFiles(true);

										  $uploader->setFilesDispersion(false);

										  $path = Mage::getBaseDir('media') . DS . 'presscontent' . DS;
										  $img = $uploader->save($path, $_FILES['images']['name'][$key]);
										  $image_name.=$img['file']."/";
		 							} 
    							catch (Exception $e) {
        							echo $e->getMessage();
        							Mage::log($e->getMessage());
    							}
		          		 
		          		}
		          		$image_name= rtrim($image_name,"/");
		          		/* Code for Multiple upload Press Article Image */
		          			         		
	  			  	}
	  			  	 $presscontentModel = Mage::getModel('presscontent/presscontent');
                $presscontentModel->setId($this->getRequest()->getParam('id'))
                    ->setTitle($postData['title'])
                    ->setCover($cover_image)
                    ->setImage($image_name)
                    ->setPresscontent($postData['presscontent'])
                    ->setUrl($postData['url'])
                    ->setPublished($postData['published'])
                    ->setPublishdate($postData['publishdate'])
                    ->setStatus($postData['status'])
                    ->setUpdateTime(now())
                    ->save();

                    
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setPresscontentData(false);
  
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setPresscontentData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
         
         
        }
        $this->_redirect('*/*/');
    }
    
    public function deleteAction()
    {
        if( $this->getRequest()->getParam('id') > 0 ) {
            try {
                $presscontentModel = Mage::getModel('presscontent/presscontent');
                
                $presscontentModel->setId($this->getRequest()->getParam('id'))
                    ->delete();
                    
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }
    
    public function massDeleteAction()
	{
	$pressIds = $this->getRequest()->getParam('presscontent_id');     
	if(!is_array($pressIds)) {
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('presscontent')->__('Please select Content Entry(es).'));
	} 
	else {
	try {
	$pressModel = Mage::getModel('presscontent/presscontent');
	foreach ($pressIds as $pressId) {
	$pressModel->load($pressId)->delete();
	}
	Mage::getSingleton('adminhtml/session')->addSuccess(
	Mage::helper('presscontent')->__(
	'Total of %d record(s) were deleted.', count($pressIds)
	)
	);
	} catch (Exception $e) {
	Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
	}
	}
	 
	$this->_redirect('*/*/index');
	}
	
	public function massStatusAction()
   {
    	  $status = (int)$this->getRequest()->getParam('status');
        $contentIds = $this->getRequest()->getParam('presscontent_id');
        if(!is_array($contentIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select Content(s)'));
        } else {
            try {
                foreach ($contentIds as $id) {
                    $contents = Mage::getSingleton('presscontent/presscontent')
                        ->load($id)
                        ->setStatus($status)
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($contentIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Product grid for AJAX request.
     * Sort and filter result for example.
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
               $this->getLayout()->createBlock('presscontent/adminhtml_presscontent_grid')->toHtml()
        );
        exit();
    }
}
