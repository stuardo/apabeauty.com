<?php
  
class Apa_Presscontent_Block_Adminhtml_Presscontent extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_presscontent';
        $this->_blockGroup = 'presscontent';
        $this->_headerText = Mage::helper('presscontent')->__('Content Manager');
        $this->_addButtonLabel = Mage::helper('presscontent')->__('Add Content');
        parent::__construct();
    }
}
