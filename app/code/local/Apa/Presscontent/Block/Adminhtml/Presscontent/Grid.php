<?php
  
class Apa_Presscontent_Block_Adminhtml_Presscontent_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('presscontentGrid');
        $this->setDefaultSort('presscontent_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
    
    protected function _prepareMassaction()
	 {
		$this->setMassactionIdField('presscontent_id');
		$this->getMassactionBlock()->setFormFieldName('presscontent_id');
		$this->getMassactionBlock()->addItem('delete', array(
		'label'=> Mage::helper('presscontent')->__('Delete'),
		'url'  => $this->getUrl('*/*/massDelete', array('' => '')),        
		'confirm' => Mage::helper('presscontent')->__('Are you sure?')
		));
		
		$statuses = Mage::getModel('presscontent/status')->getOptionArray();
		array_unshift($statuses, array('label'=>'', 'value'=>''));
		$this->getMassactionBlock()->addItem('status', array(
		'label'=> Mage::helper('presscontent')->__('Change status'),
		'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
		'additional' => array(
		               'visibility' => array(
		                    'name' => 'status',
		                    'type' => 'select',
		                    'class' => 'required-entry',
		                    'label' => Mage::helper('presscontent')->__('Status'),
		                    'values' => $statuses
		   			)
		    )
		   ) );
		 
		return $this;
	 
		return $this;
	}
  
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('presscontent/presscontent')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
  
    protected function _prepareColumns()
    {
        $this->addColumn('image', array(
            'header'    => Mage::helper('presscontent')->__('Cover Image'),
            'align'     =>'center',
            'width' => '80px',
            'index'     => 'image',
            'renderer' => 'Apa_Presscontent_Block_Adminhtml_Grid_Renderer_Image'
        ));
  
        $this->addColumn('title', array(
            'header'    => Mage::helper('presscontent')->__('Title'),
            'align'     =>'left',
            'width' => '170px',
            'index'     => 'title',
        ));
  
         $this->addColumn('url', array(
            'header'    => Mage::helper('presscontent')->__('Url'),
            'width'     => '200px',
            'index'     => 'url',
        ));
        
         
        $this->addColumn('presscontent', array(
            'header'    => Mage::helper('presscontent')->__('Press Content'),
            'width'     => '300px',
            'index'     => 'presscontent',
        ));
        
  
         $this->addColumn('published', array(
            'header'    => Mage::helper('presscontent')->__('Article Publish Date'),
            'width'     => '200px',
            'index'     => 'published',
        ));
        
  
         $this->addColumn('publishdate', array(
            'header'    => Mage::helper('presscontent')->__('Article Publish Date (for ordering)'),
            'width'     => '70px',
            'align'     => 'left',
            'type'      => 'datetime',
            'index'     => 'publishdate',
        ));
        
  
        $this->addColumn('update_time', array(
            'header'    => Mage::helper('presscontent')->__('Update Time'),
            'align'     => 'left',
            'width'     => '8px',
            'type'      => 'datetime',
            'default'   => '--',
            'index'     => 'update_time',
        ));  
  
  
        $this->addColumn('status', array(
  
            'header'    => Mage::helper('presscontent')->__('Status'),
            'align'     => 'left',
            'width'     => '80px',
            'index'     => 'status',
            'type'      => 'options',
            'options'   => array(
                1 => 'Active',
                2 => 'Inactive',
            ),
        ));
  
        return parent::_prepareColumns();
    }
  
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
  
    public function getGridUrl()
    {
      return $this->getUrl('*/*/grid', array('_current'=>true));
    }
  
  
}
