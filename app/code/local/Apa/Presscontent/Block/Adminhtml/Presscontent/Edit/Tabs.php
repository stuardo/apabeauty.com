<?php
  
class Apa_Presscontent_Block_Adminhtml_Presscontent_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
  
    public function __construct()
    {
        parent::__construct();
        $this->setId('presscontent_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('presscontent')->__('Content Information'));
    }
  
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('presscontent')->__('Content Information'),
            'title'     => Mage::helper('presscontent')->__('Content Information'),
            'content'   => $this->getLayout()->createBlock('presscontent/adminhtml_presscontent_edit_tab_form')->toHtml(),
        ));
        
        return parent::_beforeToHtml();
    }
}
