<?php  
class Apa_Presscontent_Block_Adminhtml_Presscontent_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    
    public function __construct()
    {
        parent::__construct();
                
        $this->_objectId = 'id';
        $this->_blockGroup = 'presscontent';
        $this->_controller = 'adminhtml_presscontent';
  
        $this->_updateButton('save', 'label', Mage::helper('presscontent')->__('Save Content'));
        $this->_updateButton('delete', 'label', Mage::helper('presscontent')->__('Delete Content'));
    }
    
    public function getHeaderText()
    {
        if( Mage::registry('presscontent_data') && Mage::registry('presscontent_data')->getId() ) {
            return Mage::helper('presscontent')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('presscontent_data')->getTitle()));
        } else {
            return Mage::helper('presscontent')->__('Add Content');
        }
    }
}
