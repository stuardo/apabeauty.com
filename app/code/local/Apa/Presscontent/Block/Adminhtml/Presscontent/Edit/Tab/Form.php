<?php
class Apa_Presscontent_Block_Adminhtml_Presscontent_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareLayout()
    {  
        $return = parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        return $return;
     }
   
    protected function _prepareForm()
    {
    	  $UrlData=$this->getRequest()->getParams();
		  $CheckEdit=$UrlData['id'];
	
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('presscontent_form', array('legend'=>Mage::helper('presscontent')->__('Content information')));
        
    	$URLID=$this->getRequest()->getParam('id');
    	$_edited_images = Mage::getModel('presscontent/presscontent')->load($URLID);
    	$_edited_images = ($_edited_images->getdata());
    	$images=$_edited_images['image'];
      $image=explode("/",$images);
      
       $fieldset->addType('image', Mage::getConfig()->getBlockClassName('Apa_Presscontent_Block_Adminhtml_Presscontent_Helper_Image'));
        
        $fieldset->addField('title', 'text', array(
            'label'     => Mage::helper('presscontent')->__('Title'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'title',
        	'maxlength' => 30,
        	
        ));
        
        $fieldset->addField('cover', 'image', array(
            'label'     => Mage::helper('presscontent')->__('Cover Image'),
            'mulitple'  => true,
            'class'     => 'required-file',
            'class'     => 'large-image',
            'required' => true,
		      'name'	=>'cover',
        ));
        
        
        if(isset($CheckEdit))
		  {
		  $htmlstr = '<div style="padding-top:5px;padding-bottom:5px" id="imagetag">';
			foreach($image as $item) {
		    $htmlstr.='<a href="'.Mage::getBaseUrl('media') . 'presscontent'.DS.$item.'" > <img style="padding:5px" src="'.Mage::getBaseUrl('media') . 'presscontent'.DS.$item.'" width=50px height=40px/> </a>';
		   }
		   
		  $htmlstr .= '</div>';
			
			 	$fieldset->addField('image', 'image', array(
		     'label'     => Mage::helper('presscontent')->__('Images'),
		     'class'     => 'required-entry required-file',
		     'required'  => true,
		     'multiple'  => 'multiple',
		     'mulitple'  => true,
		     'name'      => 'image[]',
		     'after_element_html' => $htmlstr, 
			 ));
		}
		else
		{
	 		$fieldset->addField('image', 'image', array(
		     'label'     => Mage::helper('presscontent')->__('Images'),
		     'class'     => 'required-entry required-file',
		     'required'  => true,
		     'multiple'  => 'multiple',
		     'mulitple'  => true,
		     'name'      => 'image[]',
			'after_element_html' => '<small><br>File Type: <i>*.jpg,*.png,*.gif,*.jpeg</i></small>',
		 	));
		}
	
        $fieldset->addField('presscontent', 'editor', array(
            'name'      => 'presscontent',
            'label'     => Mage::helper('presscontent')->__('Content'),
            'title'     => Mage::helper('presscontent')->__('Content'),
            'style'     => 'width:98%; height:400px;',
            'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
            'wysiwyg'   => true,
            'required'  => false,
        ));
        
        $fieldset->addField('url', 'text', array(
            'label'     => Mage::helper('presscontent')->__('Magazine URL'),
            'name'      => 'url',
        	'class'		=>'validate-url',
        	'after_element_html' =>'<p class="nm"><small>'.' Enter URL like <b> http://www.google.com/ </b> '.'</small></p>',
        ));
        

        $fieldset->addField('published', 'text', array(
            'label'     => Mage::helper('presscontent')->__('Article Publish Date'),
//            'class'     => 'required-entry',
            'required'  => false,
            'name'      => 'published',
        	'maxlength' => 255,
        	
        ));

        
      $dateTimeFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);  
      		$fieldset->addField('publishdate', 'datetime', array(
            'label'     => Mage::helper('presscontent')->__('Article Publish Date'),
             'class'     => 'required-entry validate-date',
            'required'  => true,
            'name'      => 'publishdate',
            'format' => $dateTimeFormatIso,
            "time" => true, 
      	    		'image' => $this->getSkinUrl('images/grid-cal.gif')
        ));


        $fieldset->addField('status', 'select', array(
            'label'     => Mage::helper('presscontent')->__('Status'),
            'name'      => 'status',
            'values'    => array(
                array(
                    'value'     => 1,
                    'label'     => Mage::helper('presscontent')->__('Active'),
                ),
  
                array(
                    'value'     => 2,
                    'label'     => Mage::helper('presscontent')->__('Inactive'),
                ),
            ),
        ));
        
       
        
        
        
        if ( Mage::getSingleton('adminhtml/session')->getPresscontentData() )
        {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getPresscontentData());
            Mage::getSingleton('adminhtml/session')->setPresscontentData(null);
        } elseif ( Mage::registry('presscontent_data') ) {
            $form->setValues(Mage::registry('presscontent_data')->getData());
        }
        return parent::_prepareForm();
    }
}

