<?php
class Apa_Presscontent_Block_Presscontent extends Mage_Core_Block_Template {
    private $_itemPerPage = 20;
    private $_pageFrame = 4;
    private $_curPage = 1;
    
    public function getCollection($collection = 'null')
    {
    
        if($collection != 'null'){ 
            $page = $this->getRequest()->getParam('p');
            
            if($page) { $this->_curPage = $page; }
            
            $clone1= clone $collection;
            $count_max= ceil($clone1->count() / $this->_itemPerPage);

				if($this->_curPage > $count_max)
				{
					$url=Mage::getBaseUrl()."presscontent".DS."index".DS."page?p=1";	
					Mage::app()->getFrontController()->getResponse()->setRedirect($url);
				}
				
            $collection->setCurPage($this->_curPage);
            $collection->setPageSize($this->_itemPerPage);
            return $collection;
        }
    }
    
    public function getPagerHtml($collection = 'null')
    {    
        $html = false;
        if($collection == 'null') return;
        if($collection->count() > $this->_itemPerPage)
        {
            $curPage = $this->getRequest()->getParam('p');
            $pager = (int)($collection->count() / $this->_itemPerPage);
            $count = ($collection->count() % $this->_itemPerPage == 0) ? $pager : $pager + 1 ;

            $url = $this->getPagerUrl();
            $start = 1;
            $end = $this->_pageFrame;
           
            $html .= '<ol>';
            if(isset($curPage) && $curPage != 1){
                $start = $curPage - 1;                                        
                $end = $start + $this->_pageFrame;
            }else{
                $end = $start + $this->_pageFrame;
            }
            if($end > $count){
                $start = $count - ($this->_pageFrame-1);
            }else{
                $count = $end-1;
            }
            

           
            for($i = $start; $i<=$count; $i++)
            {
                if($i >= 1){
                    if($curPage){
                        $html .= ($curPage == $i) ? '<li class="button" class="current">'. $i .'</li>' : '<li><a href="'.$url.'?p='.$i.'">'. $i .'</a></li>';
                    }else{
                        $html .= ($i == 1) ? '<li class="button" class="current">'. $i .'</li>' : '<li><a href="'.$url.'?p='.$i.'">'. $i .'</a></li>';
                    }
                }
                
            }
            
            $html .= '</ol>';
        }

        return $html;
        
        
    }
    
    public function getPagerUrl()   // You need to change this function as per your url.
    {
        $cur_url = mage::helper('core/url')->getCurrentUrl();
        $new_url = preg_replace('/\?p=.*/', '', $cur_url);
        
        return $new_url;
    }
}
