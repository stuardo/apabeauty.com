<?php
  
$installer = $this;
  
$installer->startSetup();
  
$installer->run("
  
-- DROP TABLE IF EXISTS {$this->getTable('presscontent')};
CREATE TABLE {$this->getTable('presscontent')} (
  `presscontent_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `cover` varchar(255) NOT NULL default '',
  `image` varchar(255) NOT NULL default '',
  `presscontent` text NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `url` varchar(255) NOT NULL default '',
  `published` varchar(255) NOT NULL default '',
  `update_time` datetime NULL,
  PRIMARY KEY (`presscontent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  
    ");
  
$installer->endSetup();
