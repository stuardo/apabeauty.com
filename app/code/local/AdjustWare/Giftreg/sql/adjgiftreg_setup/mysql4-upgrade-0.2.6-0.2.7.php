<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
$installer = $this;

$installer->startSetup();

$installer->run('

ALTER TABLE `'.$this->getTable('adjgiftreg/item').'` ADD COLUMN `sku_product_id` INT UNSIGNED NOT NULL DEFAULT 0;

UPDATE `'.$this->getTable('adjgiftreg/item').'`
SET sku_product_id = product_id;

');

$installer->endSetup();