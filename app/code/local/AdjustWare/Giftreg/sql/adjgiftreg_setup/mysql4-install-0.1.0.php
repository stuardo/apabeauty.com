<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('adjgiftreg/event')};

CREATE TABLE {$this->getTable('adjgiftreg/event')} (
  `event_id` int(10) unsigned NOT NULL auto_increment,
  `customer_id` int(10) unsigned NOT NULL default '0',
  `address_id` int(10) unsigned NOT NULL,
  `type_id` smallint(5) unsigned NOT NULL,
  `status` enum('active','expired','removed') NOT NULL default 'active',
  `sharing_code` char(32) NOT NULL,
  `date` date NOT NULL,
  `search_allowed` tinyint(1) unsigned NOT NULL,
  `pass` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `fname2` varchar(255) NOT NULL,
  `lname2` varchar(255) NOT NULL,
  `emails` text NOT NULL,
  PRIMARY KEY  (`event_id`),
  KEY `FK_CUSTOMER` (`customer_id`),
  KEY `sharing_code` (`sharing_code`),
  KEY `address_id` (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS {$this->getTable('adjgiftreg/item')};
CREATE TABLE {$this->getTable('adjgiftreg/item')} (
  `item_id` int(10) unsigned NOT NULL auto_increment,
  `event_id` int(10) unsigned NOT NULL default '0',
  `product_id` int(10) unsigned NOT NULL default '0',
  `store_id` smallint(5) unsigned NOT NULL,
  `hide` tinyint(4) unsigned NOT NULL default '0',
  `num_has` smallint(5) unsigned NOT NULL,
  `num_wants` smallint(5) unsigned NOT NULL,
  `priority` tinyint(1) unsigned NOT NULL default '0',
  `added_at` datetime NOT NULL,
  `descr` varchar(255) NOT NULL,
  PRIMARY KEY  (`item_id`),
  KEY `FK_GIFT_EVENT` (`event_id`),
  KEY `FK_GIFT_PRODUCT` (`product_id`),
  KEY `FK_GIFT_STORE` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS {$this->getTable('adjgiftreg/type')};
CREATE TABLE {$this->getTable('adjgiftreg/type')} (
  `type_id` int(10) unsigned NOT NULL auto_increment,
  `pos` smallint(5) unsigned NOT NULL,
  `hide` tinyint(1) unsigned NOT NULL,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY  (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS {$this->getTable('adjgiftreg/type_title')};
CREATE TABLE {$this->getTable('adjgiftreg/type_title')} (
  `type_id` int(10) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY  (`type_id`,`store_id`),
  KEY `store_id` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS {$this->getTable('adjgiftreg/thank')};
CREATE TABLE {$this->getTable('adjgiftreg/thank')} (
  `thank_id` int(10) unsigned NOT NULL auto_increment,
  `event_id` int(10) unsigned NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `qty` double NOT NULL,
  `purchased_at` datetime default NULL,
  `giver_name` varchar(255) NOT NULL,
  PRIMARY KEY  (`thank_id`),
  KEY `event_id` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('adjgiftreg/order')};
CREATE TABLE {$this->getTable('adjgiftreg/order')} (
  `id` int(10) unsigned NOT NULL auto_increment,
  `order_id` int(10) unsigned NOT NULL,
  `event_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `event_id` (`event_id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$fields1 = $installer->getConnection()->fetchAll("SHOW COLUMNS FROM {$this->getTable('sales/quote')} LIKE 'adjgiftreg_event%'");
    if (empty($fields1)) 
    {
    	$installer->run("
        ALTER TABLE {$this->getTable('sales/quote')} ADD `adjgiftreg_event_id` INT UNSIGNED NOT NULL DEFAULT '0';
        ");
    }
$fields2 = $installer->getConnection()->fetchAll("SHOW COLUMNS FROM {$this->getTable('sales/quote_item')} LIKE 'adjgiftreg_item%'");
    if (empty($fields2)) 
    {
    	$installer->run("
        ALTER TABLE {$this->getTable('sales/quote_item')} ADD `adjgiftreg_item_id` INT UNSIGNED NOT NULL DEFAULT '0';
        ");
    }

$installer->endSetup();