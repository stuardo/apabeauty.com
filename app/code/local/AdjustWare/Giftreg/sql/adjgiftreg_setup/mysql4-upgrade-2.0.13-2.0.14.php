<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
$installer = $this;

$installer->startSetup();

$installer->run('

ALTER TABLE `'.$this->getTable('adjgiftreg/thank').'` ADD `order_id` INT UNSIGNED NULL ,
ADD `product_id` INT UNSIGNED NULL ;

');

$installer->run('
ALTER TABLE `'.$this->getTable('adjgiftreg/item').  '` ADD `num_total` INT NOT NULL DEFAULT \'0\';
');

$installer->endSetup();