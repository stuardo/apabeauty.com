<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
$installer = $this;

$installer->startSetup();

$installer->run('

ALTER TABLE `'.$this->getTable('adjgiftreg/item').'` ADD `num_user_set` SMALLINT(5) DEFAULT \'0\';

ALTER TABLE `'.$this->getTable('adjgiftreg/item').'` CHANGE `num_has` `num_has` SMALLINT(5) NOT NULL;
');

$installer->endSetup();