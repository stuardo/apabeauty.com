<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE `{$this->getTable('adjgiftreg/item')}` ADD `buy_request` TEXT NOT NULL ;

DROP TABLE IF EXISTS `{$this->getTable('adjgiftreg/item_option')}`;
CREATE TABLE `{$this->getTable('adjgiftreg/item_option')}` (
  `option_id` int(10) unsigned NOT NULL auto_increment,
  `item_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `qty` decimal(12,4) default '0.0000',
  `code` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY  (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();