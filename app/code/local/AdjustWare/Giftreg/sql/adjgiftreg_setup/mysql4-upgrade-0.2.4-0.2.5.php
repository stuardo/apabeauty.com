<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
$installer = $this;

$installer->startSetup();

$installer->run("

ALTER TABLE {$this->getTable('adjgiftreg/item')} ADD CONSTRAINT `FK_GIFT_ITEM_PRODUCT` FOREIGN KEY ( `product_id` ) REFERENCES {$this->getTable('catalog/product')} ( `entity_id` ) ON UPDATE CASCADE ON DELETE CASCADE; 
 ALTER TABLE {$this->getTable('adjgiftreg/item_option')} ADD CONSTRAINT `FK_GIFT_ITEM_OPTION_PRODUCT` FOREIGN KEY ( `product_id` ) REFERENCES {$this->getTable('catalog/product')}( `entity_id` ) ON UPDATE CASCADE ON DELETE CASCADE;

");

$installer->endSetup();