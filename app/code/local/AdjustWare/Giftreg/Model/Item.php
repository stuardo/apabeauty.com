<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Model_Item extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('adjgiftreg/item');
    }

    public function loadBy($eventId, $productId)
    {
        $data = $this->_getResource()->loadBy($eventId, $productId);
        $this->setData($data);
        return $this;
    }
    
    public function canBeMovedTo($eventId){
        $res = true;
        if ($this->_getResource()->loadBy($eventId, $this->getProductId()))
            $res = false;
            
        return $res;
    }
    
    public function getOptions() {
        $itemId = $this->getId() ? $this->getId() : '';
        $optionCollection = Mage::getModel('adjgiftreg/item_option')->getCollection()
            ->addFieldToFilter('item_id', $itemId);
        
        $options = array();
        foreach ($optionCollection as $option) {
        	if ($option->getItemId() == $itemId) {
        	    $options[] = $option;
        	}
        }
        return $options;
    } 

    public function getOptionByCode($code)
    {
        $options = $this->getOptions();
        if(!empty($options))
        {
            foreach($options as $option)
            {
                if($option->getData('code') == $code)
                {
                    return $option;
                }    
            }         
        }
        return false;
    }
    
    public function importOldItems()
    {
        $itemCollection = Mage::getModel('adjgiftreg/item')->getCollection();
        /* @var $itemCollection AdjustWare_Giftreg_Model_Mysql4_Item_Collection */

        $limit        = 1;
        $offset       = 0;
        $productTypes = array();
        $walkItems    = clone $itemCollection;
        $walkItems->getSelect()->limit($limit, $offset);

        if (!$walkItems->getSize())
        {
            return false;
        }

        while ($offset < $walkItems->getSize())
        {
            foreach ($walkItems as $item)
            {
                /* @var $item AdjustWare_Giftreg_Model_Item */

                if (!isset($productTypes[$item->getProductId()]))
                {
                    $productTypes[$item->getProductId()] = Mage::getModel('catalog/product')->load($item->getProductId())->getTypeId();
                }
        
                if ('configurable' == $productTypes[$item->getProductId()])
                {
                    foreach ($item->getOptions() as $option)
                    {
                        /* @var $option AdjustWare_Giftreg_Model_Item_Option */
        
                        if ('simple_product' == $option->getCode())
                        {
                            $item->setSkuProductId($option->getValue())->save();
                            break;
                        }
                    }
                }
            }

            $offset   += $limit;
            $walkItems = clone $itemCollection;
            $walkItems->getSelect()->limit($limit, $offset);
        }
    }
}