<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Model_Mysql4_Event_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('adjgiftreg/event');
    }
    
    public function addSearchCriteria($criteria){
        $sel = $this->getSelect()
            ->where('status != ?', 'removed');
        if(Mage::helper('adjgiftreg')->usePrivateStatusForRegistries())
        {
            $sel->where('search_allowed = 1');
        }
        if(Mage::getStoreConfig('adjgiftreg/general/scope'))
        {
            $customerTable = $this->getTable('customer/entity');
            $websiteId = Mage::app()->getStore()->getWebsiteId();
            $sel
                ->join(array('cust' => $customerTable), '(cust.entity_id=main_table.customer_id) AND (cust.website_id ='.$websiteId.')',array());
        }
        if (!empty($criteria['fname'])){
            $sel->where('(fname LIKE ?', '%'.$criteria['fname'].'%')
                ->orWhere('fname2 LIKE ?)', '%'.$criteria['fname'].'%'); 
        }
        if (!empty($criteria['lname'])){
            $sel->where('(lname LIKE ?', '%'.$criteria['lname'].'%')
                ->orWhere('lname2 LIKE ?)', '%'.$criteria['lname'].'%'); 
        }
        if (!empty($criteria['mon'])){
            $mon = intVal($criteria['mon']);
            if ($mon >=1 && $mon <=12)
                $sel->where(new Zend_Db_Expr('MONTH(date) = ' . $mon));
        }
        return $this;
    }
    
    public function addCustomerFilter($customerId, $hideRemoved=true){
        $this->addFieldToFilter('customer_id', $customerId);
        if ($hideRemoved)
            $this->addFieldToFilter('status', array('in'=>array('active','expired')));
        
        return $this;
    }
    
    public function addShouldBeExpiredFilter(){
        $this->addFieldToFilter('status', 'active');
        $this->addFieldToFilter('date', array('to' => date('Y-m-d'), 'date'=>true));
        return $this;
    }
        
}