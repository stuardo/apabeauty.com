<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Model_Mysql4_Item_Option_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('adjgiftreg/item_option');
    }

    public function addItemFilter($item)
    {
        if (empty($item)) {
            $this->addFieldToFilter('item_id', '');
        }
        elseif (is_array($item)) {
            $this->addFieldToFilter('item_id', array('in'=>$item));
        }
        elseif ($item instanceof Mage_Sales_Model_Quote_Item) {
        	$this->addFieldToFilter('item_id', $item->getId());
        }
        else {
            $this->addFieldToFilter('item_id', $item);
        }
        return $this;
    }

    /**
     * Get array of all product ids
     *
     * @return array
     */
    public function getProductIds()
    {
        $ids = array();
        foreach ($this as $item) {
        	$ids[] = $item->getProductId();
        }
        return array_unique($ids);
    }

    /**
     * Get all option for item
     *
     * @param   mixed $item
     * @return  array
     */
    public function getOptionsByItem($item)
    {
        if ($item instanceof Mage_Sales_Model_Quote_Item) {
            $itemId = $item->getId();
        }
        else {
            $itemId = $item;
        }

        $options = array();
        foreach ($this as $option) {
        	if ($option->getItemId() == $itemId) {
        	    $options[] = $option;
        	}
        }
        return $options;
    }

    /**
     * Get all option for item
     *
     * @param   mixed $item
     * @return  array
     */
    public function getOptionsByProduct($product)
    {
        if ($product instanceof Mage_Catalog_Model_Product) {
            $productId = $product->getId();
        }
        else {
            $productId = $product;
        }

        $options = array();
        foreach ($this as $option) {
        	if ($option->getProductId() == $productId) {
        	    $options[] = $option;
        	}
        }
        return $options;
    }
}