<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Model_Mysql4_Item_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    
    public function _construct()
    {
        $this->_init('adjgiftreg/item');
    }
    
    protected function _addVisibityFilter(){
        return $this;        
    }
    
    public function addEventFilter($id)
    {
        $this->getSelect()->where('event_id = ?', $id);
        return $this->_addVisibityFilter();

    }
    
    public function addPopularFilter(){
        $this->getSelect()
            ->from(null, array('cnt'=> new Zend_Db_Expr('COUNT(*)')))
            ->group('product_id')
            ->order('cnt DESC');
            
        return $this->_addVisibityFilter(); 
    }
    
    public function addEventFilterForAdmin($id){
        $this->getSelect()->where('event_id = ?', $id);
        
        return $this; 
    }
   
    protected function _afterLoad()
    {
        parent::_afterLoad();

        $prIds = array();
        foreach ($this->_items as $item) 
        {
            $prIds[] = $item->getProductId();
        }
        if(count($prIds))
        {
            $productCollection = Mage::getModel('catalog/product')->getCollection();
            $store = Mage::app()->getStore();
            $productCollection->joinAttribute(
                'price',
                'catalog_product/price',
                'entity_id',
                null,
                'left',
                $store->getId()
            );
            $productCollection->addAttributeToSelect(array('small_image','name'));
            $productCollection->getSelect()->where('`e`.`entity_id` IN (?)', $prIds);
            foreach ($this->_items as $item) 
            {
            
                $product = $productCollection->getItemById($item->getProductId());
                $item->setProduct($product);
                $item->setProductName($product->getName());
            }
        }
        return $this;
    }
    
    
    public function hideReceived(){
        $i = $this->getTable('adjgiftreg/item');
        $this->getSelect()->where($i . '.num_has < ' . $i .'.num_wants');
        return $this;
    } 
    
    public function setPriorityOrder(){
        $i = $this->getTable('adjgiftreg/item');
        $this->getSelect()->order($i.'.priority DESC');
        return $this;
    }

    public function joinSku()
    {
        $this->getSelect()->join(
            array('p' => $this->getTable('catalog/product')), 
            'main_table.sku_product_id = p.entity_id', 
            array('sku' => 'p.sku')
            );

        return $this;
    }
}