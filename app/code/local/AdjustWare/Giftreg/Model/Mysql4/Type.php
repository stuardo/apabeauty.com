<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Model_Mysql4_Type extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('adjgiftreg/type', 'type_id');
    }
    
    protected function _afterSave(Mage_Core_Model_Abstract $object) {
        parent::_afterSave($object);

        $titles = $object->getTitles();
        if (is_array($titles) && sizeof($titles)) {
            $db = $this->_getWriteAdapter();
            try {
                $db->beginTransaction();
                $db->delete($this->getTable('type_title'), $db->quoteInto('type_id = ?', $object->getId()));
                foreach ($titles as $storeId => $value) {
                    $value = trim($value);
                    if (!$value) 
                        continue;
                    
                    $data = new Varien_Object();
                    $data->setTypeId($object->getId())
                        ->setStoreId($storeId)
                        ->setValue($value);
                    $db->insert($this->getTable('type_title'), $data->getData());
                }
                $this->_getWriteAdapter()->commit();
            }
            catch (Exception $e) {
                $db->rollBack();
            }
        }
        
        return $this;
    }
    
    protected function _afterLoad(Mage_Core_Model_Abstract $object) {
        parent::_afterLoad($object);
        
        $select = $this->_getReadAdapter()->select()
            ->from($this->getTable('type_title'))
            ->where('type_id=?', $object->getId());
        $data = $this->_getReadAdapter()->fetchAll($select);
        
        $storeTitles = array();
        foreach ($data as $row) {
            $storeTitles[$row['store_id']] = $row['value'];
        }
        $object->setTitles($storeTitles);
        
        return $this;
    }
 
}