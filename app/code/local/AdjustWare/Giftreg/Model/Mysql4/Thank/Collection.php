<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Model_Mysql4_Thank_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('adjgiftreg/thank');
    }
    
    public function addEventFilter($id) {
        $this->getSelect()->where('main_table.event_id = ?', $id);
        return $this;
    } 
    
    public function addOrderFilter($id) {
        $this->getSelect()->where('main_table.order_id = ?', $id);
        return $this;
    } 

    /** Loads standard Magento Gift Messages assigned to orders and order items
     * 
     * @return AdjustWare_Giftreg_Model_Mysql4_Thank_Collection
     */
    public function loadGiftMessages()
    {
        $orderIds       = array();
        $thanks         = array();
        $thanksProducts = array();
    
        foreach ($this as $thank)
        {
            if ($thank->getOrderId())
            {
                $orderIds[$thank->getOrderId()][]                             = $thank->getId();
                $thanks[$thank->getOrderId()][$thank->getId()]                = $thank;
                $thanksProducts[$thank->getOrderId()][$thank->getProductId()] = $thank;
            }
        }

        if (count($orderIds))
        {
            // Load sales collection by thanks collection
            $sales = Mage::getModel('sales/order')->getCollection()
                ->addFieldToFilter('entity_id', array('in' => array_keys($orderIds)));

            if ($sales instanceof Mage_Eav_Model_Entity_Collection_Abstract)
            {
                $sales->addAttributeToSelect('gift_message_id');
            }

            $order2messages = array();
            foreach ($sales as $sale)
            {
                if ($sale->getGiftMessageId())
                {
                    $order2messages[$sale->getGiftMessageId()] = $sale->getId();
                }
            }

            // Load gift messages collection by sales collection
            $giftMessages = Mage::getModel('giftmessage/message')->getCollection()
                ->addFieldToFilter('gift_message_id', array('in' => array_keys($order2messages)))
                ->load();

            // Assign sales gift messages for thanks items
            foreach ($giftMessages as $message)
            {
                foreach ($thanks[$order2messages[$message->getId()]] as $thank)
                {
                    $thank->setOrderGiftMessage($message->getMessage());
                }
            }

            // Load sales items collection by thanks collection
            $salesItems = Mage::getModel('sales/order_item')->getCollection()
                ->addFieldToFilter('order_id', array('in' => array_keys($orderIds)))
                ->load();

            $orderItem2messages = array();
            foreach ($salesItems as $saleItem)
            {
                if ($saleItem->getGiftMessageId())
                {
                    $orderItem2messages[$saleItem->getGiftMessageId()] = array(
                        $saleItem->getOrderId(), 
                        $saleItem->getProductId()
                        );
                }
            }

            // Load gift messages collection by sales items collection
            $giftMessages = Mage::getModel('giftmessage/message')->getCollection()
                ->addFieldToFilter('gift_message_id', array('in' => array_keys($orderItem2messages)))
                ->load();

            // Assign sales gift messages for thanks items
            foreach ($giftMessages as $message)
            {
                list($orderId, $productId) = $orderItem2messages[$message->getId()];

                if (isset($thanksProducts[$orderId][$productId]))
                {
                    $thanksProducts[$orderId][$productId]->setOrderItemGiftMessage($message->getMessage());
                }
            }
        }

        return $this;
    }
}