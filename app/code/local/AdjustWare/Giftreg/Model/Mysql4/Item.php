<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Model_Mysql4_Item extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('adjgiftreg/item', 'item_id');
    }

    public function loadBy($eventId, $productId)
    {
        $select = $this->_getReadAdapter()->select()
            ->from(array('main_table' => $this->getTable('item')))
            ->where('main_table.event_id = ?',  $eventId)
            ->where('main_table.product_id = ?',  $productId);
           // ->where('main_table.store_id in (?)',  $sharedStores);
        $row = $this->_getReadAdapter()->fetchRow($select);
        return $row;
    }
    
    public function removeByEvent($eventId)
    {
//        $sql = 'UPDATE ' . $this->getMainTable() . ' SET hide=1'
//             . ' WHERE event_id = ' . intVal($eventId);
//        $this->_getWriteAdapter()->query($sql);
        return true;
    }
    
    protected function _afterDelete(Mage_Core_Model_Abstract $object)
    {
        parent::_afterDelete($object);
        
        // we do not use foreign keys because quite a lot of clients
        // run magento with MyIsam 
        $sql = 'DELETE FROM ' . $this->getTable('item_option') 
             . ' WHERE item_id = ' . intVal($object->getId());
        $this->_getWriteAdapter()->query($sql);
        
        return $this;
    }

}