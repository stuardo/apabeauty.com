<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Model_Mysql4_Product_Collection extends Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
{
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->_joinFields['e_id'] = array('table'=>'e','field'=>'entity_id');
        $this->joinTable('adjgiftreg/item',
            'product_id=e_id',
            array(
                'item_id'      => 'item_id',
                'product_id'   => 'product_id',
                'gift_descr'   => 'descr',
                'store_id'     => 'store_id',
                'added_at'     => 'added_at',
                'event_id'     => 'event_id',
                'num_wants'    => 'num_wants',
                'num_has'      => 'num_has',
                'priority'     => 'priority',
                'buy_request'  => 'buy_request',
            )
        );
        return $this;
    } 
    
    protected function _addVisibityFilter()
    {
        $this->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes());
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($this);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($this);
        return $this;
    }
    
    public function addPopularFilter()
    {
        $this->getSelect()
            ->from(null, array('cnt'=> new Zend_Db_Expr('COUNT(*)')))
            ->group('entity_id')
//            ->group($this->getTable('adjgiftreg/item').'.'.'product_id')
            ->order('cnt DESC');
            
        return $this->_addVisibityFilter(); 
    }
    
    public function getSelectCountSql()
    {
        return parent::getSelectCountSql()->reset(Zend_Db_Select::GROUP);
    }
    
}