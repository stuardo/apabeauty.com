<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Model_Mysql4_Type_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('adjgiftreg/type');
    }
    
    public function addTitles($storeId) {
        $this->getSelect()->joinLeft(
            array('t'=>$this->getTable('type_title')),
            'main_table.type_id=t.type_id AND t.store_id = '. intVal($storeId),
             array('IFNULL(t.value, main_table.code) AS title'));
             
        return $this;
    } 
    
    public function addVisibilityFilter($showHidden) {
        if (!$showHidden)
            $this->getSelect()->where('hide=0');
        return $this;
    } 
    
}