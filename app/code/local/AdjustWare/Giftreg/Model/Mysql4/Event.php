<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Model_Mysql4_Event extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('adjgiftreg/event', 'event_id');
    }
    
    public function getLastEventId($customerId){
        $select = $this->_getReadAdapter()->select()
            ->from($this->getMainTable(), 'event_id')
            ->where('customer_id = ?',  $customerId)
            ->where('status = ?',  'active')
            ->order('event_id DESC')
            ->limit(1);
        return $this->_getReadAdapter()->fetchOne($select); 
    }

}