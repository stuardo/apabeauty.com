<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Model_Event extends Mage_Core_Model_Abstract {

    private $_storeId = null;
    private $_storeIds = null;
    private $_processedEvents = array();

    protected function _construct() {
        $this->_init('adjgiftreg/event');
    }

    public function ifGetOptionsHtml($item) {

        $sBuyRequest = $item->getBuyRequest();
        $aBuyRequest = null;
        if ($sBuyRequest) {
            $aBuyRequest = unserialize($sBuyRequest);
        }

        $typeInstance = $item->getProduct()->getTypeInstance(true);

        switch ($item->getProduct()->getTypeId()) {
            case 'bundle':
                if ($aBuyRequest AND !empty($aBuyRequest['bundle_option'])) {

                    $optionsCollection = $typeInstance->getOptionsByIds(array_keys($aBuyRequest['bundle_option']), $item->getProduct());

                    $beginNumberOfoptionsExcludeEmpty = 0;
                    foreach ($aBuyRequest['bundle_option'] as $k => $v) {
                        if (!empty($v)) {
                            $beginNumberOfoptionsExcludeEmpty++;
                        }
                    }

                    $aSelectionIds = array();

                    foreach ($aBuyRequest['bundle_option'] as $mValue) {
                        if (is_array($mValue)) {
                            foreach ($mValue as $mSubValue) {
                                if ($mSubValue) {
                                    $aSelectionIds[] = $mSubValue;
                                }
                            }
                        } elseif ($mValue) {
                            $aSelectionIds[] = $mValue;
                        }
                    }

                    $selectionsCollection = $typeInstance->getSelectionsByIds(
                            $aSelectionIds, $item->getProduct()
                    );

                    $bundleOptions = $optionsCollection->appendSelections($selectionsCollection, true);

                    $countAvailableOptions = 0;

                    foreach ($bundleOptions as $bundleOption) {
                        if ($bundleOption->getSelections()) {
                            $countAvailableOptions++;
                        }
                    }
                    if ($beginNumberOfoptionsExcludeEmpty !== $countAvailableOptions) {
                        return false;
                    }
                }

                break;
        }
        return true;
    }

    // get array of visible occasions
    public function getTypes() {
        return Mage::helper('adjgiftreg')->getOccasions(false);
    }

    public function getProductCollection($store_id = null) {
        $collection = $this->getData('item_collection');
        if (is_null($collection)) {
            $collection = Mage::getResourceModel('adjgiftreg/item_collection')
                ->addEventFilter($this->getId());
            $filters['store_id'] = !is_null($store_id) ? $store_id : $this->getStoreId();
            $conditions = array(
                'cat_index.product_id=main_table.sku_product_id',
                $collection->getConnection()->quoteInto('cat_index.store_id=?', $filters['store_id'])
            );
            $joinCond = join(' AND ', $conditions);

            $collection->getSelect()->join(
                    array('cat_index' => $collection->getTable('catalog/category_product_index')), $joinCond, array()
            );

            $joinCond = array();
            $conditions = array(
                'cat_indexv.product_id=main_table.product_id',
                $collection->getConnection()->quoteInto('cat_indexv.store_id=?', $filters['store_id'])
            );

            $filters['visibility'] = Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds();
            $conditions[] = $collection->getConnection()
                ->quoteInto('cat_indexv.visibility IN(?)', $filters['visibility']);
            $joinCond = join(' AND ', $conditions);
            $collection->getSelect()->join(
                    array('cat_indexv' => $collection->getTable('catalog/category_product_index')), $joinCond, array()
            );

            $joinCond = '`cat_pr`.`entity_id`=`main_table`.`sku_product_id`'; //join(' AND ', $conditions);

            $collection->joinSku();
            $collection->distinct(true);
            $collection->load();
            foreach ($collection as $k => $item) {
                if (!$this->ifGetOptionsHtml($item)) {
                    $collection->removeItemByKey($k);
                }
            }

            /* @var $collection AdjustWare_Giftreg_Model_Mysql4_Item_Collection */
            $this->setData('item_collection', $collection);
        }
        return $collection;
    }

    protected function _findKeyInArray($array, $keyName) {
//          var_dump($array);
        $flag = false;

        foreach ($array as $k => $v) {
            if (is_array($v))
                $flag = $this->_findKeyInArray($v, $keyName);
            if ($k === $keyName) {
                return $flag = $v;
            }
        }
        return $flag;
    }

    /*
     * @param AdjustWare_Giftreg_Model_Item $item
     * @param Mage_Sales_Model_Order_Item $orderItem
     * @return array
     */
    protected function _updateOrderedCounterBundle($eventItem, $orderItem) {
        $countersData = array('order_counter' => 0,
            'invoice_counter' => 0,
            'continue' => 0);
        if ($orderItem->getProductType() == 'bundle') {
            $eventArray = unserialize($eventItem->getBuyRequest());
            $orderArray = $orderItem->getProductOptions();
            $orderArray = $orderArray['info_buyRequest'];
            if (!isset($eventArray['bundle_option'])
                    or !isset($orderArray['bundle_option'])) {
                $countersData['continue'] = 1;
                return $countersData;
            }
            $newEventArray['product'] = $eventArray['product'];
            $newEventArray['bundle_option'] = $eventArray['bundle_option'];
            $newOrderArray['product'] = $orderArray['product'];
            $newOrderArray['bundle_option'] = $orderArray['bundle_option'];
            if ($newEventArray == $newOrderArray) {
                $countersData['order_counter'] = $orderItem->getQtyOrdered() - $orderItem->getQtyRefunded() - $orderItem->getQtyCanceled();
                $countersData['invoice_counter'] = $orderItem->getQtyInvoiced() - $orderItem->getQtyRefunded();
            }
        }

        return $countersData;
    }

    /*
     * @param AdjustWare_Giftreg_Model_Item $item
     * @param Mage_Sales_Model_Order_Item $orderItem
     * @return array
     */
    protected function _updateOrderedCounterSimpleWithOptions($eventItem, $orderItem) {
        $countersData = array('order_counter' => 0,
            'invoice_counter' => 0,
            'continue' => 0);
        $eventArray = unserialize($eventItem->getBuyRequest());
        $orderArray = $orderItem->getProductOptions();
        $orderArray = $orderArray['info_buyRequest'];

        if (!isset($eventArray['options']) or !isset($orderArray['options'])) {
            $countersData['continue'] = 1;
            return $countersData;
        }
        $newEventArray['options'] = $eventArray['options'];
        $newOrderArray['options'] = $orderArray['options'];
        if ($newEventArray == $newOrderArray) {
            $countersData['order_counter'] = $orderItem->getQtyOrdered() - $orderItem->getQtyRefunded() - $orderItem->getQtyCanceled();
            $countersData['invoice_counter'] = $orderItem->getQtyInvoiced() - $orderItem->getQtyRefunded();
        }

        return $countersData;
    }

    /**
     *
     * @param AdjustWare_Giftreg_Model_Mysql4_Item_Collection $container
     */
    public function updateOrderedCounter($container) {
        foreach ($container as $eventItem) {
            $this->_updateOrderedEvent($eventItem);
        }
    }

    protected function _updateOrderedEvent($eventItem) {
        if($eventItem->getEventId()==0) {
            return false;
        }
        $orderCounter = 0;
        $invoiceCounter = 0;

        //selecting all orders that were purchased for this event
        if(isset($this->_processedEvents[$eventItem->getEventId()])) {
            $orderCollection = $this->_processedEvents[$eventItem->getEventId()];
        } else {
            $orderCollection = Mage::getModel('sales/order')->getCollection();
            $orderCollection->getSelect()
                ->joinInner(
                    array('adj_order'=> $orderCollection->getTable('adjgiftreg/order') ),
                    'adj_order.order_id=main_table.entity_id',
                    array('event_id')
                )
                ->where('adj_order.event_id = ?', $eventItem->getEventId());
            //prevent selecting same orders for one event, if event have several items
            $this->_processedEvents[$eventItem->getEventId()] = $orderCollection;
        }

        foreach ($orderCollection as $order) {
            switch ($order->getState()) {
                case Mage_Sales_Model_Order::STATE_COMPLETE:
                case Mage_Sales_Model_Order::STATE_NEW:
                case Mage_Sales_Model_Order::STATE_PENDING_PAYMENT:
                case Mage_Sales_Model_Order::STATE_PROCESSING:
                case Mage_Sales_Model_Order::STATE_HOLDED:
                    $orderItems = $order->getItemsCollection();

                    foreach ($orderItems as $orderItem) {
                        /* bundle products */
                        if (strstr($eventItem->getBuyRequest(), 'bundle_option')) {
                            $countersData = $this->_updateOrderedCounterBundle($eventItem, $orderItem);
                            if ($countersData['continue'] == 1)
                                continue;
                            $orderCounter += $countersData['order_counter'];
                            $invoiceCounter += $countersData['invoice_counter'];
                        }
                        /* products with custom options */
                        elseif ($eventItem->getProductId() == $orderItem->getProductId() AND $eventItem->getData('sku') == $orderItem->getSku()
                                AND $this->_findKeyInArray($orderItem->getProductOptions(), 'options')) {
                            $countersData = $this->_updateOrderedCounterSimpleWithOptions($eventItem, $orderItem);
                            if ($countersData['continue'] == 1)
                                continue;
                            $orderCounter += $countersData['order_counter'];
                            $invoiceCounter += $countersData['invoice_counter'];
                        }
                        /* configurable products */
                        elseif ($eventItem->getProductId() == $orderItem->getProductId()
                                AND $eventItem->getData('sku') == $this->_findKeyInArray($orderItem->getProductOptions(), 'simple_sku')) {
                            $orderCounter += $orderItem->getQtyOrdered() - $orderItem->getQtyRefunded() - $orderItem->getQtyCanceled();
                            $invoiceCounter += $orderItem->getQtyInvoiced() - $orderItem->getQtyRefunded();
                        }
                        /* no matched configurable products */ elseif ($this->_findKeyInArray($orderItem->getProductOptions(), 'simple_sku')) {
                            continue;
                        }
                        /* simple products */ elseif ($eventItem->getProductId() == $orderItem->getProductId()) {
                            if ($this->_findKeyInArray($orderItem->getProductOptions(), 'bundle_option')) {

                            } else {
                                $orderCounter += $orderItem->getQtyOrdered() - $orderItem->getQtyRefunded() - $orderItem->getQtyCanceled();
                                $invoiceCounter += $orderItem->getQtyInvoiced() - $orderItem->getQtyRefunded();
                            }
                        }
                    }
                    break;
            }
        }

        if ($eventItem->getItemId()) {
            $orderCounter = $orderCounter + $eventItem->getNumUserSet();
            $invoiceCounter = $invoiceCounter + $eventItem->getNumUserSet();

            $eventItem
                ->setNumTotal($orderCounter)
                ->setNumHas($invoiceCounter)
                ->save();
        }
    }

    public function getStoreId() {
        if (is_null($this->_storeId)) {
            $this->_storeId = Mage::app()->getStore()->getId();
        }
        return $this->_storeId;
    }

    /*
     * @param string $productId
     * @param array $customOptions
     * @return integer
     */
    protected function _wasAddedItemId($productId, $customOptions) {
        if ($customOptions) {
            foreach ($customOptions as $product) {
                $options = $product->getCustomOptions();

                foreach ($options as $option) {
                    if ($option->getProductId() == $productId && $option->getCode() == 'info_buyRequest') {
                        $aNewRequest = unserialize($option->getValue());

                        unset($aNewRequest['event']);
                        unset($aNewRequest['qty']);

                        if (isset($aNewRequest['super_group']))
                            unset($aNewRequest['super_group']);
                        if (isset($aNewRequest['related_product']))
                            unset($aNewRequest['related_product']);
                        if (isset($aNewRequest['gift_next']))
                            unset($aNewRequest['gift_next']);

                        $oProductItem = $product;
                    }
                }
            }
        }

        $aResModel = Mage::getResourceModel('adjgiftreg/item');
        $oDb = $aResModel->getReadConnection();
        $oSelect = $oDb->select()->from($aResModel->getMainTable(), array('item_id', 'buy_request'))
                ->where('event_id = ?', $this->getId())
                ->where('product_id = ?', $productId);
        $aAllRecords = $oDb->fetchPairs($oSelect);

        $iWasAddedItemId = 0;

        foreach ($aAllRecords as $iItemId => $sBuyRequest) {
            $aOldRequest = unserialize($sBuyRequest);

            unset($aOldRequest['qty']);

            if (isset($aOldRequest['super_group']))
                unset($aOldRequest['super_group']);
            if (isset($aOldRequest['related_product']))
                unset($aOldRequest['related_product']);
            if (isset($aOldRequest['gift_next']))
                unset($aOldRequest['gift_next']);

            if (!empty($aOldRequest['options'])) {
                if (!$this->checkOptionsValues($oProductItem, $aOldRequest['options']))
                    unset($aOldRequest['options']);
            }

            if (!empty($aNewRequest['options'])) {
                if (!$this->checkOptionsValues($oProductItem, $aNewRequest['options']))
                    unset($aNewRequest['options']);
            }

            if ($aNewRequest AND $aNewRequest == $aOldRequest) {
                $iWasAddedItemId = $iItemId;
            }
        }

        return $iWasAddedItemId;
    }

    /*
     * @param string $productId
     * @param array $customOptions
     * @return integer
     */
    protected function _retrieveNewQty($productId, $customOptions) {
        $newQty = 0;

        if ($customOptions) {
            foreach ($customOptions as $product) {
                $options = $product->getCustomOptions();

                foreach ($options as $option) {
                    if ($option->getProductId() == $productId && $option->getCode() == 'info_buyRequest') {
                        $aNewRequest = unserialize($option->getValue());
                        $newQty = $aNewRequest['qty'];
                        if (!$newQty) {
                            $newQty = 1;
                        }
                    }
                }
            }
        }

        return $newQty;
    }

    /*
     * @param string $productId
     * @param array $customOptions
     * @return AdjustWare_Giftreg_Model_Item
     */
    public function addItem($productId, $customOptions) {
        $item = Mage::getModel('adjgiftreg/item');
        $item->loadBy($this->getId(), $productId); //, $this->getSharedStoreIds()
        // TODO allow to add different products with different options/attributes
        if ($item->getId()) {
            $aNewRequest = array();

            $oProductItem = null;

            $iNewQty = $this->_retrieveNewQty($productId, $customOptions);

            $iWasAddedItemId = $this->_wasAddedItemId($productId, $customOptions);

            if (!$iWasAddedItemId) { // add as new
                $item = Mage::getModel('adjgiftreg/item');

                $item->setProductId($productId)
                        ->setEventId($this->getId())
                        ->setAddedAt(now())
                        ->setNumWants(1)
                        ->setNumHas(0)
                        ->setSkuProductId($productId) // Sku product is used for configurable products only @ksenevich
                        ->save();   // we need to get ID
            } else { // add qty

                $item->load($iWasAddedItemId);

                $item->setNumWants($item->getNumWants() + $iNewQty)
                        ->save();
                $item->setAddToOld(true);
            }
        } else {
            $item->setProductId($productId)
                    ->setEventId($this->getId())
                    ->setAddedAt(now())
                    ->setNumWants(1)
                    ->setNumHas(0)
                    ->setSkuProductId($productId) // Sku product is used for configurable products only @ksenevich
                    ->save();   // we need to get ID
        }

        if ($customOptions) {
            $item = $this->_saveItemOptions($item, $productId, $customOptions);
        }
        $item->save();  // save buy request
        // TODO delete items, extept this one with the same product id and buyRequest

        return $item;
    }

    /*
     * @param AdjustWare_Giftreg_Model_Item $item
     * @param string $productId
     * @param array $customOptions
     * @return AdjustWare_Giftreg_Model_Item
     */
    protected function _saveItemOptions($item, $productId, $customOptions) {
        if ($customOptions) {
            foreach ($customOptions as $product) {
                $options = $product->getCustomOptions();
                foreach ($options as $option) {
                    if (is_array($option)) {
                        $option = Mage::getModel('adjgiftreg/item_option')
                                ->setData($option)
                                ->setQty($product->getCartQty())
                                ->setItemId($item->getId());
                    } elseif (($option instanceof Varien_Object) && !($option instanceof Mage_Sales_Model_Quote_Item_Option)) {
                        $option = Mage::getModel('adjgiftreg/item_option')->setData($option->getData())
                                ->setProduct($option->getProduct())
                                ->setQty($product->getCartQty())
                                ->setItemId($item->getId());
                    }

                    if ($option->getProductId() == $productId && $option->getCode() == 'info_buyRequest') {
                        $v = unserialize($option->getValue());
                        unset($v['event']);
                        $qty = isset($v['qty']) ? max(1, $v['qty']) : 1;

                        if (!$item->getAddToOld()) {
                            $item->setNumWants($qty);
                            $item->setBuyRequest(serialize($v));
                        }
                    }

                    // for the future upgrades.
                    // This allows to display custom options and attributes in 
                    $option->save();
                }

                if ('configurable' == $product->getTypeId()) {
                    $options = $product->getCustomOptions();
                    foreach ($options as $option) {
                        if ('simple_product' == $option->getCode()) {
                            $item->setSkuProductId($option->getValue());
                        }
                    }
                }
            }
        }

        return $item;
    }

    public function checkOptionsValues($oProductItem, $aOptions) {
        foreach ($aOptions as $iOptionId => $mValue) {
            if ($mValue) {
                if (is_array($mValue)) {
                    if (isset($mValue['hour']) OR isset($mValue['year'])) { // date/time option
                        $option = $oProductItem->getOptionById($iOptionId);

                        $group = $option->groupFactory($option->getType())
                                ->setOption($option);

                        $aBuyRequest = array(array('options' => array($iOptionId => $mValue)));
                        $group->setUserValue($mValue);
                        $group->validateUserValue($mValue);
                        $group->setIsValid(true);
                        $group->setRequest(new Varien_Object($aBuyRequest));

                        $mValue = $group->prepareForCart();

                        if ($mValue)
                            return true;
                    }
                    else {
                        foreach ($mValue as $mArrayValue) {
                            if ($mArrayValue)
                                return true;
                        }
                    }
                }
                else {
                    return true;
                }
            }
        }

        return false;
    }

    public function getSharedStoreIds() {
        if (is_null($this->_storeIds)) {
            $this->_storeIds = Mage::app()->getStore()->getWebsite()->getStoreIds();
        }
        return $this->_storeIds;
    }

    public function getLastEventId($customerId) {
        return $this->_getResource()->getLastEventId($customerId);
    }

    public function getDateFormat($returnType = false) {
        $type = Mage_Core_Model_Locale::FORMAT_TYPE_SHORT;
        if ($returnType)
            return $type;
        return Mage::app()->getLocale()->getDateFormat($type);
    }

    public function getFullTitle($sep = ' ') {
        $res = ucfirst($this->getFname()) . ' ' . ucfirst($this->getLname());
        if ($this->getFname2() || $this->getLname2())
            $res .= ' & ' . ucfirst($this->getFname2()) . ' ' . ucfirst($this->getLname2());
        $res .= $sep . $this->getTitle() . ' ' . Mage::helper('adjgiftreg')->__('Gift Registry');

        return $res;
    }

    public function loadByCode($code) {
        $this->_getResource()->load($this, $code, 'sharing_code');
        #$this->updateOrderedCounter($this->getProductCollection());//should be called only when order change status or created
        return $this;
    }

    public function getEmails() {
        $prevEmails = $this->_getData('emails');
        if ($prevEmails) {
            $prevEmails = explode(',', $prevEmails);
            sort($prevEmails);
        }
        else
            $prevEmails = array();

        return $prevEmails;
    }

    public function setEmails(array $newEmails) {
        $newEmails = implode(',', array_unique($newEmails));
        $this->setData('emails', $newEmails);

        return $this;
    }

    public function remove() {
        $this->setStatus('removed');
        //$items = Mage::getResourceModel('adjgiftreg/item');
        //$items->removeByEvent($this->getId());
        return $this->save();
    }

    public function isRemoved() {
        return $this->getStatus() == 'removed';
    }

    public function isAvailableFor($customerId) {
        $eventExists = $this->getId() && !$this->isRemoved();
        $validOwner = ($customerId == $this->getCustomerId());

        return ($eventExists && $validOwner);
    }

    public function getAddressId()
    {
        $addressId = parent::getAddressId();
        if (!$addressId){
            $customerId = $this->getCustomerId();//->getPrimaryShippingAddress();
            $customer = Mage::getModel('customer/customer')->load($customerId);
            if ($customer && ($address = $customer->getPrimaryShippingAddress())) {
                $addressId = $address->getId();
            }
        }
        return $addressId;
    }
}