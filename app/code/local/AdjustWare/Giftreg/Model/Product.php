<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Model_Product extends Mage_Core_Model_Abstract
{


    private function getOptionsAsInJson($item)
    {
        $options = array();
        $products = array();
        $allProducts = $item->getProduct()->getTypeInstance(true)
            ->getUsedProducts(null, $item->getProduct());
        foreach ($allProducts as $product) {
            if ($product->isSaleable()) {
                $products[] = $product;
            }
        }
        $allowedAttr = $item->getProduct()->getTypeInstance(true)
            ->getConfigurableAttributes($item->getProduct());
        foreach ($products as $product) {
            $productId  = $product->getId();
            foreach ($allowedAttr as $attribute) {
                $productAttribute   = $attribute->getProductAttribute();
                $productAttributeId = $productAttribute->getId();
                $attributeValue     = $product->getData($productAttribute->getAttributeCode());
                if (!isset($options[$productAttributeId])) {
                    $options[$productAttributeId] = array();
                }

                if (!isset($options[$productAttributeId][$attributeValue])) {
                    $options[$productAttributeId][$attributeValue] = array();
                }
                $options[$productAttributeId][$attributeValue][] = $productId;
            }
        }
       return $options;
    }

    /*
    * a sub-method for ifSimpleInStock() method
    * @param AdjustWare_Giftreg_Model_Item $item
    * @return number
    */
    protected function _getCountForConfigurableProduct($item)
    {
        $count = 0;
        $intersect = array();
        $sBuyRequest = $item->getBuyRequest();
        $aBuyRequest = null;
        if ($sBuyRequest)
            $aBuyRequest = unserialize($sBuyRequest);
  
        $typeInstance = $item->getProduct()->getTypeInstance(true);
        $options = $this->getOptionsAsInJson($item);
        if($aBuyRequest AND !empty($aBuyRequest['super_attribute']))
        {
            $first = true;
            foreach($aBuyRequest['super_attribute'] as $attr => $attr_val)
            {
                if($first)
                {
                    if(!isset($options[$attr])|| !isset($options[$attr][$attr_val]))
                        return 0;
                    $intersect = $options[$attr][$attr_val];
                    $first = false;
                }
                else
                {
                    $intersect = array_intersect($intersect,$options[$attr][$attr_val]);
                    if(empty($intersect)) break;
                }
            }
        }

        $count = count($intersect);

        return $count;
    }

    /*
    * @param AdjustWare_Giftreg_Model_Item $item
    * @return array
    */
    protected function _retrieveBundleOptions($item)
    {
        $bundleOptions = array();

        $sBuyRequest = $item->getBuyRequest();
        if ($sBuyRequest)
            $aBuyRequest = unserialize($sBuyRequest);
        $typeInstance = $item->getProduct()->getTypeInstance(true);

        if ($aBuyRequest AND !empty($aBuyRequest['bundle_option']))
        {   
            $optionsCollection = $typeInstance->getOptionsByIds(array_keys($aBuyRequest['bundle_option']), $item->getProduct());
            $aSelectionIds = array();
                    
            foreach ($aBuyRequest['bundle_option'] as $mValue)
            {
                if (is_array($mValue))
                {
                    foreach ($mValue as $mSubValue)
                    {
                        if ($mSubValue)
                        {
                            $aSelectionIds[] = $mSubValue;
                        }
                    }
                }
                elseif ($mValue) 
                {
                    $aSelectionIds[] = $mValue;
                }
            }
                    
            $selectionsCollection = $typeInstance->getSelectionsByIds(
                $aSelectionIds,
                $item->getProduct()
            );
                    
            $bundleOptions = $optionsCollection->appendSelections($selectionsCollection, true);
        }

        return $bundleOptions;
    }

    /*
    * @param AdjustWare_Giftreg_Model_Item $item
    * @return number
    */
    public function ifSimpleInStock($item)
    {
        $intersect = array();
        $count = 1;
        switch ($item->getProduct()->getTypeId())
        {
            case 'configurable':
                $count = $this->_getCountForConfigurableProduct($item);
                break;
            case 'bundle':
                $sBuyRequest = $item->getBuyRequest();
		        $aBuyRequest = null;
		        if ($sBuyRequest)
		        {
		            $aBuyRequest = unserialize($sBuyRequest);
		        }
		        $typeInstance = $item->getProduct()->getTypeInstance(true);
            	if ($aBuyRequest AND !empty($aBuyRequest['bundle_option']))
                {   
                    $bundleOptions = $this->_retrieveBundleOptions($item);
                    
                    foreach ($bundleOptions as $bundleOption) 
                    {
                        if ($bundleOption->getSelections()) 
                        {                            
                            $bundleSelections = $bundleOption->getSelections();
        
                            foreach ($bundleSelections as $bundleSelection) 
                            {
                            	if(!$bundleSelection->isSaleable())
                            	{
                            		return 0;
                            	}
                            }
                        }
                    }
                }            	
                break;
        }
        return $count;
    }

    /*
    * @param AdjustWare_Giftreg_Model_Item $item
    * @return string
    */
    protected function _getOptionsHtmlBundle($item)
    {
        $sHtml = '';
        
        $nAdditionalPrice = 0;

        $sBuyRequest = $item->getBuyRequest();

        $aBuyRequest = null;
        
        if ($sBuyRequest)
        {
            $aBuyRequest = unserialize($sBuyRequest);
        }

        $typeInstance = $item->getProduct()->getTypeInstance(true);

        if ($aBuyRequest AND !empty($aBuyRequest['bundle_option']))
        {                    
            $bundleOptions = $this->_retrieveBundleOptions($item);
                    
            foreach ($bundleOptions as $bundleOption) 
            {
                if ($bundleOption->getSelections()) 
                {
                    $bundleSelections = $bundleOption->getSelections();
        
                    foreach ($bundleSelections as $bundleSelection) 
                    {
                        if (!empty($aBuyRequest['bundle_option_qty'][$bundleOption->getOptionId()]))
                        {
                            $iSelectionQty = $aBuyRequest['bundle_option_qty'][$bundleOption->getOptionId()];
                        }
                        else 
                        {
                            $iSelectionQty = 1;
                        }
                        
                        $sPrice =  $item->getProduct()->getPriceModel()->getSelectionFinalPrice(
                            $item->getProduct(), $bundleSelection,
                            $aBuyRequest['qty'],
                            $iSelectionQty);

                        $sValue = $iSelectionQty .' x '. $bundleSelection->getName(). ' ' .Mage::helper('core')->currency($sPrice);
                                
                        $sHtml .= '<dt>' . $bundleOption->getTitle() . '</dt><dd>' . $sValue . '</dd>';
                                
                        // add to price
                        $nAdditionalPrice += $sPrice;
                    }
                }
            }
        }        
        
        Mage::unregister('ait_additional_price');
        Mage::register('ait_additional_price', $nAdditionalPrice);

        return $sHtml;
    }

    /*
    * @param AdjustWare_Giftreg_Model_Item $item
    * @return string
    */
    protected function _getOptionsHtmlDownloadable($item)
    {
        $sHtml = '';
        
        $nAdditionalPrice = 0;

        $sBuyRequest = $item->getBuyRequest();

        $aBuyRequest = null;
        
        if ($sBuyRequest)
        {
            $aBuyRequest = unserialize($sBuyRequest);
        }

        $typeInstance = $item->getProduct()->getTypeInstance(true);

        if ($aBuyRequest AND !empty($aBuyRequest['links']))
        {
            $productLinks = $typeInstance
                ->getLinks($item->getProduct());
                        
            $sDownHtml = '';                        
                        
            foreach ($productLinks as $link) 
            {
                $linkId = $link->getId();
                        
                if ($linkId AND in_array($linkId, $aBuyRequest['links']))
                {
                    $sDownHtml .= '<dd>' . $link->getTitle() . '</dd>';
                    // add to price
                    $nAdditionalPrice += $link->getPrice();
                }
            }            
                    
            if ($sDownHtml)
            {
                if ($item->getProduct()->getLinksTitle())
                { 
                    $sLinksTitle = $item->getProduct()->getLinksTitle();
                }
                else 
                {
                    $sLinksTitle = Mage::getStoreConfig(Mage_Downloadable_Model_Link::XML_PATH_LINKS_TITLE);
                }
                        
                $sHtml .= '<dt>' . $sLinksTitle . '</dt>' . $sDownHtml; 
                        
            }
        }

        Mage::unregister('ait_additional_price');
        Mage::register('ait_additional_price', $nAdditionalPrice);

        return $sHtml;
    }

    /*
    * @param AdjustWare_Giftreg_Model_Item $item
    * @return string
    */
    protected function _getOptionsHtmlConfigurable($item)
    {
        $sHtml = '';
        
        $nAdditionalPrice = 0;

        $sBuyRequest = $item->getBuyRequest();

        $aBuyRequest = null;
        
        if ($sBuyRequest)
        {
            $aBuyRequest = unserialize($sBuyRequest);
        }

        $typeInstance = $item->getProduct()->getTypeInstance(true);

        if ($aBuyRequest AND !empty($aBuyRequest['super_attribute']))
        {
            $attributes = $typeInstance
                ->getConfigurableAttributes($item->getProduct());
                        
            foreach ($attributes as $attribute) 
            {
                $attributeId = $attribute->getProductAttribute()->getId();
                        
                if ($attributeId AND !empty($aBuyRequest['super_attribute'][$attributeId]))
                {
                    if ($aPrices = $attribute->getPrices())
                    {
                        foreach ($aPrices as $aOptionData)
                        {
                            if ($aBuyRequest['super_attribute'][$attributeId] == $aOptionData['value_index'])
                            {
                                $sHtml .= '<dt>' . $attribute->getProductAttribute()->getFrontendLabel() . '</dt><dd>' . $aOptionData['store_label'] . '</dd>';
                                // add to price
                                $nAdditionalPrice += $aOptionData['pricing_value'];
                            }
                        }
                    }
                }
            }            
        }

        Mage::unregister('ait_additional_price');
        Mage::register('ait_additional_price', $nAdditionalPrice);

        return $sHtml;
    }

    /*
    * @param AdjustWare_Giftreg_Model_Item $item
    * @return string
    */
    protected function _getOptionsHtmlSimple($item)
    {
        $sHtml = '';

        $nAdditionalPrice = 0;
        if (Mage::registry('ait_additional_price'))
        {
            $nAdditionalPrice += Mage::registry('ait_additional_price');
        }
        $sBuyRequest = $item->getBuyRequest();

        $aBuyRequest = null;
        
        if ($sBuyRequest)
        {
            $aBuyRequest = unserialize($sBuyRequest);
        }

        $typeInstance = $item->getProduct()->getTypeInstance(true);

        if ($aBuyRequest AND !empty($aBuyRequest['options']))
        {                    
            $item->getProduct()->load($item->getProduct()->getId());
                    
            foreach ($aBuyRequest['options'] as $iOptionId => $mValue)
            {
                if ($mValue AND $option = $item->getProduct()->getOptionById($iOptionId)) 
                {
                    $isValid = true;
                            
                    $group = $option->groupFactory($option->getType())
                        ->setOption($option)
                        ->setProduct($item->getProduct());

                    if (in_array($option->getType(), array('date', 'date_time', 'time')))
                    {
                        $group->setUserValue($mValue); 
                        $value = $mValue;
                        $dateValid = true;
                        if ($this->_dateExists($option->getType())) 
                        {
                            if ($group->useCalendar()) {
                                $dateValid = isset($value['date']) && preg_match('/^[0-9]{1,4}.+[0-9]{1,4}.+[0-9]{1,4}$/', $value['date']);
                            } else {
                                $dateValid = isset($value['day']) && isset($value['month']) && isset($value['year'])
                                    && $value['day'] > 0 && $value['month'] > 0 && $value['year'] > 0;
                            }
                        }
                        
                        $timeValid = true;
                        if ($this->_timeExists($option->getType())) {
                            $timeValid = isset($value['hour']) && isset($value['minute'])
                                && is_numeric($value['hour']) && is_numeric($value['minute']);
                        }
                        
                        $isValid = $dateValid && $timeValid;
                        
                        if ($isValid) {
                            $group->setUserValue(
                                array(
                                    'date' => isset($value['date']) ? $value['date'] : '',
                                    'year' => isset($value['year']) ? intval($value['year']) : 0,
                                    'month' => isset($value['month']) ? intval($value['month']) : 0,
                                    'day' => isset($value['day']) ? intval($value['day']) : 0,
                                    'hour' => isset($value['hour']) ? intval($value['hour']) : 0,
                                    'minute' => isset($value['minute']) ? intval($value['minute']) : 0,
                                    'day_part' => isset($value['day_part']) ? $value['day_part'] : '',
                                    'date_internal' => isset($value['date_internal']) ? $value['date_internal'] : '',
                                )
                            );
                                    
                            $group->setIsValid(true);                                
                            $group->setRequest(new Varien_Object($aBuyRequest));                                
    
                            $mValue = $group->prepareForCart();
                        }
                        else 
                            $mValue = '';
                    }

                    if ($option->getType() == 'file')
                        $aOrigValue = $mValue;
                                  
                    // add to price
                    foreach ($option->getValues() as $_value) 
                    {
                        if ($mValue)
                        {
                            if (is_array($mValue) AND in_array($_value->getId(), $mValue))
                                $nAdditionalPrice += $_value->getPrice(true);
                            elseif (!is_array($mValue) AND $mValue == $_value->getOptionTypeId()) 
                                $nAdditionalPrice += $_value->getPrice(true);
                        }
                    }

                    if (is_array($mValue))
                        $mValue = implode(',', $mValue);
                    
                    if ($isValid)
                    {
                        $aOptionData = array(
                            'label' => $option->getTitle(),
                            'print_value' => $group->getPrintableOptionValue($mValue),
                        );
                    }
                    else 
                        $aOptionData['print_value'] = '';
                                                
                    if (in_array($option->getType(), array('date', 'date_time', 'time')))
                    {
                        if ($mValue)
                        {
                            if ($option->getPrice())
                                $nAdditionalPrice += $option->getPrice();
                        }
                        else 
                        {
                            $aOptionData['print_value'] = '';
                        }
                    }
                    else 
                    {
                        // add to price
                        if ($option->getPrice())
                        {
                            $nAdditionalPrice += $option->getPrice();
                        }
                    }
       
                    if ($option->getType() == 'file')
                    {
                        if ($aOrigValue['width'] > 0 && $aOrigValue['height'] > 0) {
                            $sizes = $aOrigValue['width'] . ' x ' . $aOrigValue['height'] . ' ' . Mage::helper('catalog')->__('px.');
                        } else {
                            $sizes = '';
                        }
                                
                        $aOptionData['print_value'] =  sprintf('%s %s',
                            Mage::helper('core')->htmlEscape($aOrigValue['title']),
                            $sizes
                        );
                    }
                                                
                    if ($aOptionData['print_value'])
                        $sHtml .= '<dt>' . $aOptionData['label'] . '</dt><dd>' . $aOptionData['print_value'] . '</dd>';
                }
            }
        }

        Mage::unregister('ait_additional_price');
        Mage::register('ait_additional_price', $nAdditionalPrice);

        return $sHtml;
    }

    public function getOptionsHtml($item)
    {
        $sHtml = '<dl class="item-options">';

        Mage::unregister('ait_additional_price');

        switch ($item->getProduct()->getTypeId())
        {
            case 'bundle':
                $sHtml .= $this->_getOptionsHtmlBundle($item);                 
                break;                
            case 'downloadable':
                $sHtml .= $this->_getOptionsHtmlDownloadable($item);  
            case 'configurable':
                $sHtml .= $this->_getOptionsHtmlConfigurable($item);
            case 'simple':
            case 'virtual':
                $sHtml .= $this->_getOptionsHtmlSimple($item);
                break;
        }        
        $sHtml .= '</dl>';
        return $sHtml;
    }
    
    protected function _dateExists($sType)
    {
        return in_array($sType, array(
            Mage_Catalog_Model_Product_Option::OPTION_TYPE_DATE,
            Mage_Catalog_Model_Product_Option::OPTION_TYPE_DATE_TIME
        ));
    }

    protected function _timeExists($sType)
    {
        return in_array($sType, array(
            Mage_Catalog_Model_Product_Option::OPTION_TYPE_DATE_TIME,
            Mage_Catalog_Model_Product_Option::OPTION_TYPE_TIME
        ));
    }
    
}