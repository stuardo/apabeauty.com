<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
/**
 * @copyright  Copyright (c) 2009 AITOC, Inc. 
 */

class AdjustWare_Giftreg_Model_ModuleObserver
{
    public function __construct()
    {
    }
    
    /**
     * aitoc_module_disable_before
     * Disable the module in config
     */
    public function onAitocModuleDisableBefore($observer)
    {
        if ('AdjustWare_Giftreg' == $observer->getAitocmodulename())
        {
            //$oInstaller = $observer->getObject();
            /* @var $oInstaller Aitoc_Aitsys_Model_Aitsys */
            
            $res = Mage::getSingleton('core/resource');
            /* @var $resource Mage_Core_Model_Resource */
            
            $conn = $res->getConnection('core_write');
            /* @var $conn Varien_Db_Adapter_Pdo_Mysql */
            
            $conn->delete($res->getTableName('core/config_data'), 'path = "adjgiftreg/general/active"');
        }
    }
    
}