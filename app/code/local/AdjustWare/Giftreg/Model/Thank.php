<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Model_Thank extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('adjgiftreg/thank');
    }

	/** 
	 *
	 * @param AdjustWare_Giftreg_Model_Event $event Current Event Object
	 * @return AdjustWare_Giftreg_Model_Mysql4_Thank_Collection
	 */
    public function getThanks(AdjustWare_Giftreg_Model_Event $event)
    {
        $thanks = $this->getCollection();

        $thanks
            ->addEventFilter($event->getId())
            ->addOrder('order_id');

        return $thanks;
    }
}