<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Model_Observer extends Mage_Core_Model_Abstract
{
    public function processCartAddProductComplete($observer)
    {
        $request = $observer->getRequest();
        $messages = Mage::getSingleton('checkout/session')->getGiftregPendingMessages();
        $urls = Mage::getSingleton('checkout/session')->getGiftregPendingUrls();
        if ($request->getParam('gift_next') && count($urls)) {
            $url = array_shift($urls);
            $message = array_shift($messages);

            Mage::getSingleton('checkout/session')->setGiftregPendingUrls($urls);
            Mage::getSingleton('checkout/session')->setGiftregPendingMessages($messages);

            Mage::getSingleton('checkout/session')->addNotice($message);
            
            $observer->getResponse()->setRedirect($url);
            Mage::getSingleton('checkout/session')->setNoCartRedirect(true);
        }
    }     

    /*
    * a sub-method for processCartProductAdd() method, retrieves Product Id
    * @param Sales_Model_Quote_Item $quoteItem
    * @return number
    */
    protected function _getQuoteItemCurrentProductId($quoteItem)
    {
        $gifts  = Mage::getSingleton('checkout/session')->getAdjgiftregItem();

        if ($quoteItem->getProductType() == 'grouped')
        {
            $iGroupedId = 0;
            
            if (is_array($gifts))
            {
                if ($gifts && is_array($gifts))
                {
                    foreach ($gifts as $iGiftId => $oGiftdata)
                    {
                        if ($oGiftdata AND $sBuyRequest = $oGiftdata->getBuyRequest())
                        {
                            $aBuyData = unserialize($sBuyRequest);
                            
                            if ($aBuyData AND !empty($aBuyData['super_group']))
                            {
                                if (isset($aBuyData['super_group'][$quoteItem->getProductId()]))
                                {
                                    $iGroupedId = $oGiftdata->getProductId();
                                }
                            }
                        }
                    }
                }
            }
            
            if ($iGroupedId)
            {
                $iCurrentId = $iGroupedId;
            } else 
            {
                $iCurrentId = $quoteItem->getProductId();
            }
            
        }
        else 
        {
            $iCurrentId = $quoteItem->getProductId();
        }

        return $iCurrentId;
    }
    
    public function processCartProductAdd($observer)
    {
        //prevent adding no-gifts and gifts from different events to gift-quote 
        $subItem = $observer->getEvent()->getQuoteItem();
        $parent  = $subItem->getParentItem();
        
        $quoteItem =  $parent ? $parent : $subItem;
        
        $gifts  = Mage::getSingleton('checkout/session')->getAdjgiftregItem();
        
        $iCurrentId = $this->_getQuoteItemCurrentProductId($quoteItem);

        $isGiftAdding = ($gifts && is_array($gifts) && !empty($gifts[$iCurrentId]));
        $gift = $isGiftAdding ? $gifts[$iCurrentId] : null;
        
        $quote     = $quoteItem->getQuote();
        $eventId   = $quote->getAdjgiftregEventId();
        
        // quote linked to event and non-gift added
        if ($eventId && !$isGiftAdding){
            Mage::throwException(Mage::helper('adjgiftreg')->__('Please select only gifts for you order or remove all gifts from you shopping cart. Sorry for the unconvinience')); 
        }
        // quote linked to one event and gift from another event added
        if ($eventId && $isGiftAdding && $gift->getEventId() != $eventId){

            Mage::throwException(Mage::helper('adjgiftreg')->__('To ensure correct shipping, please select gifts from only one registry at once. Sorry for the unconvinience')); 
        }

        // quote already has not-gift item(s) and gift added
        if (!$eventId && $isGiftAdding && $quote->getItemsCount() > 0){ 

            Mage::throwException(Mage::helper('adjgiftreg')->__('Please remove all non-gifts from your shopping cart before adding a gift to ensure correct shipping and order tracking. Sorry for the unconvinience')); 
        }
        if ($isGiftAdding){
            $quoteItem->setAdjgiftregItemId($gift->getItemId());
            $quote->setAdjgiftregEventId($gift->getEventId());
            
            $this->_checkQty($quoteItem);
            
            $backUrl = Mage::getModel('core/url')->getUrl('gifts/index/view', array('code'=> $gift->getEvent()->getSharingCode()));
            
            
            $msg = Mage::helper('adjgiftreg')->__('Return to the <a href="%1$s">%2$s</a>.', $backUrl, $gift->getEvent()->getFullTitle());
            if (!Mage::getSingleton('checkout/session')->getGiftregPendingUrls())
                $this->addMessageIfNotExist($msg);
            
            $gifts[$quoteItem->getProductId()] = null;
            Mage::getSingleton('checkout/session')->setAdjgiftregItem($gifts);
        }
    }
    
    private function addMessageIfNotExist($msg){
        $exists = false;
        $allMessages = Mage::getSingleton('checkout/session')
            ->getMessages()
            ->getItemsByType(Mage_Core_Model_Message::SUCCESS);
        foreach ($allMessages as $m){
            if ($m->getText() == $msg){
                $exists = true;
                break;
            }
        }
        if (!$exists)
            Mage::getSingleton('checkout/session')->addSuccess($msg);
        
    }

    //one product
    public function processCartProductRemoved($observer)
    {
        $quote = $observer->getEvent()->getQuoteItem()->getQuote();
        $cnt = $quote->getItemsCount();
        if (1 == $cnt){ // we remove last item from the quote
            if (!Mage::registry('ait_cart_edit')) // fix for Aiteditablecart module
            {            
                $quote->setAdjgiftregEventId(0);  
            }
        }
    }
    
    public function onEditablecartProductAdd($observer)
    {
        
        $quote = $observer->getEvent()->getCart()->getQuote();
        
        $iEventId = $quote->getAdjgiftregEventId();  
        $iProductId = $observer->getProduct()->getId();

        $item = Mage::getModel('adjgiftreg/item')->loadBy($iEventId, $iProductId);
        if (!$item->getId()){
            return;
        }
        
        $event = Mage::getModel('adjgiftreg/event')->load($item->getEventId());
        if (!$event->getId()){ 
            return;
        }
        
        $prevEventId = $quote->getAdjgiftregEventId();
        
        if ($prevEventId && $prevEventId != $event->getId())
        {
            Mage::getSingleton('adjgiftreg/session')->addError(Mage::helper('adjgiftreg')->__('To ensure correct shipping, please select gifts from only one registry at once. Sorry for the unconvinience'));

            return;
        }
        
        $item->setEvent($event);
        $this->_addItemToSession($item);
    }
    
    // we will need this info at the checkout process
    private function _addItemToSession($item){
        $gifts = Mage::getSingleton('checkout/session')->getAdjgiftregItem();
        if (!is_array($gifts)) // first time
            $gifts = array();
            
        $gifts[$item->getProductId()] = $item;
        Mage::getSingleton('checkout/session')->setAdjgiftregItem($gifts);       
    }
    
    
    //all cart updated
    public function processCartUpdated($observer)
    {
        $quote = $observer->getEvent()->getCart()->getQuote();
        $cnt = $quote->getItemsCount();
        foreach ($observer->getEvent()->getInfo() as $itemId => $itemInfo) {
            $item = $quote->getItemById($itemId);
            if (!$item) {
                continue;
            }
            if (!empty($itemInfo['remove']) || (isset($itemInfo['qty']) && $itemInfo['qty']=='0')) {
                $cnt --;
                continue;
            }
            $this->_checkQty($item);
        }
        if (!$cnt) // remove event mark for empty cart        
            $quote->setAdjgiftregEventId(0); 
    }
    
    public function processBeforeOrderSaved($observer)
    {
        //$order = $observer->getEvent()->getOrder();
        $quote = $observer->getEvent()->getQuote();
        
        if ($quote->getAdjgiftregEventId())
        {
            $event = Mage::getModel('adjgiftreg/event')->load($quote->getAdjgiftregEventId());
            Mage::register('giftreg_purchase_customer_id', $event->getCustomerId());
            Mage::log('Registered');
        }
        
        // ckeck that user buy correct items qties (other buyers can already bought some)
        $errors = array();
        foreach ($quote->getAllVisibleItems() as $item){
            if (!$item->getAdjgiftregItemId())
                continue;
            $gift = Mage::getModel('adjgiftreg/item');
            $gift->load($item->getAdjgiftregItemId()); 
            $wants = $gift->getNumWants();
            $has   = $gift->getNumHas();
            //add config 
            //if ($gift->getId() && ($has + $item->getQty() > $wants)){
            //    $errors[] = Mage::helper('adjgiftreg')->__('Registrant wants %d items of %s but already has %d. Please edit you cart and decrease quantity or select another gift.',
            //        $wants, $item->getName(), $has);    
            //}
            //add config  
            if (!$gift->getId()){
                $errors[] = Mage::helper('adjgiftreg')->__('Registrant has already removed %s from his/her gift registry. Please select another gift.',
                    $item->getName());
            }
        }
        
        if ($errors)
            Mage::throwException(join('<br />', $errors)); 
    }    

    public function processAfterOrderSaved($observer)
    {
        $quote = $observer->getEvent()->getQuote();
        $order = $observer->getEvent()->getOrder();
        
        $gift = Mage::getModel('adjgiftreg/item');
        $thank = Mage::getModel('adjgiftreg/thank');
        foreach ($quote->getAllVisibleItems() as $item){

            if(!$item->getAdjgiftregItemId())
                continue;
            $gift->load($item->getAdjgiftregItemId()); 
            if ($gift->getId()){
                // increase Received values in gift items
                $gift->setNumHas($gift->getNumHas() + $item->getQty())
                    ->save();
                // create thank you notes
                $thank->setId(null)
                    ->setProductName($item->getName())
                    ->setProductId($gift->getProductId())
                    ->setOrderId($order->getEntityId())
                    ->setEventId($gift->getEventId())
                    ->setQty($item->getQty())
                    ->setGiverName($quote->getBillingAddress()->getName())
                    ->setPurchasedAt(now())
                    ->save();
            }
        }
        if ($quote->getAdjgiftregEventId()){
            $gifOrder = Mage::getModel('adjgiftreg/order');
            $gifOrder->setId(null)
                ->setOrderId($order->getId())
                ->setEventId($quote->getAdjgiftregEventId())
                ->save();
            
            $event = Mage::getModel('adjgiftreg/event')->load($quote->getAdjgiftregEventId());
            $event->updateOrderedCounter( $event->getProductCollection() );
            
            $hlp = Mage::helper('adjgiftreg');
            $hlp->sendAdminNotification($hlp->__('A new order %s has been placed', $order->getIncrementId()), $event);
        }
    }

    public function salesOrderSaveAfter( $observer )
    {
        $order = $observer->getEvent()->getOrder();
        $eventCollection = Mage::getResourceModel('adjgiftreg/event_collection');
        $eventCollection->getSelect()
            ->joinInner(
                array('adj_order'=> $eventCollection->getTable('adjgiftreg/order') ),
                'adj_order.event_id=main_table.event_id',
                array('event_id','order_id')
            )
            ->where('adj_order.order_id = ?', $order->getId());

        $event = Mage::getModel('adjgiftreg/event');
        foreach($eventCollection as $orderedEvent) {
            $orderedEvent->updateOrderedCounter( $orderedEvent->getProductCollection( $order->getStoreId() ) );
        }
    }
    
    public function expireEvents()
    {
        $hlp = Mage::helper('adjgiftreg');
        $events = Mage::getResourceModel('adjgiftreg/event_collection')
            ->addShouldBeExpiredFilter()
            ->load(); 

        foreach ($events as $e){
            $e->setStatus('expired')->save();
            $hlp->sendAdminNotification($hlp->__('Gift registry has been expired'), $e);
        }
            
    }
    
    protected function _checkQty($item){
        $warningsAllowed = Mage::getStoreConfig('adjgiftreg/general/qty_warning');
        if (!$warningsAllowed)
            return;
        
        $giftId = $item->getAdjgiftregItemId();
        if (!$giftId)
            return;
            
        $gift = Mage::getModel('adjgiftreg/item')->load($giftId);
            
        if (!$gift->getId())
            return;
            
        $wants = $gift->getNumWants();
        $has   = $gift->getNumHas();
        
        $iRealQty = $item->getQty();
        
        if ($item->getProductType() == 'grouped')
        {
            $iRealQty = 1;
        }
        
//        if ($has + $item->getQty() <= $wants)
        if ($has + $iRealQty <= $wants)
            return;
            
        // show different massages if registrant has item and doesnt have
        $error = '';
        if ($has){
            $error = Mage::helper('adjgiftreg')->__('Registrant wants %d items of %s but already has %d. Please edit you cart and decrease quantity or select another gift.',
                $wants, $item->getName(), $has);    
        } 
        else{
            $error = Mage::helper('adjgiftreg')->__('Notice, that registrant wants only %d items of %s',
                $wants, $item->getName(), $has);    
        }
        Mage::getSingleton('checkout/session')->addNotice($error);
    }
    
    public function mergeQuotes($observer)
    {
        $quote = $observer->getEvent()->getQuote();
        $source = $observer->getEvent()->getSource();
        
        $newEventId = $source->getAdjgiftregEventId(); 
        if ($newEventId != $quote->getAdjgiftregEventId()){
            //remove all items from old quote
            foreach ($quote->getAllVisibleItems() as $item) {
                $item->isDeleted(true);
                if ($item->getHasChildren()) {
                    foreach ($item->getChildren() as $child) {
                        $child->isDeleted(true);
                    }
                }
            }
            //set event id of the new quote 
            $quote->setAdjgiftregEventId($newEventId);
        }
    } 

}