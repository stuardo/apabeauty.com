<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Model_System_Config_Source_Stores
{
    public function toOptionArray()
    {   
        $options[] = array('label' => Mage::helper('adjgiftreg')->__('Global'), 'value' => 0);
        $options[] = array('label' => Mage::helper('adjgiftreg')->__('Website'), 'value' => 1);
        
        return $options;
    }
}