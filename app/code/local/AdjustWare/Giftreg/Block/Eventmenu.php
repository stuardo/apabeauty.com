<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Eventmenu extends AdjustWare_Giftreg_Block_Common
{ 
    protected $_item;
    
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('adjgiftreg/eventmenu.phtml');
    }
    
    public function getEvent(){
        return Mage::registry('current_event');
    }
    
    public function isActive($menuItem){
        return $menuItem == $this->getAction()->getRequest()->getActionName();
    }
}