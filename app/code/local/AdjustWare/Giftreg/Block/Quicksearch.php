<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Quicksearch extends Mage_Core_Block_Template
{
    public function getUrl($route = '', $params = array())
    {
        return Mage::helper('adjgiftreg')->prepareSecureUrls(parent::getUrl($route, $params));
    }
}