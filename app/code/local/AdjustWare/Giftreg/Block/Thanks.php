<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Thanks extends AdjustWare_Giftreg_Block_Common
{ 
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        
        $e = Mage::registry('current_event');
        
        $this->setEvent($e);

        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->setTitle($this->__('Gifts purchased from %s', $e->getFullTitle()));
        }
    }
    
	public function getGiftMessage($id)
	{

		$gift_message = Mage::getModel('giftmessage/message');
		$gift_message->load($id);
		return $gift_message->getMessage();
	}
	
	public function isMessagesAvailable()
	{

		$resultItems = Mage::getStoreConfig(Mage_GiftMessage_Helper_Message::XPATH_CONFIG_GIFT_MESSAGE_ALLOW_ITEMS);
        $resultOrder = Mage::getStoreConfig(Mage_GiftMessage_Helper_Message::XPATH_CONFIG_GIFT_MESSAGE_ALLOW_ORDER);
		return $resultItems || $resultOrder;
	}

	/**
	 * 
	 * @return AdjustWare_Giftreg_Model_Mysql4_Thank_Collection
	 */
    public function getThanks()
    {
        return Mage::getModel('adjgiftreg/thank')
            ->getThanks($this->getEvent())
            ->loadGiftMessages();
    }

}