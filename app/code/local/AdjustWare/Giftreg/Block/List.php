<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_List extends AdjustWare_Giftreg_Block_Common
{ 
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        
        $events = Mage::getResourceModel('adjgiftreg/event_collection')
            ->addCustomerFilter(Mage::getSingleton('customer/session')->getCustomerId())
            ->load();
            
        $this->setEvents($events);
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle($this->__('My Events'));
        }
    }
}