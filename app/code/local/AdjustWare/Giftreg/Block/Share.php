<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Share extends AdjustWare_Giftreg_Block_Common
{
    public function __construct()
    {
        parent::__construct(); 
        $this->setEvent(Mage::registry('current_event'));      
    }  
    
    protected function _prepareLayout()
    {
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle($this->getTitle());
        }
    }
    
    public function getTitle()
    {
        $title = $this->_getData('title');
        return $title ? $title : $this->__('Share Your Gift Registry');
    }
    
    public function getFormData($key, $defValue=''){
        $val = $defValue;
        
        $data = Mage::getSingleton('adjgiftreg/session')->getSharingForm();
        if (!empty($data[$key]))
            $val = $data[$key];
            
        return $this->htmlEscape($val);
    }

    
    public function getLinkNotice()
    {
    	$curr_customer_id = Mage::getSingleton('customer/session')->getCustomer()->getEntityId();

		if($this->getEvent()->getCustomerId() == $curr_customer_id)
		{

    		$url = $this->getUrl('*/index/view', array('code'=>$this->getEvent()->getSharingCode()));
    		return Mage::helper('adjgiftreg')->__('You can also share your gift registry using <a href="').$url. Mage::helper('adjgiftreg')->__('">this link. </a> Just copy  and paste it wherever you want.');
		}
    	else 
    	{
    		return '';
    	}	
    	
    }
    
}