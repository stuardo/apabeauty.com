<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Result extends Mage_Core_Block_Template
{ 
    private $_events = null;
    
    public function __construct()
    {
        parent::__construct(); 
        $ev = Mage::getResourceModel('adjgiftreg/event_collection')
              ->addSearchCriteria($this->getRequest()->getQuery());
        $this->setEvents($ev);       
    }
    
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle($this->getTitle());
        }
        
        if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbs->addCrumb('home', array(
                'label'=>Mage::helper('adjgiftreg')->__('Home'),
                'title'=>Mage::helper('adjgiftreg')->__('Go to Home Page'),
                'link'=>Mage::getBaseUrl()
            ))->addCrumb('search', array(
                'label'=>Mage::helper('adjgiftreg')->__('Gift Lists Search'),
                'link'=>$this->getUrl('gifts/*/')
            ))->addCrumb('search_result', array(
                'label'=>Mage::helper('adjgiftreg')->__('Results')
            ));
        }
        
        $pager = $this->getLayout()->createBlock('page/html_pager', 'adjgiftreg.result.pager');
        $pager->setCollection($this->getEvents());
            
        $this->getEvents()->load();  
          
        $this->setChild('pager', $pager);
        
        return $this;
    }
    
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }    
    
    public function getTitle(){
        return $this->__('Gift Registries Search Results');
    }
    
    public function getViewUrl($e){
         return $this->getUrl('gifts/*/view', array('code'=>$e->getSharingCode()));
     }
}