<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Popular extends Mage_Catalog_Block_Product_List
{
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle($this->getTitle());
        }
        
        if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbs->addCrumb('home', array(
                'label'=>Mage::helper('adjgiftreg')->__('Home'),
                'title'=>Mage::helper('adjgiftreg')->__('Go to Home Page'),
                'link'=>Mage::getBaseUrl()
            ))->addCrumb('search', array(
                'label'=>Mage::helper('adjgiftreg')->__('Gift Lists Search'),
                'link'=>$this->getUrl('gifts/*/')
            ))->addCrumb('search_result', array(
                'label'=>$this->getTitle()
            ));
        }
        return $this;
    }
    
    public function getTitle(){
        return $this->__('Popular Gifts');
    }
    
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
            $this->_productCollection = Mage::getResourceModel('adjgiftreg/product_collection')
                ->addPopularFilter()
                ;
            /* @var $productCollection Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection */
        }  
        return $this->_productCollection;      
    }

    /** Returns the name for the layout block with toolbar
     *
     * @return string
     */
    public function getToolbarBlockName()
    {
        return 'product_list_toolbar';
    }

    public function getToolbarHtml()
    {
        return Mage::helper('adjgiftreg')->prepareSecureUrls(parent::getToolbarHtml());
    }
}