<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Indicator extends AdjustWare_Giftreg_Block_Common
{
    public function getRegistryName()
    {
        $id = Mage::getSingleton('checkout/session')->getQuote()->getAdjgiftregEventId();
        if (!$id)
            return '';
        $event = Mage::getModel('adjgiftreg/event')->load($id);
        if (!$event->getId())
            return '';
        
        return $event->getFullTitle();
    }
}