<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Rewrite_AdminhtmlCustomerEditTabs extends Mage_Adminhtml_Block_Customer_Edit_Tabs
{
    protected function _beforeToHtml()
    {
        if (Mage::registry('current_customer')->getId()) {
            $this->addTab('adjgiftreg', array(
                'label'     => Mage::helper('adjgiftreg')->__('Gift Registries'),
                'class'     => 'ajax',
                'url'       => $this->getUrl('adjgiftregadmin/adminhtml_customer/events', array('_current' => true)),
                'after'     => 'wishlist',
            ));
        }
        
        $this->_updateActiveTab();
        return parent::_beforeToHtml();
    }
    
    protected function _updateActiveTab()
    {
    	$tabId = $this->getRequest()->getParam('tab');
    	if( $tabId ) {
    		$tabId = preg_replace("#{$this->getId()}_#", '', $tabId);
    		if($tabId) {
    			$this->setActiveTab($tabId);
    		}
    	}
    } 
}