<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Rewrite_AdminhtmlView extends Mage_Adminhtml_Block_Sales_Order_View

{
  public function getHeaderText()
    {
        if ($_extOrderId = $this->getOrder()->getExtOrderId()) {
            $_extOrderId = '[' . $_extOrderId . '] ';
        } else {
            $_extOrderId = '';
        }

        $event = Mage::getResourceModel('adjgiftreg/thank_collection')
            ->addOrderFilter($this->getOrder()->getEntityId())
			->join('event','(`event`.event_id = `main_table`.event_id)', array('title','lname','fname'));
		
		$registry_title = '';	
		foreach($event as $item)
    	{
    		$registry_title = '<br/>'.$this->__('for gift registry').': '.$item->getFname().' '.$item->getLname().' '.$item->getTitle();
    	}	

        return Mage::helper('sales')->__('Order  # %s %s | %s '.$registry_title, $this->getOrder()->getRealOrderId(), $_extOrderId, $this->formatDate($this->getOrder()->getCreatedAtDate(), 'medium', true));
    }





}