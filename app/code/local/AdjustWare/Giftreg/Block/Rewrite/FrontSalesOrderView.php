<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Sales order history block
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */


class AdjustWare_Giftreg_Block_Rewrite_FrontSalesOrderView extends Mage_Sales_Block_Order_View
{
	
	protected $_event = null;

    public function __construct()
    {
    	        
        parent::__construct();
        $event = Mage::getResourceModel('adjgiftreg/thank_collection')
            ->addOrderFilter($this->getOrder()->getEntityId())
			->join('event','(`event`.event_id = `main_table`.event_id)', array('title','lname','fname'));
		
		//Zend_debug::dump($event->getData());
		//die('uuu');
		$this->_event = $event;
		
		$this->setTemplate('adjgiftreg/order/view.phtml');
    }
    
    public function getEvent()
    {
    	if(!empty($this->_event))
    	{
    		foreach($this->_event as $item)
    		{
    			return $this->__('Gift registry').': '.$item->getFname().' '.$item->getLname().' '.$item->getTitle();
    		}
    	}
    	return '';
    }


}