<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Rewrite_FrontCheckoutOnepageShipping extends Mage_Checkout_Block_Onepage_Shipping
{
    protected function _construct()
    {
        parent::_construct();
    }

    public function getGiftAddressId(){
        $addrId = 0;
        $id = $this->getQuote()->getAdjgiftregEventId();
        if ($id){
            $event = Mage::getModel('adjgiftreg/event')->load($id);
            if ($event->getId())
                $addrId = $event->getAddressId();
        }
        return $addrId;
    }
    
    public function customerHasAddresses()
    {
        //Mage::throwException('id = ' . $this->getGiftAddressId());  
        if ($this->getGiftAddressId()){
            return true;
        }
        return parent::customerHasAddresses();
    }

    // we do not use $type, but left it for methods signatures coinside
    public function getAddressesHtmlSelect($type)
    {
        $addressId = 0;
        $options = array();
        if ($id = $this->getGiftAddressId()){
            $options[] = array(
                'value' => $id,
                'label' => Mage::helper('adjgiftreg')->__('Ship to the owner of the gift registry'),
            ); 
            $addressId = $id;           
        }
        
        if ($this->isCustomerLoggedIn()) {
            foreach ($this->getCustomer()->getAddresses() as $address) {
                $options[] = array(
                    'value'=>$address->getId(),
                    'label'=>$address->format('oneline')
                );
            }
        
            if (!$addressId) {
                $address = $this->getCustomer()->getPrimaryShippingAddress();
                if ($address) {
                    $addressId = $address->getId();
                }
            }
        }
        
        $html = '';
        if ($options){
            $select = $this->getLayout()->createBlock('core/html_select')
                ->setName($type.'_address_id')
                ->setId($type.'-address-select')
                ->setClass('address-select')
                ->setExtraParams('onchange="'.$type.'.newAddress(!this.value)"')
                ->setValue($addressId)
                ->setOptions($options);

            $select->addOption('', Mage::helper('checkout')->__('New Address'));
            $html = $select->getHtml();
        }
        
        return $html;
    }
}