<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Toplinks extends Mage_Core_Block_Template
{
    public function addGiftregLink()
    {
        $parentBlock = $this->getParentBlock();
        if ($parentBlock && Mage::getStoreConfig('adjgiftreg/general/active')) {
            $text = $this->__('My Gift Registry');
            $parentBlock->addLink($text, 'gifts/event', $text, true, array(), 40, null, 'class="top-link-adjgiftreg"');
        }
        return $this;
    }
}