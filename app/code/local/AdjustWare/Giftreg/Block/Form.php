<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Form extends AdjustWare_Giftreg_Block_Common
{ 
    protected $_event;

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        
        $this->_event = Mage::registry('current_event');

        if (!$this->_event->getId()) {
            $this->_event->setFname($this->getCustomer()->getFirstname());
            $this->_event->setLname($this->getCustomer()->getLastname());
        }

        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle($this->getTitle());
        }
        if ($postedData = Mage::getSingleton('adjgiftreg/session')->getEventFormData(true)) {
            $this->_event->setData($postedData);
        }
    }

    public function getTitle()
    {
        if ($title = $this->getData('title')) {
            return $title;
        }
        if ($this->getEvent()->getId()) {
            $title = Mage::helper('adjgiftreg')->__('Update Event Information');
        }
        else {
            $title = Mage::helper('adjgiftreg')->__('Add New Event');
        }
        return $title;
    }
    
    public function getEvent()
    {
        return $this->_event;
    }

    public function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }
    
    public function getAddressesHtmlSelect()
    {
        $options = array();
        foreach ($this->getCustomer()->getAddresses() as $address) {
            $options[] = array(
                'value'=>$address->getId(),
                'label'=>$address->format('oneline')
            );
        }

        $addressId = $this->_event->getAddressId();

        $select = $this->getLayout()->createBlock('core/html_select')
            ->setName('address_id')
            ->setId('address_id')
            ->setClass('address-select')
            //->setExtraParams('onchange="newAddress(!this.value)"')
            ->setValue($addressId)
            ->setOptions($options);

        $select->addOption('0', Mage::helper('adjgiftreg')->__('I will provide an address later'));

        return $select->getHtml();
    }
    
    public function getDateInput($value, $name = 'date', $title='')
    {
        $block = $this->getLayout()->createBlock('core/html_date');
        return $block->setName($name)
            ->setId($name)
            ->setTitle($title ? $title : $this->__('Event Date'))

            ->setValue(
                    Mage::helper('core')->formatDate($value . ' 12:00:00 PM', Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM)
                )

            ->setImage($this->getSkinUrl('images/calendar.gif'))
            ->setFormat(
                    Mage::app()->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM)
                )
            ->setClass('input-text required-entry')
            ->getHtml();
    }

    public function showPrivacySettings()
    {
        if(!Mage::helper('adjgiftreg')->usePasswordForRegistries() && !Mage::helper('adjgiftreg')->usePrivateStatusForRegistries())
            return false;

        return true;
    }

    /**
     * Retrieve short date format with 4-digit year
     *
     * @return  string
     */
    public function getDateFormatWithLongYear()
    {
        return preg_replace('/(?<!y)yy(?!y)/', 'yyyy',
            Mage::app()->getLocale()->getTranslation(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT, 'date'));
    }

}