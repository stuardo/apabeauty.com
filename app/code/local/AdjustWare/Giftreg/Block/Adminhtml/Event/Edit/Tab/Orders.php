<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Adminhtml_Event_Edit_Tab_Orders extends Mage_Adminhtml_Block_Sales_Order_Grid
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function setCollection($collection)
    {
        $t = Mage::getSingleton('core/resource')->getTableName('adjgiftreg/order');
        $s = $collection->getSelect()
          ->join(array('o'=> $t),'main_table.entity_id = o.order_id', array())
          ->where('o.event_id = ? ', Mage::registry('adjgiftreg_event')->getId());
        //print_r((string)$s);
        //exit;
        $this->_collection = $collection;
    }   


    protected function _prepareColumns()
    {
        $hlp = Mage::helper('adjgiftreg');
        
        $res = parent::_prepareColumns();
        if (isset($this->_columns['action']))
            unset($this->_columns['action']);
        
        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            $this->addColumn('action',
                array(
                    'header'    => $hlp->__('Action'),
                    'width'     => '50px',
                    'type'      => 'action',
                    'getter'     => 'getId',
                    'actions'   => array(
                        array(
                            'caption' => $hlp->__('View'),
                            'url'     => array('base'=>'adminhtml/sales_order/view/'),
                            'field'   => 'order_id'
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                    'index' => 'stores',
                    'is_system' => true,
            ));
        }
        
        $this->_rssLists = array();
        
        return $res;
    }

    protected function _prepareMassaction()
    {
        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('adminhtml/sales_order/view', array('order_id' => $row->getId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/orders', array('_current' => true));
    }  
}