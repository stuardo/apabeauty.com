<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Adminhtml_Event_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('eventsGrid');
        $this->setDefaultSort('date');
        $this->setDefaultDir('DESC');
    }

    protected function _prepareCollection()
    {
        $this->setCollection(Mage::getResourceModel('adjgiftreg/event_collection'));
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $hlp = Mage::helper('adjgiftreg');

        $this->addColumn('event_id', array(
            'header'    => $hlp->__('ID'),
            'index'     => 'event_id',
            'width'     => '50px', 
        ));

        $this->addColumn('date', array(
            'header'    => $hlp->__('Event date'),
            'index'     => 'date',
//            'gmtoffset' => true,
            'type'      => 'date',
        ));

        $this->addColumn('type_id', array(
            'header'    => $hlp->__('Occasion'),
            'index'     => 'type_id',
            'type'      => 'options', 
            'options'   => $hlp->getOccasions(),
        ));


        $this->addColumn('status', array(
            'header'    => $hlp->__('Status'),
            'index'     => 'status',
            'type'      => 'options', 
            'options'   => $hlp->getStatuses(),
        ));

        $this->addColumn('fname', array(
            'header'    => $hlp->__('Registrant First Name'),
            'index'     => 'fname'
        ));

        $this->addColumn('lname', array(
            'header'    => $hlp->__('Registrant Last Name'),
            'index'     => 'lname',
        ));

        $this->addColumn('fname2', array(
            'header'    => $hlp->__('Co-Registrant First Name'),
            'index'     => 'fname2'
        ));

        $this->addColumn('lname2', array(
            'header'    => $hlp->__('Co-Registrant Last Name'),
            'index'     => 'lname2',
        ));
        /*
        $this->addColumn('pass', array(
            'header'    => $hlp->__('Password'),
            'index'     => 'pass',
        ));*/

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}