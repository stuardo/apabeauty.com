<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Adminhtml_Event_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'adjgiftreg';
        $this->_controller = 'adminhtml_event';
        
        parent::__construct();
        $this->_updateButton('save', 'label', Mage::helper('customer')->__('Save Event')); 
        $this->_removeButton('reset'); 
        //$this->_removeButton('back'); 
    }

    public function getHeaderText()
    {
        $e = Mage::registry('adjgiftreg_event');
        return $e->getFullTitle();
    }
}