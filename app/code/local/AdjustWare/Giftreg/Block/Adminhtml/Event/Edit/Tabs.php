<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Adminhtml_Event_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('eventTabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('adjgiftreg')->__('Registry Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('details', array(
            'label'     => Mage::helper('adjgiftreg')->__('Event Details'),
            'content'   => $this->getLayout()->createBlock('adjgiftreg/adminhtml_event_edit_tab_details')->toHtml(),
        ));

        $this->addTab('items', array(
            'label'     => Mage::helper('adjgiftreg')->__('Items'),
            //'content'   => $this->getLayout()->createBlock('adjgiftreg/adminhtml_event_edit_tab_items')->toHtml(),
            'class'     => 'ajax',
            'url'       => $this->getUrl('*/*/items', array('_current' => true)),
        ));

        $this->addTab('orders', array(
            'label'     => Mage::helper('customer')->__('Orders'),
            'class'     => 'ajax',
            'url'       => $this->getUrl('*/*/orders', array('_current' => true)),
        )); 

        $this->_updateActiveTab();
        return parent::_beforeToHtml();
    }
    
    protected function _updateActiveTab()
    {
    	$tabId = $this->getRequest()->getParam('tab');
    	if ($tabId) {
    		$tabId = preg_replace("#{$this->getId()}_#", '', $tabId);
    		if ($tabId) {
    			$this->setActiveTab($tabId);
    		}
    	}
    	else
    	   $this->setActiveTab('details'); 
    }

}