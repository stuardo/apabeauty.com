<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Adminhtml_Event_Edit_Tab_Details extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        //create form structure
        $form = new Varien_Data_Form();
        $this->setForm($form);
        
        $hlp = Mage::helper('adjgiftreg');
        
        $fldInfo = $form->addFieldset('registrant', array('legend'=> $hlp->__('Registrant')));
        $fldInfo->addField('fname', 'text', array(
          'label'     => $hlp->__('First Name'),
          'name'      => 'fname',
        ));
        $fldInfo->addField('lname', 'text', array(
          'label'     => $hlp->__('Last Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'lname',
        ));

        $fldInfo2 = $form->addFieldset('coregistrant', array('legend'=> $hlp->__('Co-Registrant')));
        $fldInfo2->addField('fname2', 'text', array(
          'label'     => $hlp->__('First Name'),
          'name'      => 'fname2',
        ));
        $fldInfo2->addField('lname2', 'text', array(
          'label'     => $hlp->__('Last Name'),
          'name'      => 'lname2',
        ));
        
        //set form values
        $data = Mage::getSingleton('adminhtml/session')->getFormData();
        $model = Mage::registry('adjgiftreg_event');
        if ($data) {
            $form->setValues($data);
            Mage::getSingleton('adminhtml/session')->setFormData(null);
        }
        elseif ($model) {
            $form->setValues($model->getData());
        }
        
        return parent::_prepareForm();
    }
}