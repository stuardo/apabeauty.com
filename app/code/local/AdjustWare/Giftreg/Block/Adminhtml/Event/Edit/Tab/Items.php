<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Adminhtml_Event_Edit_Tab_Items extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {

        parent::__construct();
        $this->setId('itemsGrid');
        $this->setUseAjax(true);
        $this->setDefaultSort('priority');
        $this->setDefaultDir('DESC');

        
        
    }

    protected function _prepareCollection()
    {

        $id = Mage::registry('adjgiftreg_event')->getId();
        $items = Mage::getResourceModel('adjgiftreg/item_collection')
            ->addEventFilterForAdmin($id)
            ->joinSku();

        Mage::getModel('adjgiftreg/event')->updateOrderedCounter($items);
        
        $items = Mage::getResourceModel('adjgiftreg/item_collection')
            ->addEventFilterForAdmin($id)
            ->joinSku();
            
        $this->setCollection($items);

        
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
    	


        $hlp = Mage::helper('adjgiftreg');

        $this->addColumn('item_id', array(
            'header'    => $hlp->__('ID'),
            'index'     => 'item_id',
            'width'     => '50px', 
        ));

        $this->addColumn('priority', array(
            'header'    => $hlp->__('Priority'),
            'index'     => 'priority',
            'type'      => 'options', 
            'options'   => $hlp->getPriorities(),
        ));
        
        $this->addColumn('product_name', array(
            'header'    => $hlp->__('Product Name'),
            'index'     => 'product_name',
            'filter'    => false,
            'sortable'  => false,
        ));

        $this->addColumn('sku', array(
            'header'    => $hlp->__('SKU'),
            'index'     => 'sku',
            'filter'    => false,
            'sortable'  => false,
        ));

        $this->addColumn('descr', array(
            'header' => $hlp->__('Customer Comments'), 
            'index'  => 'descr', 
            ));

        $this->addColumn('num_wants', array(
            'header'    => $hlp->__('Desired'),
            'index'     => 'num_wants', 
            'width'     => '50px', 
            ));
        if(Mage::getStoreConfig('adjgiftreg/general/dspl_invoices') == 1)
        {
        $this->addColumn('num_has', array(
            'header'    => $hlp->__('Invoiced'),
            'index'     => 'num_has', 
            'width'     => '50px', 
            ));
        }
        $this->addColumn('num_total', array(
            'header'    => $hlp->__('Ordered'),
            'index'     => 'num_total', 
            'width'     => '50px', 
            ));

        $this->addColumn('added_at', array(
            'header'    => $hlp->__('Added At'),
            'index'     => 'added_at',
            'gmtoffset' => true,
            'type'      => 'date',
            'width'     => '100px',
            ));
        if (Mage::getSingleton('admin/session')->isAllowed('adminhtml/item/actions/edit')) {
            $this->addColumn('action',
                array(
                    'header'    => $hlp->__('Action'),
                    'width'     => '50px',
                    'type'      => 'action',
                    'getter'     => 'getId',
                    'actions'   => array(
                        array(
                            'caption' => $hlp->__('View'),
                            'url'     => array('base'=>'*/adminhtml_item/edit/',
                                        'params'=>array('event'=>Mage::registry('adjgiftreg_event')->getId())),
                            'field'   => 'id'
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                    'index' => 'stores',
                    'is_system' => true,
            ));
        }

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/adminhtml_item/edit', array('id' => $row->getItemId(), 'event' => Mage::registry('adjgiftreg_event')->getId()));
    }

}