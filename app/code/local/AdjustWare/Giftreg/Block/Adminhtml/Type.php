<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Adminhtml_Type extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_controller = 'adminhtml_type';
        $this->_headerText = Mage::helper('adjgiftreg')->__('Manage Event Types');
        $this->_addButtonLabel = Mage::helper('adjgiftreg')->__('Add New Type');
        $this->_blockGroup = 'adjgiftreg';
        
    }

}