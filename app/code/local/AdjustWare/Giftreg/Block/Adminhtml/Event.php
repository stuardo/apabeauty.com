<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Adminhtml_Event extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_controller = 'adminhtml_event';
        $this->_headerText = Mage::helper('adjgiftreg')->__('Manage Registries');
        $this->_blockGroup = 'adjgiftreg';
        $this->_removeButton('add'); 
    }
    
    protected function _enabledAddNewButton()
    {
        return false;
    }

}