<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Adminhtml_Customer_Tab extends Mage_Adminhtml_Block_Widget_Grid
{
    public $_parentTemplate = '';
    
    public function __construct()
    {
        parent::__construct();
        $this->setId('adjgiftregGrid');
        $this->setUseAjax(true);
        $this->_parentTemplate = $this->getTemplate();
        $this->setTemplate('adjgiftreg/tab.phtml');
        $this->setEmptyText(Mage::helper('customer')->__('No Items Found'));
    }

    protected function _prepareCollection()
    {
        $events = Mage::getResourceModel('adjgiftreg/event_collection')
            ->addCustomerFilter(Mage::registry('current_customer')->getId(), false);
        $this->setCollection($events);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $hlp = Mage::helper('adjgiftreg');

        $this->addColumn('date', array(
            'header'    => $hlp->__('Event date'),
            'index'     => 'date',
            'gmtoffset' => true,
            'type'      => 'date',
        ));

        $this->addColumn('type_id', array(
            'header'    => $hlp->__('Occasion'),
            'index'     => 'type_id',
            'type'      => 'options', 
            'options'   => $hlp->getOccasions(),
        ));


        $this->addColumn('status', array(
            'header'    => $hlp->__('Event Status'),
            'index'     => 'status',
            'type'      => 'options', 
            'options'   => $hlp->getStatuses(),
        ));

        $this->addColumn('fname2', array(
            'header'    => $hlp->__('Co-Registrant First Name'),
            'index'     => 'fname2'
        ));

        $this->addColumn('lname2', array(
            'header'    => $hlp->__('Co-Registrant Last Name'),
            'index'     => 'lname2',
        ));

        $this->addColumn('pass', array(
            'header'    => $hlp->__('Password'),
            'index'     => 'pass',
        ));

        $this->addColumn('action', array(
            'header'    => Mage::helper('customer')->__('Action'),
            'index'     => 'event_id',
            'type'      => 'action',
            'filter'    => false,
            'sortable'  => false,
            'actions'   => array(
                array(
                    'caption' =>  $hlp->__('Remove'),
                    'url'     =>  'adjgiftregadmin/adminhtml_customer/events',
                    'onclick' =>  'return adjgiftregControl.removeItem($event_id);'
                ),
            )
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('adjgiftregadmin/adminhtml_customer/events', array('_current'=>true));
    }
 
    public function getGridParentHtml()
    {
        $templateName = Mage::getDesign()->getTemplateFilename($this->_parentTemplate, array('_relative'=>true));
        return $this->fetchView($templateName);
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('adjgiftregadmin/adminhtml_event/edit', array('id' => $row->getId()));
    }

}