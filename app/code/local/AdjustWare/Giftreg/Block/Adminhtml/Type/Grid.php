<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Adminhtml_Type_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('typesGrid');
        $this->setDefaultSort('pos');
        $this->setDefaultDir('ASC');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('adjgiftreg/type')
            ->getResourceCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $hlp = Mage::helper('adjgiftreg');
        $this->addColumn('type_id', array(
            'header'    => $hlp->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'type_id',
        ));
        $this->addColumn('hide', array(
            'header'    => $hlp->__('Hidden'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'hide',
            'type'      => 'options', 
            'options'   => array($hlp->__('No'), $hlp->__('Yes')),
        ));
        $this->addColumn('pos', array(
            'header'    => $hlp->__('Position'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'pos',
        ));
        
        $this->addColumn('code', array(
            'header'    => $hlp->__('Event Type'),
            'align'     =>'left',
            'index'     => 'code',
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}