<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Adminhtml_Type_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
        //create form structure
        $form = new Varien_Data_Form(array(
          'id' => 'edit_form',
          'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
          'method' => 'post')
         );
        
        $form->setUseContainer(true);
        $this->setForm($form);
        $hlp = Mage::helper('adjgiftreg');
        
        $fldInfo = $form->addFieldset('adjgiftreg_info', array('legend'=> $hlp->__('Titles')));
        $fldInfo->addField('code', 'text', array(
          'label'     => $hlp->__('Default'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'code',
        ));
      
        foreach (Mage::getSingleton('adminhtml/system_store')->getStoreCollection() as $store) {
            $fldInfo->addField('title_' . $store->getId(), 'text', array(
                'label'     => $store->getName(),
                'name'      => 'titles['. $store->getId() .']',
            ));
        }
      
        $fldVis = $form->addFieldset('adjgiftreg_vis', array('legend'=> $hlp->__('Display Options')));
        $fldVis->addField('pos', 'text', array(
          'label'     => $hlp->__('Sorting Order'),
          'class'     => 'validate-number',
          'name'      => 'pos',
        ));
        
        $fldVis->addField('hide', 'select', array(
          'label'     => $hlp->__('Hide'),
          'name'      => 'hide',
          'values'      => array(
            array(
                'value' => 0,
                'label' => Mage::helper('catalog')->__('No')
            ),
            array(
                'value' => 1,
                'label' => Mage::helper('catalog')->__('Yes')
            )),
        ));
        
        //set form values
        $data = Mage::getSingleton('adminhtml/session')->getFormData();
        $model = Mage::registry('adjgiftreg_type');
        if ($data) {
            $form->setValues($data);
            if (!empty($data['titles']) && is_array($data['titles'])) {
               $this->_setTitlesPerStore($data['titles']);
            }
            Mage::getSingleton('adminhtml/session')->setFormData(null);
        }
        elseif ($model) {
            $form->setValues($model->getData());
            if ($model->getTitles()) {
               $this->_setTitlesPerStore($model->getTitles());
            }
        }
        
        return parent::_prepareForm();
  }
  
    protected function _setTitlesPerStore($codes) {
        foreach($codes as $storeId=>$value) {
            if ($element = $this->getForm()->getElement('title_' . $storeId)) {
                $element->setValue($value);
            }
        }
    }
}