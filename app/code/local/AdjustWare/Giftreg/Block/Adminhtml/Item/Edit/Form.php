<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Adminhtml_Item_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
        //create form structure
        $form = new Varien_Data_Form(array(
          'id' => 'edit_form',
          'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
          'method' => 'post')
         );
        
        $form->setUseContainer(true);
        $this->setForm($form);
        $hlp = Mage::helper('adjgiftreg');
        
        $fldInfo = $form->addFieldset('main', array('legend'=> $hlp->__('Item')));
        $fldInfo->addField('num_wants', 'text', array(
          'label'     => $hlp->__('Desired'),
          'class'     => 'validate-number',
          'name'      => 'num_wants',
        ));
        if(Mage::getStoreConfig('adjgiftreg/general/dspl_invoices') == 1)
        {
        $fldInfo->addField('num_has', 'text', array(
                'label'     => $hlp->__('Invoiced'),
                'class'     => 'validate-number',
                'name'      => 'num_has',
                'disabled'  => '1',
            ));
        }
        $fldInfo->addField('num_total', 'text', array(
          'label'     => $hlp->__('Ordered'),
          'class'     => 'validate-number',
          'name'      => 'num_total',
          'disabled'  => '1',
        ));
        $fldInfo->addField('num_user_set', 'text', array(
                'label'     => $hlp->__('Increase/decrease received items by'),
                'class'     => 'validate-number',
                'name'      => 'num_user_set',
                'note'   => $hlp->__('Fill the field with negative value to decrease the numbers of Invoiced and Ordered items appropriately'),
            ));
        /*
        $fldInfo->addField('priority', 'select', array(
          'label'     => $hlp->__('Priority'),
          'name'      => 'priority',
          'values'    => $hlp->getPriorities(true),
        ));
        */
        $fldInfo->addField('descr', 'textarea', array(
          'label'     => $hlp->__('Comments'),
          'name'      => 'descr',
        ));
        
        $fldInfo->addField('event_id', 'hidden', array(
          'name'      => 'event_id',
        ));
        
        //set form values
        $data = Mage::getSingleton('adminhtml/session')->getFormData();
        $model = Mage::registry('adjgiftreg_item');
        if ($data) {
            $form->setValues($data);
            Mage::getSingleton('adminhtml/session')->setFormData(null);
        }
        elseif ($model) {
            $form->setValues($model->getData());
            }
        
        return parent::_prepareForm();
  }

}