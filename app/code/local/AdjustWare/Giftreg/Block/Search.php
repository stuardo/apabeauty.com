<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Search extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle($this->getTitle());
        }

        // add Home breadcrumb
        if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbs->addCrumb('home', array(
                'label'=>Mage::helper('adjgiftreg')->__('Home'),
                'title'=>Mage::helper('adjgiftreg')->__('Go to Home Page'),
                'link'=>Mage::getBaseUrl()
            ))->addCrumb('search', array(
                'label'=> $this->getTitle()
            ));
        }
        return parent::_prepareLayout();
    }
    
    public function getTitle(){
        return Mage::helper('adjgiftreg')->__('Gift Lists Search');
    }
    
    public function getOptionMonths(){
        $options= array();
        $mon = Mage::app()->getLocale()->getTranslationList('months');
        foreach ($mon['format']['wide'] as $code => $name) 
            $options[$code] = $name;
            
        return $options;    
    }

    public function getUrl($route = '', $params = array())
    {
        return Mage::helper('adjgiftreg')->prepareSecureUrls(parent::getUrl($route, $params));
    }
}