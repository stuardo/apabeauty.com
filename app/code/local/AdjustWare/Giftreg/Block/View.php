<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_View extends Mage_Catalog_Block_Product_Abstract
{
    public function __construct()
    {
        parent::__construct(); 
        $this->setEvent(Mage::registry('adjgiftreg_event'));      
    } 
    
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        
        if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbs->addCrumb('home', array(
                'label'=>Mage::helper('adjgiftreg')->__('Home'),
                'title'=>Mage::helper('adjgiftreg')->__('Go to Home Page'),
                'link'=>Mage::getBaseUrl()
            ))->addCrumb('search', array(
                'label'=>Mage::helper('adjgiftreg')->__('Gift Lists Search'),
                'link'=>$this->getUrl('gifts/*/')
            ))->addCrumb('search_result', array(
                'label'=> $this->getTitle()
            ));
        }
        
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle($this->getTitle());
        }
        
        
        return $this;
    }  
    
    public function getTitle()
    {
        $title = $this->getEvent()->getFullTitle(); 
        if ($this->_getData('title'))
            $title = $this->_getData('title');
        return $title;
    }
    

    
    public function getPriority($item){
        $proirities = Mage::helper('adjgiftreg')->getPriorities();
        return $proirities[$item->getPriority()];
    }
    
    public function isOwner(){
        return Mage::getSingleton('customer/session')->getCustomerId() == $this->getEvent()->getCustomerId();
    }
    
    public function needPassword(){
       if(!Mage::helper('adjgiftreg')->usePasswordForRegistries())
           return false;

       $result = false;
       
       if ($this->getEvent()->getPass() && !$this->isOwner())
       {
           $allowedEvents = Mage::getSingleton('adjgiftreg/session')->getAllowedEvents();
           if (!is_array($allowedEvents) || !in_array($this->getEvent()->getId(), $allowedEvents))
                $result = true;
       }
       
       return $result;
    }
    
    public function getOptionsHtml($item)
    {
        return Mage::getModel('adjgiftreg/product')->getOptionsHtml($item);
    }
    public function ifSimpleInStock($item)
    {
        return Mage::getModel('adjgiftreg/product')->ifSimpleInStock($item);
    }

    
    public function getTotalPrice($item)
    {
        $nBasePrice = $item->getProduct()->getFinalPrice();
        
        if ($nAdditionPrice = Mage::registry('ait_additional_price'))
        {
            $nBasePrice += $nAdditionPrice;
        }
        
        return $nBasePrice;
    }
    
    public function getLinkNotice()
    {
    	$curr_customer_id = Mage::getSingleton('customer/session')->getCustomer()->getEntityId();

		if($this->getEvent()->getCustomerId() == $curr_customer_id)
		{
    		$url = Mage::getUrl('gifts/index/view', array('code' => $this->getRequest()->getParam('code')));
    		return Mage::helper('adjgiftreg')->__('You can also share your gift registry using <a href="%s">this link</a>. Just copy and paste it wherever you want.', $url);
		}
    	else 
    	{
    		return '';
    	}	
    	
    }
    
    public function getItemImageUrl(AdjustWare_Giftreg_Model_Item $item)
    {
        return Mage::getBlockSingleton('adjgiftreg/manage')->getItemImageUrl($item);   
    }
    
    
}