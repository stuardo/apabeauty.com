<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Details extends AdjustWare_Giftreg_Block_Common
{ 
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle($this->__('Event\'s Gift Registry'));
        }
    }
    
    public function getEvent(){
        return Mage::registry('current_event');
    }
    
    public function getLinkNotice()
    {
    	return Mage::helper('adjgiftreg')->__('You can also share your gift registry using the link to Preview section. Just copy its URL and paste it where you want your registry to be visible for others.');
    }
}