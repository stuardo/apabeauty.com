<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Block_Manage extends Mage_Catalog_Block_Product_Abstract
{ 
    protected $_events = null;
    
    //gets active and expired events except given id
    public function getVisibleEvents($exclude=0){
        if (is_null($this->_events)){
            $this->_events = Mage::getResourceModel('adjgiftreg/event_collection')
                ->addCustomerFilter(Mage::getSingleton('customer/session')->getCustomerId())
                ->addFieldToFilter('event_id', array('neq' => $exclude))
                ->load(); 
                
        }


        return $this->_events;
    }
    
    public function getEvent(){
        return Mage::registry('current_event');
    }
    
    public function getUrl($route='', $params=array())
    {
        $params['_forced_secure'] = true;
        return parent::getUrl($route, $params);
    } 
    
    public function getOptionsHtml($item)
    {
        return Mage::getModel('adjgiftreg/product')->getOptionsHtml($item);
    }

    public function ifSimpleInStock($item)
    {
        return Mage::getModel('adjgiftreg/product')->ifSimpleInStock($item);
    }

    public function getTotalPrice($item)
    {
        $nBasePrice = $item->getProduct()->getFinalPrice();
        
        if ($nAdditionPrice = Mage::registry('ait_additional_price'))
        {
            $nBasePrice += $nAdditionPrice;
        }
        
        return $nBasePrice;
    }
    
    public function getItemImageUrl(AdjustWare_Giftreg_Model_Item $item)
    {
        $option = $item->getOptionByCode('info_buyRequest');
        $buyRequest = unserialize($option->getValue());
        $simpleProduct = $item->getOptionByCode('simple_product');  
        #$simpleProductId = $simpleProduct->getProductId();
        if(!empty($buyRequest['grouped_product_id']))
        {
            $productId = !empty($buyRequest['grouped_product_id']) && (Mage::getStoreConfig('checkout/cart/grouped_product_image') == 'parent')  ? $buyRequest['grouped_product_id'] : $item->getProductId();    
        }
        elseif(!empty($simpleProduct))
        {
            $productId = (Mage::getStoreConfig('checkout/cart/configurable_product_image') != 'parent')  ? $simpleProduct->getProductId() : $item->getProductId();
        }
        else
        {
            $productId = $item->getProductId();    
        }
        return $this->helper('catalog/image')->init(Mage::getModel('catalog/product')->load($productId), 'small_image')->resize(113, 113);        
    }
    
}