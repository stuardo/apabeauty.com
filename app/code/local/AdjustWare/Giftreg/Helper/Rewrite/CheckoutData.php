<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Helper_Rewrite_CheckoutData extends Mage_Checkout_Helper_Data
{
    public function isMultishippingCheckoutAvailable()
    {
        $quote = $this->getQuote();
        if ($quote->getAdjgiftregEventId())
        {
            return false;
        }
        return parent::isMultishippingCheckoutAvailable();
    }
}