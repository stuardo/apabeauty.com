<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Helper_Data extends Mage_Core_Helper_Abstract
{
    private $_addBlock = null;
    private $_types = null;
    
    public function getPriorities($asLabels=false){
        $a = array(
            $this->__('Nice to have'),
            $this->__('Like to have'),
            $this->__('Love to have'),
        );
        
        if ($asLabels){
            for ($i=0; $i<sizeof($a); ++$i)
                $a[$i] = array('label'=>$a[$i], 'value'=>$i);
        }
        
        return $a;
    }
    
    public function getOccasions($showHidden = true){
        if (is_null($this->_types)){
            $types = Mage::getResourceModel('adjgiftreg/type_collection')
                ->addTitles(Mage::app()->getStore()->getId())
                ->addVisibilityFilter($showHidden)
                ->load(); 
            $this->_types = array();
            foreach ($types as $type)
                $this->_types[$type->getId()] = $type->getTitle();
        }
        return $this->_types;

    }
    
    public function getStatuses(){
        return array(
            'active'  => $this->__('Active'), 
            'removed' => $this->__('Removed'), 
            'expired' => $this->__('Expired'),
        ); 
    }
    
    public function isAllow($product=null){
        $res = false;
        if (Mage::getStoreConfigFlag('adjgiftreg/general/active')) 
			$res = true;
//		if ($product && $product->isGrouped())
		if (!$product) //aitoc fix
            $res = false;
		
		return $res; 
    }

    public function getAddUrl($product){
        $url = '';
        if ($this->isAllow($product)){
            $url =  $this->_getUrl('gifts/event/addItem', 
                array('product'=>$product->getId())); 
        }
        
        return $url;
    }
    
    public function getCartUrl($item){
         return $this->_getUrl('gifts/index/cart', array('item' => $item->getId())); 
    }
        
    public function sendAdminNotification($message, $event){
        $enabled = Mage::getStoreConfig('adjgiftreg/email/notification_enable');
        if (!$enabled)
            return;
            
        $recipient = trim(Mage::getStoreConfig('adjgiftreg/email/notification_recipient'));
        if (!$recipient)
            return;
            
        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);
        try {
            $emailModel = Mage::getModel('core/email_template');
            $emailModel->sendTransactional(
                Mage::getStoreConfig('adjgiftreg/email/notification'),
                Mage::getStoreConfig('adjgiftreg/email/notification_identity'),
                $recipient,
                null,
                array(
                    'message'      => $message,
                    'title'        => $event->getFullTitle(),
                    'frontend_url' => Mage::getModel('core/url')->getUrl('gifts/index/view', array('code' => $event->getSharingCode())),
                    'backend_url'  => Mage::getModel('core/url')->getUrl('adjgiftregadmin/adminhtml_event/edit', array('id' => $event->getId(), '_store' => 'admin')),
                    'date'         => date('Y-m-d'),
            ));            

            $translate->setTranslateInline(true);
            
        } catch (Exception $e) {
            $translate->setTranslateInline(true);
        }
    }

    /** Replaces unsecure URLs to secure if configured
     * 
     * @param string $html input HTML or URL
     * @return string replaced HTML or URL
     */
    public function prepareSecureUrls($html)
    {
        if (Mage::getStoreConfig('adjgiftreg/general/secure') && !Mage::getStoreConfig('web/secure/use_in_frontend'))
        {
            if ('{{base url}}' == Mage::getStoreConfig('web/unsecure/base_url'))
            {
                $html = str_replace('http://', 'https://', $html);
            }
            else 
            {
                $html = str_replace(Mage::getStoreConfig('web/unsecure/base_url'), Mage::getStoreConfig('web/secure/base_url'), $html);
            }
        }

        return $html;
    }

    public function getOrderEventInfo($orderId)
    {
        /* @TODO FIX IT! REALLY BAD CODE! */
        $event = Mage::getResourceModel('adjgiftreg/thank_collection')
            ->addOrderFilter($orderId)
			->join('event','(`event`.event_id = `main_table`.event_id)', array('title','lname','fname'));

		foreach($event as $item)
		{
			return $this->__('Gift registry').': '.$item->getFname().' '.$item->getLname().' '.$item->getTitle();
		}

		return '';
    }

    public function usePasswordForRegistries()
    {
        if(Mage::getStoreConfig('adjgiftreg/general/enable_password_protection'))
            return true;

        return false;
    }

    public function usePrivateStatusForRegistries()
    {
        if(Mage::getStoreConfig('adjgiftreg/general/enable_private_status_selection'))
            return true;

        return false;
    }
}