<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_EventController extends Mage_Core_Controller_Front_Action
{
    private $_customerId = 0;
    
    protected function _redirect($route, $params=array()){
        if (Mage::getStoreConfig('adjgiftreg/general/secure'))
            $params['_forced_secure'] = true; 
            
        return parent::_redirect($route, $params); 
    }
    
    public function preDispatch()
    {
        parent::preDispatch();
        $session = Mage::getSingleton('customer/session');

        if (!$session->authenticate($this)) {
            $this->setFlag('', 'no-dispatch', true);
            if(!$session->getBeforeGiftregUrl()) {
                $session->setBeforeGiftregUrl($this->_getRefererUrl());
            }
            if ($this->getRequest()->isPost()){
                //store custom options for the guest
                $productId = $this->getRequest()->getParam('product');
                if ($productId){
                    $params[$productId] = $this->getRequest()->getParams();
                    $session->setGiftregParams($params);
                }
            }
        }
        $this->_customerId = $session->getCustomer()->getId();
        if (!Mage::getStoreConfigFlag('adjgiftreg/general/active')) {
            $this->norouteAction();
            return;
        }
    }

    public function indexAction()
    {
      $events = Mage::getResourceModel('adjgiftreg/event_collection')
        ->addCustomerFilter($this->_customerId)
        ->load();
        
      $cnt = $events->count();        
      if (0 == $cnt) {
          $this->_redirect('gifts/*/edit');
          return;
      } 
      elseif (1 == $cnt){
        $this->_redirect('gifts/*/details', array('id'=>$events->getFirstItem()->getId()));
        return;	
      }
      $this->_redirect('gifts/*/list');
    }
    
    // list all customer events
    public function listAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
            $navigationBlock->setActive('gifts/event');
        }
        $this->renderLayout();
    }

    //show form with event details and addresses
    public function editAction()
    {
        $event = Mage::getModel('adjgiftreg/event');
        if ($id = $this->getRequest()->getParam('id')){
            $event->load($id);
		    if (!$event->isAvailableFor($this->_customerId)){
		      $this->_redirect('gifts/*/index');
		    }    
        } 
        Mage::register('current_event', $event);
        
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
            $navigationBlock->setActive('gifts/event');
        }
        $this->renderLayout();
    }
    
    public function detailsAction()
    {
        $event  = Mage::getModel('adjgiftreg/event');
		$id     = (int)$this->getRequest()->getParam('id');
		$event->load($id);
		if (!$event->isAvailableFor($this->_customerId)){
		    $this->_redirect('gifts/*/index');
		}
		Mage::register('current_event', $event);
		
		$this->loadLayout();
        if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
            $navigationBlock->setActive('gifts/event');
        }
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        $this->renderLayout();
    }
    
    public function thanksAction()
    {
        $event  = Mage::getModel('adjgiftreg/event');
		$id     = (int)$this->getRequest()->getParam('id');
		$event->load($id);
		if (!$event->isAvailableFor($this->_customerId)){
		    $this->_redirect('gifts/*/index');
		}
		Mage::register('current_event', $event);
		
		$this->loadLayout();
        if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
            $navigationBlock->setActive('gifts/event');
        }
        $this->_initLayoutMessages('customer/session');
        
        if ($blockDetails = $this->getLayout()->getBlock('adjgiftreg.details'))
		  $blockDetails->setEvent($event); 
		  
        $this->renderLayout();
    }

    /*
    * a sub-method for saveAction() method
    * @param AdjustWare_Giftreg_Model_Event $event
    * @return AdjustWare_Giftreg_Model_Event
    */
    protected function _saveEventData($event)
    {
        $data = $this->getRequest()->getPost();
        $id = $this->getRequest()->getParam('id');

        if (Mage::helper('adjgiftreg')->usePasswordForRegistries() && ($data['password'] == $data['confirmation']))
            $event->setPass($data['password']); // store as plain text
        if(Mage::helper('adjgiftreg')->usePrivateStatusForRegistries())
        {
            if (empty($data['search_allowed']))
                $event->setSearchAllowed(0);
            else
                $event->setSearchAllowed(1);
        }

        $event->setStatus('active');

        $date = Mage::app()->getLocale()->date($event->getDate() . ' 12:00:00 PM');

        $event->setDate($date->toString('yyyy-MM-dd'));
    
        $types = $event->getTypes(); 
        if (!empty($types[$event->getTypeId()]))
        $event->setTitle($types[$event->getTypeId()]);
        else 
        Mage::throwException(Mage::helper('adjgiftreg')->__('Please provide occasion'));
    
        if (!$id){ // generate code only once
        $event->setSharingCode(md5(uniqid('ev')));
        }
     
        $event->setCustomerId($this->_customerId);

        $event->save();
        Mage::getSingleton('customer/session')->addSuccess(Mage::helper('adjgiftreg')->__('Event has been successfully saved'));    
        Mage::getSingleton('customer/session')->setEventFormData(false);

        return $event;
    }
    
    //save event details and addresses
 	public function saveAction() {
 	    if (!$this->_validateFormKey()) {
            return $this->_redirect('gifts/*/index');
        }
	    $id     = $this->getRequest()->getParam('id');
	    $event  = Mage::getModel('adjgiftreg/event');
	    if ($id){
	       $event->load($id);
	       if (!$event->isAvailableFor($this->_customerId)){
	           $this->_redirect('gifts/*/index');    
	       }
	    }
	    
		if ($data = $this->getRequest()->getPost()) {
			$event->setData($data)->setId($id);
			try {
    			$event = $this->_saveEventData($event);				
				
				// new event
				if (!$id){ 
				    $hlp = Mage::helper('adjgiftreg');
				    $hlp->sendAdminNotification($hlp->__('A new gift registry has been created'), $event);
				}				

			    $productId = Mage::getSingleton('adjgiftreg/session')->getAddProductId(); 
			    Mage::getSingleton('adjgiftreg/session')->setAddProductId(null);  
				if ($productId){
				    $this->_redirect('gifts/*/addItem', array('product' => $productId, 'event'=>$event->getId()));
				    return;
				}
				$this->_redirect('gifts/*/details', array('id' => $event->getId()));
				return;
            } catch (Exception $e) {
                Mage::getSingleton('customer/session')->addError($e->getMessage());
                Mage::getSingleton('customer/session')->setEventFormData($data);
                $this->_redirect('gifts/*/', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('customer/session')->addError(Mage::helper('adjgiftreg')->__('Unable to find event to save'));
        $this->_redirect('gifts/*/');
	}
	
	public function removeAction() {

	    $id     = (int)$this->getRequest()->getParam('id');
	    $event  = Mage::getModel('adjgiftreg/event');
	    $event->load($id);
	    if ($event->isAvailableFor($this->_customerId)){
    		try {
        		$event->remove();
 
			    $hlp = Mage::helper('adjgiftreg');
			    $hlp->sendAdminNotification($hlp->__('gift registry has been removed'), $event);
        		
        		Mage::getSingleton('customer/session')->addSuccess($this->__('Event has been successfully removed'));
            } catch (Exception $e) {
                Mage::getSingleton('customer/session')->addError($e->getMessage());
            }
		}
        $this->_redirect('gifts/*/list');
	}
	
	public function removeItemAction() {
	    $id    = (int) $this->getRequest()->getParam('id');
	    
	    $item  = Mage::getModel('adjgiftreg/item');
	    $item->load($id);
	    if (!$item->getId()){
	        $this->_redirect('gifts/*/');
	    }
	    
	    $event = Mage::getModel('adjgiftreg/event');
	    $event->load($item->getEventId());
	    if (!$event->isAvailableFor($this->_customerId)){
	        $this->_redirect('gifts/*/');
	    }
	    
        try {
            $item->delete();
    		Mage::getSingleton('customer/session')->addSuccess($this->__('Product has been successfully removed from your Gift List'));
        }
        catch (Exception $e) {
                $session->addError($this->__('There was an error while removing item from the Gift List: %s'), $e->getMessage());
        }
        $this->_redirect('gifts/*/details', array('id' => $event->getId()));
        
	}
   
	
    /**
     * Get product for product information
     *
     * @param   mixed $productInfo
     * @return  Mage_Catalog_Model_Product
     */
    protected function _getProduct($productInfo)
    {
        $product = null;
        if ($productInfo instanceof Mage_Catalog_Model_Product) {
            $product = $productInfo;
        }
        elseif (is_numeric($productInfo)) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($productInfo);
        }
        return $product;
    }

    /**
     * Get request for product add to cart procedure
     *
     * @return  Varien_Object
     */
    protected function _getProductRequest()
    {
        $requestInfo = $this->getRequest()->getParams();
        
        $params = Mage::getSingleton('customer/session')->getGiftregParams();
        if ($params && key($params) == $this->getRequest()->getParam('product')){
            $requestInfo = current($params);
            Mage::getSingleton('customer/session')->setGiftregParams(null);
        }
        
        if ($requestInfo instanceof Varien_Object) {
            $request = $requestInfo;
        }
        elseif (is_numeric($requestInfo)) {
            $request = new Varien_Object();
            $request->setQty($requestInfo);
        }
        else {
            $request = new Varien_Object($requestInfo);
        }

        if (!$request->hasQty()) {
            $request->setQty(1);
        }
        
        return $request;
    }	
	
 	public function addItemAction() {        
 	    $session    = Mage::getSingleton('customer/session'); 	    
	    $productId  = $this->getRequest()->getParam('product');	    
	    $event      = Mage::getModel('adjgiftreg/event');
	    $eventId    = $this->getRequest()->getParam('event');
	    
	    if (!$eventId){ //get default - last
	       $eventId = $event->getLastEventId($this->_customerId); 
	    }
	    
	    if (!$eventId) { //create new
	       Mage::getSingleton('adjgiftreg/session')->setAddProductId($productId);
	       $this->_redirect('gifts/*/edit/');
	       return;
	    }
	    
	    $event->load($eventId);
	    if ($event->isAvailableFor($this->_customerId)){
            try {
                $product = $this->_getProduct($productId);
                
                $item = null;
                
                if ($product->getTypeId() == 'grouped')
                {
                    $groupedProductParentId = $product->getId();
                    $sessionParams = $session->getGiftregParams();
                    $requestInfo = $this->getRequest()->getParams();
                    $bHasProducts = false;
                    
                    if (isset($sessionParams[$product->getId()]['super_group']))
                    {
                        $requestInfo['super_group'] = $sessionParams[$product->getId()]['super_group'];
                    }
                    
                    if ($requestInfo AND !empty($requestInfo['super_group']))
                    {
                        foreach ($requestInfo['super_group'] as $iSubProductId => $iQty)
                        {
                            if ($iQty)
                            {
                                $request = $this->_getProductRequest();    
                                
                                $request->setProduct($iSubProductId);
                                $request->setQty($iQty);            
                                $request->setGroupedProductId($groupedProductParentId);
                                $product = $this->_getProduct($iSubProductId);
                                
                                // check if params are valid
                                $customOptions = $product->getTypeInstance()->prepareForCart($request, $product);
                                
                                // string == error during prepare cycle
                                if (is_string($customOptions)) {
                                    $session->setRedirectUrl($product->getProductUrl());
                                    Mage::throwException($customOptions);
                                }                                  
                                $item = $event->addItem($iSubProductId, $customOptions); 
                                
                                $bHasProducts = true;
                            }
                        }
                    }
                    
                    if (!$bHasProducts)
                    {
                        $this->getResponse()->setRedirect($product->getProductUrl());
                        
                        $checkoutSession = Mage::getSingleton('checkout/session');
                
                        $message = $this->__('Please specify the product(s) quantity');
                        
                        $checkoutSession->addError($message);        
                        
                        return '';
                    }
                }
                else 
                {
                    $bHasProducts = true;
                    
                    $request = $this->_getProductRequest();    
                
                    // check if params are valid
                    $customOptions = $product->getTypeInstance()->prepareForCart($request, $product);
                    
                    // string == error during prepare cycle
                    if (is_string($customOptions)) {
                        $session->setRedirectUrl($product->getProductUrl());
                        Mage::throwException($customOptions);
                    }                
                    
                    $item = $event->addItem($productId, $customOptions); 
                }

                
                if ($referer = $session->getBeforeGiftregUrl())
                    $session->setBeforeGiftregUrl(null);
                else 
                    $referer = $this->_getRefererUrl();

               if ($bHasProducts)
               { 

	           $message = $this->__('Product has been successfully added to your gift registry. Click <a href="%s">here</a> to continue shopping', $referer);
	           if ($item AND $item->getIsOld()){
                    $message = $this->__('You already have this product in your registry. To add the product with different attributes please remove the old one first. Click <a href="%s">here</a> to continue shopping', $referer);	               
               }   
               
               $session->addSuccess($message); 
               }
               
               $this->_redirect('gifts/*/details/', array('id'=>$eventId));
            } 
            catch (Exception $e) {
                $url =  $session->getRedirectUrl(true);
                if ($url) {
                    Mage::getSingleton('checkout/session')->addNotice($e->getMessage());
                    $this->getResponse()->setRedirect($url);
                }
                else {
                    $session->addError($this->__('There was an error while adding item to the Gift List: %s'), $e->getMessage());  
                }
            }
        }
    }
	
	public function updateAction() {
        if (!$this->_validateFormKey()) {
            return $this->_redirect('gifts/*/');
        }
        
        $eventId = $this->getRequest()->getParam('event_id');

        $event  = Mage::getModel('adjgiftreg/event');
        $event->load($eventId);
        if (!$event->isAvailableFor($this->_customerId)){
            return $this->_redirect('gifts/*/');
        }
        
        $post = $this->getRequest()->getPost();
        if ($post && isset($post['description']) && is_array($post['description'])) {
            foreach ($post['description'] as $itemId => $descr) {
                $item = Mage::getModel('adjgiftreg/item')->load($itemId);
                if ($item->getEventId() != $eventId) {
                     continue;
                }
                try {
                    if (isset($post['num_wants'][$itemId]) && '0' == $post['num_wants'][$itemId])
                        $item->delete();
                    else 
                    { 
                        $item->setDescr($descr)
                            ->setNumWants(max(1, intVal($post['num_wants'][$itemId])))
                            ->setPriority(intVal($post['priority'][$itemId]));
                        
                        $newEventId = isset($post['moveto']) ? $post['moveto'][$itemId] : 0;
                        if ($newEventId ){
                            if ($item->canBeMovedTo($newEventId)){ 
                                $item->setEventId($newEventId);
                            } else {
                                Mage::getSingleton('customer/session')->addError(
                                    $this->__('Can not move id=%d, because destination event already contains this item', $itemId)
                                );
                            }
                        }
                        
                        $item->save();
                    }
                }
                catch (Exception $e) {
                    Mage::getSingleton('customer/session')->addError(
                        $this->__('Can not save item: %s.', $e->getMessage())
                    );
                }
            }
            Mage::getSingleton('customer/session')->addSuccess($this->__('Registry has been successfully updated'));
        }
        $this->_redirect('gifts/*/details/', array('id'=>$eventId));
	}

    public function shareAction()
    {
        $id = (int)$this->getRequest()->getParam('id');

        $event  = Mage::getModel('adjgiftreg/event')
            ->load($id);
            
        if (!$event->isAvailableFor($this->_customerId)){
            return $this->_redirect('gifts/*/list');
        } 
        
        Mage::register('current_event', $event);
               
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('adjgiftreg/session');
        if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
            $navigationBlock->setActive('gifts/event');
        }
        $this->renderLayout();
    }

    /*
    * @return array
    */
    protected function _getCorrectEmails()
    {
        $emails = explode(',', $this->getRequest()->getPost('emails'));
        
        $error  = array();
        if (empty($emails)) {
            $error[] = $this->__('Email address can\'t be empty.');
        }
        else {
            foreach ($emails as $index => $email) {
                $email = trim($email);
                if (!Zend_Validate::is($email, 'EmailAddress')) {
                    $error[] = $this->__('Please check this address: %s', $email);
                } 
                $emails[$index] = $email;
            }
        }
        if ($error) {
            Mage::getSingleton('adjgiftreg/session')->addError(implode(',', $error));
            Mage::getSingleton('adjgiftreg/session')->setSharingForm($this->getRequest()->getPost());
            $id = (int)$this->getRequest()->getParam('id');
            $this->_redirect('gifts/*/share', array('id' => $id));
            $emptyArray = array();
            return $emptyArray;
        }

        return $emails;
    }

    public function sendAction()
    {
        if (!$this->_validateFormKey()) {
            return $this->_redirect('gifts/*/');
        }
        
        //check event
        $id = (int)$this->getRequest()->getParam('id');
        $event  = Mage::getModel('adjgiftreg/event')
            ->load($id);
            
        if (!$event->isAvailableFor($this->_customerId)){
            return $this->_redirect('gifts/*/list');
        } 
        
        $emails = $this->_getCorrectEmails();
        if(count($emails) == 0)
            return '';
                
        $translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);

        try {
            $customer = Mage::getSingleton('customer/session')->getCustomer();

            $block = $this->getLayout()->createBlock('adjgiftreg/email')
                ->setCollection($event->getProductCollection())
                ->toHtml();

            $emails = array_unique($emails);
            $emailModel = Mage::getModel('core/email_template');
            $message= nl2br(htmlspecialchars((string) $this->getRequest()->getPost('message')));
            $pass = $event->getPass();
            if(!Mage::helper('adjgiftreg')->usePasswordForRegistries())
                $pass = '';

            foreach ($emails as $email) {
                $emailModel->sendTransactional(
                    Mage::getStoreConfig('adjgiftreg/email/share'),
                    Mage::getStoreConfig('adjgiftreg/email/share_identity'),
                    $email,
                    null,
                    array(
                        'customer'   => $customer,
                        'items'      => &$block,
                        'viewLink'   => Mage::getUrl('gifts/index/view', array('code'=>$event->getSharingCode())),
                        'message'    => $message,
                        'store_name' => Mage::app()->getStore()->getName(),
                        'password'   => $pass,
                    ));
            }
            // join previous and current emails and save for logging purposes
            $prevEmails = $event->getEmails();
            $event->setEmails(array_merge($emails, $prevEmails));
            $event->save();

            $translate->setTranslateInline(true);

            Mage::getSingleton('customer/session')->addSuccess(
                $this->__('Your Gift Registry has been successfully shared')
            );
            Mage::getSingleton('adjgiftreg/session')->setSharingForm(null);
            $this->_redirect('gifts/*/details', array('id' => $id));
        }
        catch (Exception $e) {
            $translate->setTranslateInline(true);

            Mage::getSingleton('adjgiftreg/session')->addError($e->getMessage());
            Mage::getSingleton('adjgiftreg/session')->setSharingForm($this->getRequest()->getPost());
            $this->_redirect('gifts/*/share', array('id' => $id));
        }
    }

}