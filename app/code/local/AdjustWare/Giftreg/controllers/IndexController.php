<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('adjgiftreg/session');
        $this->renderLayout();
    }
    
    public function resultAction()
    {
        $q = $this->getRequest()->getQuery();
        if (empty($q['lname'])){
            Mage::getSingleton('adjgiftreg/session')->addError(Mage::helper('adjgiftreg')->__('Please provide Last Name'));
            $url = Mage::getModel('core/url')
                ->setQueryParams($this->getRequest()->getQuery())
                ->getUrl('gifts/*/', array('_escape' => true));
            $this->_redirectError($url);
        }
        $this->loadLayout();
        $this->renderLayout();
    }
    
    public function viewAction(){

        $code = (string) $this->getRequest()->getParam('code');
        if (empty($code)) {
            $this->_forward('gifts/*');
            return;
        }

        $event = Mage::getModel('adjgiftreg/event')->loadByCode($code);

        if (!$event->getId()) {
            $this->_forward('gifts/*');
            return;
        } 
        
        Mage::register('adjgiftreg_event', $event);
        
        $this->loadLayout();
        $this->_initLayoutMessages('adjgiftreg/session');
        $this->_initLayoutMessages('checkout/session');
        $this->renderLayout();
    }
    
    public function cartAction()
    {
        $id   = (int) $this->getRequest()->getParam('id');
        $qty  = (int) $this->getRequest()->getParam('qty');
        $item = Mage::getModel('adjgiftreg/item')->load($id);
        if (!$item->getId()){
            $this->_forward('gifts/*');
            return;
        }
        
        $event = Mage::getModel('adjgiftreg/event')->load($item->getEventId());
        //todo check status 
        if (!$event->getId()){ 
            $this->_forward('gifts/*');
            return;
        }
        $quote = Mage::getSingleton('checkout/cart');
        $prevEventId = $quote->getQuote()->getAdjgiftregEventId();
        
        if ($prevEventId && $prevEventId != $event->getId()){
            Mage::getSingleton('adjgiftreg/session')->addError(Mage::helper('adjgiftreg')->__('To ensure correct shipping, please select gifts from only one registry at once. Sorry for the unconvinience'));
            $this->_redirect('gifts/*/view', array('code'=> $event->getSharingCode()));
            return;
        }
        
        try {
            $item->setEvent($event);
            $this->_addItemToSession($item);

            $product = Mage::getModel('catalog/product')
                ->load($item->getProductId())
                ->setQty(max(1, $qty));
            
            $req = unserialize($item->getBuyRequest());
            $req['qty'] = $product->getQty();			

            $quote = Mage::getSingleton('checkout/cart')
                   ->addProduct($product, $req)
                   ->save();
        }
        catch (Exception $e) {
            Mage::getSingleton('checkout/session')->addNotice($e->getMessage());
            $url = Mage::getSingleton('checkout/session')->getRedirectUrl(true);
            if ($url) {
                $url = Mage::getModel('core/url')->getUrl('catalog/product/view', array(
                    'id'        => $item->getProductId(),
                ));
                $this->getResponse()->setRedirect($url);
            }
            else {
                $this->_redirect('gifts/*/view', array('code'=>$event->getSharingCode()));
            }
            return;
        }
        $this->_redirectToCart();
    }
    
    protected function _redirectToCart()
    {
        if (Mage::getStoreConfig('checkout/cart/redirect_to_cart')) {
            $this->_redirect('checkout/cart');
        } 
        else {
            $url = $this->_getRefererUrl();
            if ($url)
                $this->getResponse()->setRedirect($url);
            else 
                $this->_redirect('gifts/*/');
        }
    }
    
    public function popularAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('adjgiftreg/session');
        $this->renderLayout();
    }
    
    public function authAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        $pass = (string) $this->getRequest()->getParam('password');
        
        $event = Mage::getModel('adjgiftreg/event')->load($id);

        if (!$event->getId() || !$pass) {
            $this->_forward('gifts/*');
            return;
        }
        
        if ($event->getPass() == $pass){
           $allowedEvents = Mage::getSingleton('adjgiftreg/session')->getAllowedEvents();
           if (!is_array($allowedEvents))
                $allowedEvents = array();
           $allowedEvents[] = $id;
           Mage::getSingleton('adjgiftreg/session')->setAllowedEvents($allowedEvents);
        }
        else {
            Mage::getSingleton('adjgiftreg/session')->addError(Mage::helper('adjgiftreg')->__('Please provide a valid registry password'));
            
        }
        $this->_redirect('gifts/*/view', array('code'=> $event->getSharingCode()));
    }
    
    // we will need this info at the checkout process
    private function _addItemToSession($item){
        $gifts = Mage::getSingleton('checkout/session')->getAdjgiftregItem();
        if (!is_array($gifts)) // first time
            $gifts = array();
            
        $gifts[$item->getProductId()] = $item;
        Mage::getSingleton('checkout/session')->setAdjgiftregItem($gifts);       
    }
    
    public function allcartAction() {
        $messages           = array();
        $urls               = array();
        $notSalableNames    = array(); // Out of stock products message

        $eventId = $this->getRequest()->getParam('id');
        $event = Mage::getModel('adjgiftreg/event')->load($eventId);
        if (!$event->getId()) {
            $this->_forward('gifts/*');
            return;
        }
        
        $quote = Mage::getSingleton('checkout/cart');
        $prevEventId = $quote->getQuote()->getAdjgiftregEventId();
        
        if ($prevEventId && $prevEventId != $event->getId()){
            Mage::getSingleton('adjgiftreg/session')->addError(Mage::helper('adjgiftreg')->__('To ensure correct shipping, please select gifts from only one registry at once. Sorry for the unconvinience'));
            $this->_redirect('gifts/*/view', array('code'=> $event->getSharingCode()));
            return;
        }     
           
        $selectedQties   = (array)$this->getRequest()->getParam('qty');
        $selectedItems = (array)$this->getRequest()->getParam('items');
        
        foreach ($event->getProductCollection() as $item) {
            if (!in_array($item->getItemId(), $selectedItems)) // its' better to add a filter to the collection
                continue;
                
            try {
                $qty = isset($selectedQties[$item->getItemId()]) ? $selectedQties[$item->getItemId()] : 1;
                $product = Mage::getModel('catalog/product')
                    ->load($item->getProductId())
                    ->setQty(max(1, $qty));
                    
                $item->setEvent($event);
                
                $this->_addItemToSession($item);

                $req = unserialize($item->getBuyRequest());
                $req['qty'] = $product->getQty();			

                $quote->addProduct($product, $req);
                
            } 
            catch (Exception $e) {
                $url = Mage::getSingleton('checkout/session')
                    ->getRedirectUrl(true);
                
                if ($url) {
                    $url = Mage::getModel('core/url')
                        ->getUrl('catalog/product/view', array(
                            'id' => $item->getProductId(),
                            'gift_next' => 1
                        ));

                    $urls[]         = $url;
                    $messages[]     = $e->getMessage();
                } 
                else {
                    Mage::getSingleton('checkout/session')->addNotice($e->getMessage());
                    $this->_redirect('gifts/*/view', array('code'=>$event->getSharingCode()));
                    return;
                }
            }
        }
        $quote->save();
        
        
        // do we still need this code ??  ))
//        if (count($notSalableNames) > 0) {
//            Mage::getSingleton('checkout/session')
//                ->addNotice($this->__('This product(s) are currently out of stock:'));
//                
//            array_map(array(Mage::getSingleton('checkout/session'), 'addNotice'), $notSalableNames);
//        }

        if ($urls) {
            Mage::getSingleton('checkout/session')->addNotice(array_shift($messages));
            $this->getResponse()->setRedirect(array_shift($urls));

            Mage::getSingleton('checkout/session')->setGiftregPendingUrls($urls);
            Mage::getSingleton('checkout/session')->setGiftregPendingMessages($messages);
        }
        else {
             $this->_redirectToCart();
        }
    } 
    
    
    
}