<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Adminhtml_EventController extends Mage_Adminhtml_Controller_Action
{
    protected function _initEvent($idFieldName = 'id')
    {
        $id = (int) $this->getRequest()->getParam($idFieldName);
        $event = Mage::getModel('adjgiftreg/event');

        if ($id) {
            $event->load($id);
        }

        Mage::register('adjgiftreg_event', $event);
        return $event;
    } 
    
    public function indexAction(){
	    $this->loadLayout(); 
        $this->_setActiveMenu('customer/adjgiftreg/registries');
        $this->_addBreadcrumb($this->__('Gift Registries'), $this->__('Registries')); 
        $this->_addContent($this->getLayout()->createBlock('adjgiftreg/adminhtml_event')); 	    
 	    $this->renderLayout();
    }
    
	public function newAction() {
		$this->editAction();
	}
	
    public function editAction() {
		$model = $this->_initEvent();
		if (!$model->getId()) {
    		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adjgiftreg')->__('Registry does not exist'));
			$this->_redirect('*/*/');
		}
		
		$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		$this->loadLayout();
		$this->_setActiveMenu('customer/adjgiftreg/registries');
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addContent($this->getLayout()->createBlock('adjgiftreg/adminhtml_event_edit'))
             ->_addLeft($this->getLayout()->createBlock('adjgiftreg/adminhtml_event_edit_tabs'));
		$this->renderLayout();
	}

	public function saveAction() {
	    $id     = $this->getRequest()->getParam('id');
	    $model  = $this->_initEvent();
	    $data = $this->getRequest()->getPost();
		if ($data) {
			$model
                ->setFname($data['fname'])
                ->setLname($data['lname'])
                ->setFname2($data['fname2'])
                ->setLname2($data['lname2'])
                ->setId($id);
			try {
				$model->save();
				    
			    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adjgiftreg')->__('Event has been successfully saved'));    
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				$this->_redirect('*/*/');
				return;
				
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adjgiftreg')->__('Unable to find Event to save'));
        $this->_redirect('*/*/');
	} 
	
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        if (!$id) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adjgiftreg')->__('Unable to find a Event to delete'));
            $this->_redirect('*/*/');
            return;
        }
        
        try {
            $model = Mage::getModel('adjgiftreg/event');
            $model->setId($id);
            $model->remove();
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adjgiftreg')->__('Event has been marked as removed'));
            $this->_redirect('*/*/');
        }
        catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
        }
        
    } 
    
    public function ordersAction() {
        $this->_initEvent();
		$this->getResponse()->setBody($this->getLayout()->createBlock('adjgiftreg/adminhtml_event_edit_tab_orders')->toHtml());
	}

	public function itemsAction() {
        $this->_initEvent();
		$this->getResponse()->setBody($this->getLayout()->createBlock('adjgiftreg/adminhtml_event_edit_tab_items')->toHtml());
	}
    
}