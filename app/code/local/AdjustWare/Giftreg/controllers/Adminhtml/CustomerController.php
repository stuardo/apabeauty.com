<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Adminhtml_CustomerController extends Mage_Adminhtml_Controller_Action
{
    protected function _initCustomer($idFieldName = 'id')
    {
        $customerId = (int) $this->getRequest()->getParam($idFieldName);
        $customer = Mage::getModel('customer/customer');

        if ($customerId) {
            $customer->load($customerId);
        }

        Mage::register('current_customer', $customer);
        return $this;
    }
    
    public function eventsAction()
    {
        $this->_initCustomer();
        $customer = Mage::registry('current_customer');
        if ($customer->getId()) {
            if ($id = (int) $this->getRequest()->getParam('delete')) {
                try {
                    Mage::getModel('adjgiftreg/event')
                        ->load($id)
                        ->remove();
                }
                catch (Exception $e) {
                    //
                }
            }
        }
        $this->getResponse()->setBody($this->getLayout()->createBlock('adjgiftreg/adminhtml_customer_tab')->toHtml());
    }
}