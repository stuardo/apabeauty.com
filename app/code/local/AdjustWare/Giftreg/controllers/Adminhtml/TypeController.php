<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Adminhtml_TypeController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction(){
	    $this->loadLayout(); 
        $this->_setActiveMenu('customer/adjgiftreg/types');
        $this->_addBreadcrumb($this->__('Gift Registries'), $this->__('Event Types')); 
        $this->_addContent($this->getLayout()->createBlock('adjgiftreg/adminhtml_type')); 	    
 	    $this->renderLayout();
    }
    
	public function newAction() {
		$this->editAction();
	}
	
    public function editAction() {
		$id     = (int) $this->getRequest()->getParam('id');
		$model  = Mage::getModel('adjgiftreg/type')->load($id);

		if ($id && !$model->getId()) {
    		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adjgiftreg')->__('Event Type does not exist'));
			$this->_redirect('*/*/');
		}
		
		$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register('adjgiftreg_type', $model);

		$this->loadLayout();
		$this->_setActiveMenu('customer/adjgiftreg/types');
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addContent($this->getLayout()->createBlock('adjgiftreg/adminhtml_type_edit'));
		$this->renderLayout();
	}

	public function saveAction() {
	    $id     = $this->getRequest()->getParam('id');
	    $model  = Mage::getModel('adjgiftreg/type');
	    $data = $this->getRequest()->getPost();
		if ($data) {
			$model->setData($data)->setId($id);
		    if (!empty($data['titles']))
			    $model->setTitles($data['titles']);
			try {
				$model->save();
				    
			    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adjgiftreg')->__('Event Type has been successfully saved'));    
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				$this->_redirect('*/*/');
				return;
				
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adjgiftreg')->__('Unable to find Event Type to save'));
        $this->_redirect('*/*/');
	} 
	
		
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        if (!$id) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adjgiftreg')->__('Unable to find a Event Type to delete'));
            $this->_redirect('*/*/');
            return;
        }
        
        try {
            Mage::getModel('adjgiftreg/type')
                ->setId($id)
                ->setHide(1)
                ->save();
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adjgiftreg')->__('Event Type has been marked as hidden'));
            $this->_redirect('*/*/');
        }
        catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
        }
        
    }
}