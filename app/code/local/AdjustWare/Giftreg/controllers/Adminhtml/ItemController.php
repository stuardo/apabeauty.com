<?php
/**
 * Gift Registry
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Giftreg
 * @version      2.2.11
 * @license:     iVswWldT67nnLz2HBq4Um0pXfKHCOk8d3Yav6a7rCA
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class AdjustWare_Giftreg_Adminhtml_ItemController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction(){
//	    $this->loadLayout(); 
//        $this->_setActiveMenu('customer/adjgiftreg/registries');
//        $this->_addBreadcrumb($this->__('Gift Registries'), $this->__('Registries')); 
//        $this->_addContent($this->getLayout()->createBlock('adjgiftreg/adminhtml_event')); 	    
// 	    $this->renderLayout();
        $this->editAction();
    }
    
	public function newAction() {
		$this->editAction();
	}
	
    public function editAction() {
		$id     = (int) $this->getRequest()->getParam('id');
		$model  = Mage::getModel('adjgiftreg/item')->load($id);

		if ($id && !$model->getId()) {
    		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adjgiftreg')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
		
		$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register('adjgiftreg_item', $model);

		$this->loadLayout();
		$this->_setActiveMenu('customer/adjgiftreg/registries');
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addContent($this->getLayout()->createBlock('adjgiftreg/adminhtml_item_edit'));
		$this->renderLayout();
	}

	public function saveAction() {
	    $id     = $this->getRequest()->getParam('id');
	    $model  = Mage::getModel('adjgiftreg/item');
	    $data = $this->getRequest()->getPost();
		if ($data) {
			$model->setData($data)->setId($id);
			try {
				$model->save();
				    
			    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adjgiftreg')->__('Item has been successfully saved'));    
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				$this->_redirect('*/adminhtml_event/edit', array('id'=>$model->getEventId(), 'tab'=>'items'));
				return;
				
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adjgiftreg')->__('Unable to find Item to save'));
        $this->_redirect('*/*/');
	} 
	
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        if (!$id) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adjgiftreg')->__('Unable to find an item to delete'));
            $this->_redirect('*/*/');
            return;
        }
        
        try {
            $item = Mage::getModel('adjgiftreg/item')->load($id);
            $eventId = $item->getEventId();
            
            $item->delete();
            
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adjgiftreg')->__('Item has been deleted'));
            $this->_redirect('*/adminhtml_event/edit', array('id'=>$eventId, 'tab'=>'items'));

        }
        catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
        }
        
    }
	
	
	
}