<?php
/**
 * Created by RedChamps.
 * User: Rav
 * Date: 22/08/17
 * Time: 5:12 PM
 */
class Varien_Image_Adapter_Imagemagic extends Varien_Image_Adapter_Abstract
{

    protected $_requiredExtensions = array('imagick');

    /**
     * Get the Imagemagick class.
     *
     * @return Imagick|null
     * @throws Exception
     */
    protected function getImageMagick()
    {
        if ($this->_imageHandler === null) {
            $this->_imageHandler = new Imagick();
        }
        return $this->_imageHandler;
    }

    /**
     * @param $fileName
     */
    public function open($fileName)
    {
        Varien_Profiler::start(__METHOD__);
        $this->_fileName = $fileName;
        $this->getMimeType();
        $this->_getFileAttributes();
        $this->getImageMagick()->readimage($fileName);
        Varien_Profiler::stop(__METHOD__);
    }

    /**
     * Write file to file system.
     *
     * @param null $destination
     * @param null $newName
     * @throws Exception
     */
    public function save($destination = null, $newName = null)
    {
        Varien_Profiler::start(__METHOD__);
        if (isset($destination) && isset($newName)) {
            $fileName = $destination . "/" . $newName;
        } elseif (isset($destination) && !isset($newName)) {
            $info = pathinfo($destination);
            $fileName = $destination;
            $destination = $info['dirname'];
        } elseif (!isset($destination) && isset($newName)) {
            $fileName = $this->_fileSrcPath . "/" . $newName;
        } else {
            $fileName = $this->_fileSrcPath . $this->_fileSrcName;
        }

        $destinationDir = (isset($destination)) ?
            $destination : $this->_fileSrcPath;

        if (!is_writable($destinationDir)) {
            try {
                $io = new Varien_Io_File();
                $io->mkdir($destination);
            } catch (Exception $e) {
                Varien_Profiler::stop(__METHOD__);
                throw
                new Exception(
                    "Unable to write into directory '{$destinationDir}'."
                );
            }
        }
        //set compression quality
        $this->getImageMagick()->setImageCompressionQuality(
            $this->getQuality()
        );
        //remove all underlying information
        $this->getImageMagick()->stripImage();
        //write to file system
        $this->getImageMagick()->writeImages($fileName, true);
        $this->getImageMagick()->writeImage(str_ireplace('.gif','-static.gif', $fileName));
        //clear data and free resources
        $this->getImageMagick()->clear();
        $this->getImageMagick()->destroy();
        Varien_Profiler::stop(__METHOD__);
    }

    /**
     * Just display the image
     */
    public function display()
    {
        header("Content-type: " . $this->getMimeType());
        echo $this->getImageMagick();
    }

    /**
     * @param null $frameWidth
     * @param null $frameHeight
     * @throws Exception
     */
    public function resize($frameWidth = null, $frameHeight = null)
    {

        Varien_Profiler::start(__METHOD__);
        $imagick = $this->getImageMagick();

        $dims = $this->_adaptResizeValues($frameWidth, $frameHeight);

        $imagick = $imagick->coalesceImages();
        do {
            $current = $imagick->current();
            $current->thumbnailImage($dims['dst']['width'], $dims['dst']['height']);

            $newImage = new Imagick();
            $newImage->newImage(
                $dims['frame']['width'],
                $dims['frame']['height'],
                new ImagickPixel('transparent')
            );
            $newImage->compositeImage(
                $current,
                Imagick::COMPOSITE_COPYOPACITY,
                $dims['dst']['x'],
                $dims['dst']['y']
            );

            $newImage->compositeImage(
                $current,
                Imagick::COMPOSITE_OVER,
                $dims['dst']['x'],
                $dims['dst']['y']
            );
            $newImage->setImageFormat($current->getImageFormat());
            $newImage->setImageDelay($current->getImageDelay());
            $current->setImage($newImage);
        } while ($imagick->nextImage());
        $imagick = $imagick->deconstructImages();
        $this->_imageHandler->clear();
        $this->_imageHandler->destroy();
        $this->_imageHandler = $imagick;
        $this->refreshImageDimensions();

        Varien_Profiler::stop(__METHOD__);
    }

    protected function _adaptResizeValues($frameWidth, $frameHeight)
    {
        $this->_checkDimensions($frameWidth, $frameHeight);

        // calculate lacking dimension
        if (!$this->_keepFrame && $this->_checkSrcDimensions()) {
            if (null === $frameWidth) {
                $frameWidth = round($frameHeight * ($this->_imageSrcWidth / $this->_imageSrcHeight));
            } elseif (null === $frameHeight) {
                $frameHeight = round($frameWidth * ($this->_imageSrcHeight / $this->_imageSrcWidth));
            }
        } else {
            if (null === $frameWidth) {
                $frameWidth = $frameHeight;
            } elseif (null === $frameHeight) {
                $frameHeight = $frameWidth;
            }
        }

        // define coordinates of image inside new frame
        $srcX = 0;
        $srcY = 0;
        list($dstWidth, $dstHeight) = $this->_checkAspectRatio($frameWidth, $frameHeight);

        // define position in center
        // TODO: add positions option
        $dstY = round(($frameHeight - $dstHeight) / 2);
        $dstX = round(($frameWidth - $dstWidth) / 2);

        // get rid of frame (fallback to zero position coordinates)
        if (!$this->_keepFrame) {
            $frameWidth = $dstWidth;
            $frameHeight = $dstHeight;
            $dstY = 0;
            $dstX = 0;
        }

        return [
            'src' => ['x' => $srcX, 'y' => $srcY],
            'dst' => ['x' => $dstX, 'y' => $dstY, 'width' => $dstWidth, 'height' => $dstHeight],
            // size for new image
            'frame' => ['width' => $frameWidth, 'height' => $frameHeight]
        ];
    }

    /**
     * Check aspect ratio
     *
     * @param int $frameWidth
     * @param int $frameHeight
     * @return int[]
     */
    protected function _checkAspectRatio($frameWidth, $frameHeight)
    {
        $dstWidth = $frameWidth;
        $dstHeight = $frameHeight;
        if ($this->_keepAspectRatio && $this->_checkSrcDimensions()) {
            // do not make picture bigger, than it is, if required
            if ($this->_constrainOnly) {
                if ($frameWidth >= $this->_imageSrcWidth && $frameHeight >= $this->_imageSrcHeight) {
                    $dstWidth = $this->_imageSrcWidth;
                    $dstHeight = $this->_imageSrcHeight;
                }
            }
            // keep aspect ratio
            if ($this->_imageSrcWidth / $this->_imageSrcHeight >= $frameWidth / $frameHeight) {
                $dstHeight = round($dstWidth / $this->_imageSrcWidth * $this->_imageSrcHeight);
            } else {
                $dstWidth = round($dstHeight / $this->_imageSrcHeight * $this->_imageSrcWidth);
            }
        }
        return [$dstWidth, $dstHeight];
    }

    /**
     * Check Frame dimensions and throw exception if they are not valid
     *
     * @param int $frameWidth
     * @param int $frameHeight
     * @return void
     * @throws \Exception
     */
    protected function _checkDimensions($frameWidth, $frameHeight)
    {
        if ($frameWidth !== null && $frameWidth <= 0 ||
            $frameHeight !== null && $frameHeight <= 0 ||
            empty($frameWidth) && empty($frameHeight)
        ) {
            throw new Exception('Invalid image dimensions.');
        }
    }

    /**
     * Return false if source width or height is empty
     *
     * @return bool
     */
    protected function _checkSrcDimensions()
    {
        return !empty($this->_imageSrcWidth) && !empty($this->_imageSrcHeight);
    }

    /**
     * @param $angle
     */
    public function rotate($angle)
    {
        $this->getImageMagick()->rotateimage(new ImagickPixel(), $angle);

        $this->refreshImageDimensions();
    }

    /**
     * @param int $top
     * @param int $left
     * @param int $right
     * @param int $bottom
     */
    public function crop($top = 0, $left = 0, $right = 0, $bottom = 0)
    {
        if ($left == 0 && $top == 0 && $right == 0 && $bottom == 0) {
            return;
        }

        $newWidth = $this->_imageSrcWidth - $left - $right;
        $newHeight = $this->_imageSrcHeight - $top - $bottom;

        /* because drlrdsen said so!  */
        $this->getImageMagick()->cropImage(
            $newWidth,
            $newHeight,
            $left,
            $top
        );

        $this->refreshImageDimensions();
    }

    /**
     * @param $watermarkImage
     * @param int $positionX
     * @param int $positionY
     * @param int $watermarkImageOpacity
     * @param bool $repeat
     */
    public function watermark(
        $watermarkImage,
        $positionX = 0,
        $positionY = 0,
        $watermarkImageOpacity = 30,
        $repeat = false)
    {
        Varien_Profiler::start(__METHOD__);

        /** @var $watermark Imagick */
        $watermark = new Imagick($watermarkImage);

        //better method to blow up small images.
        $watermark->setimageinterpolatemethod(
            Imagick::INTERPOLATE_NEARESTNEIGHBOR
        );

        if ($this->_watermarkImageOpacity == null) {
            $opc = $watermarkImageOpacity;
        } else {
            $opc = $this->getWatermarkImageOpacity();
        }

        $watermark->evaluateImage(
            Imagick::EVALUATE_MULTIPLY,
            $opc,
            Imagick::CHANNEL_ALPHA
        );

        // how big are the images?
        $iWidth = $this->getImageMagick()->getImageWidth();
        $iHeight = $this->getImageMagick()->getImageHeight();

        //resize watermark to configuration size
        if ($this->getWatermarkWidth() &&
            $this->getWatermarkHeigth() &&
            ($this->getWatermarkPosition() != self::POSITION_STRETCH)
        ) {
            $watermark->scaleImage(
                $this->getWatermarkWidth(),
                $this->getWatermarkHeigth()
            );
        }

        // get watermark size
        $wWidth = $watermark->getImageWidth();
        $wHeight = $watermark->getImageHeight();

        //check if watermark is still bigger then image.
        if ($iHeight < $wHeight || $iWidth < $wWidth) {
            // resize the watermark
            $watermark->scaleImage($iWidth, $iHeight);
            // get new size
            $wWidth = $watermark->getImageWidth();
            $wHeight = $watermark->getImageHeight();
        }

        $x = 0;
        $y = 0;

        switch ($this->getWatermarkPosition()) {
            case self::POSITION_CENTER:
                $x = ($iWidth - $wWidth) / 2;
                $y = ($iHeight - $wHeight) / 2;
                break;
            case self::POSITION_STRETCH:
                $watermark->scaleimage($iWidth, $iHeight);
                break;
            case self::POSITION_TOP_RIGHT:
                $x = $iWidth - $wWidth;
                break;
            case self::POSITION_BOTTOM_LEFT:
                $y = $iHeight - $wHeight;
                break;
            case self::POSITION_BOTTOM_RIGHT:
                $x = $iWidth - $wWidth;
                $y = $iHeight - $wHeight;
                break;
            default:
                break;

        }

        $this->getImageMagick()->compositeImage(
            $watermark,
            Imagick::COMPOSITE_OVER,
            $x,
            $y
        );
        $watermark->clear();
        $watermark->destroy();
        Varien_Profiler::stop(__METHOD__);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function checkDependencies()
    {
        foreach ($this->_requiredExtensions as $value) {
            if (!extension_loaded($value)) {
                throw
                new Exception(
                    "Required PHP extension '{$value}' was not loaded."
                );
            }
        }
        return true;
    }

    protected function refreshImageDimensions()
    {
        $this->_imageSrcWidth = $this->_imageHandler->getImageWidth();
        $this->_imageSrcHeight = $this->_imageHandler->getImageHeight();
        $this->_imageHandler->setImagePage($this->_imageSrcWidth, $this->_imageSrcHeight, 0, 0);
    }

    public function __destruct()
    {
        @$this->getImageMagick()->clear();
        @$this->getImageMagick()->destroy();
    }

    /**
     * @return int
     */
    public function getQuality()
    {
        if ($this->_quality == null) {
            $this->_quality = 80;
        }
        return $this->_quality;
    }

    /**
     * @return float
     */
    public function getWatermarkImageOpacity()
    {
        if ($this->_watermarkImageOpacity == 0) {
            return $this->_watermarkImageOpacity = 0;
        }
        return $this->_watermarkImageOpacity / 100;
    }
}
