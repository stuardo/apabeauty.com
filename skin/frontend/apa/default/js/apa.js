$j(document).ready(function () {

    $j('.slider').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 5,
        // centerPadding: '0px',
        responsive: [
            {
                breakpoint: 1280,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5
                }
            },
            {
                breakpoint: 1038,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 770,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 568,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    /*
    $j("#product-description").dotdotdot({
        after: 'a.readmore',
        callback: dotdotdotCallback,
        watch: "window"
    });
    $j('#product-description').on('click','a',function(e) {
        if ($j(this).hasClass('readmore')) {
            var div = $j(this).closest('#product-description');
            div.trigger('destroy').find('a.readmore').hide();
            div.css('height', 'auto');
            $j("a.readless", div).show();
        }
        else {
            $j(this).hide();
            $j(this).closest('#product-description').css("height", "80px").dotdotdot({ after: "a.readmore", callback: dotdotdotCallback, watch: "window" });
        }

        e.preventDefault();
    });

    function dotdotdotCallback(isTruncated, originalContent) {
        if (!isTruncated) {
            $j("a", this).remove();
        }
    }
    */

    // fix padding in header
//    var activeItems = $j(".nav-primary").find(".level0");
//console.log(activeItems);
//    if ( ! activeItems.hasClass("active")) {
//        console.log('setting paddin');
//        activeItems.find('a').css({
//            'padding-bottom': '4px'
//        });
//    }

    // fix separator on PDP with registry
    $j(".add-to-links .separator").remove();

    // video play click
    if ($j('#homepage_video').length > 0) {


        var videoPlayer = videojs("homepage_video").ready(function() {
            var myPlayer = this;    // Store the video object
            var aspectRatio = 9/16; // Make up an aspect ratio

            function resizeVideoJS(){
                // Get the parent element's actual width
                var width = document.getElementById("homepage_video").parentElement.offsetWidth;
                // Set width to fill parent element, Set height
                myPlayer.width(width).height( width * aspectRatio );
            }

            $j("#homepage_video").click(function() {
    //            debugger;
    //            myPlayer.play();
            });

            resizeVideoJS(); // Initialize the function
            window.onresize = resizeVideoJS; // Call the function on resize
        });
    }

    // PDP
    $j('#block-related').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        // centerMode: true,
        // centerPadding: '0px',
        arrows: true,
        responsive: [
            {
                breakpoint: 1280,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 770,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 568,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    // rearrange back links in buttons-sets.
    jQuery('.buttons-set').find('p.back-link').each(function() {
        jQuery(this).insertAfter(jQuery(this).parent('.buttons-set').find('.button'));
    });


    // dynamic positioning of mobile nav left padding to line up with
    // hamburger
    function respaceMobileSubNav() {

        var windowWidth = jQuery(window).width();
        if (windowWidth < 770) {

            var hamburgerOffset = jQuery('.skip-nav .icon').offset();
            var hamLeft = hamburgerOffset.left + 3;

            jQuery('#header-nav a.level0').css("padding-left", hamLeft + "px");
            jQuery('#header-account a').css("padding-left", hamLeft + "px");
            jQuery('.nav-primary a.level1').css("padding-left", (hamLeft + 25) + "px");
        }
    }

    respaceMobileSubNav();
    window.onresize = respaceMobileSubNav;

    // make menu active (bold) on these pages
    var bodyClasses = ["catalog-category-view", "catalog-product-view",
                       "category-about", "category-daily-foundation-care",
                       "category-radiance-care"];
    bodyClasses.each(function(value) {

        if (jQuery("body").hasClass(value)) {
            jQuery('.skip-nav').addClass('skip-nav-hover');
        }
    });

    function makeCaptions() {
        jQuery('.caption').hcaptions({
            effect: "fade",
            speed: 200,
            overlay_bg: ""
        });
    }

    $j(window).on('delayed-resize', function (e, resizeEvent) {
        makeCaptions();
    });

    //makeCaptions();

    setTimeout(function() {
        makeCaptions();
    }, 100);
});


